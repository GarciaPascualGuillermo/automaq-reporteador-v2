﻿using MongoDB.Driver;
using Npgsql;
using System;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using iTextSharp.text.pdf;
using iTextSharp.text;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace Automaq_Reporteador_V2
{
    public partial class Eficiencia : System.Web.UI.Page
    {
        static bool ajustador = false;
        static bool pr = false;
        String query = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                loadEmployee();
                loadTable();
            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }
        //apartir de aqui van las funciones propias de cada reporte
        public void resetFilter(object sender, EventArgs e)

        {
            Tipo.SelectedValue = "0";
            UserList.SelectedValue = "0";
            JobNumber.Value = "";
            Customer.Value = "";
            Partn.Value = "";
            Fecha_final.Value = "";
            Fecha_inicio.Value = "";
            Excel.Visible = false;
            Pdf.Visible = false;
            JobNumber.Visible = false;
        }

        public void Find(object sender, EventArgs e)
        {
            Excel.Visible = true;
            Pdf.Visible = true;
           
            loadData();
          
        }
        public void Find2(object sender, EventArgs e)
        {
           
            loadEmployee();
           //loadData();

        }
        public void loadEmployee()
        {
            Excel.Visible = false;
            Pdf.Visible = false;
       

            if (ajustador== false && Request.Form["Tipo"]=="2")
            {
                UserList.Items.Clear();
               // System.Diagnostics.Debug.WriteLine("limpiar ajustador");
                    
                ListItem all = new ListItem("Todos los Ajustadores", "0");
                UserList.Items.Insert(0,all);              
                loadajustador();
                ajustador = true;
                pr = false;
                UserList.SelectedValue = "0";
            }
            else if (ajustador == true && Request.Form["Tipo"] == "2")
            {
               // System.Diagnostics.Debug.WriteLine("reutilizar ajustes");

            }
            else if (pr == true && (Request.Form["Tipo"] == "1" || Request.Form["Tipo"] == "0"))
            {
               // System.Diagnostics.Debug.WriteLine("reutilizar pr");

            }
            else
            {
                UserList.Items.Clear();
                ListItem all = new ListItem("Todos los Empleados", "0");
                UserList.Items.Insert(0, all);
               // System.Diagnostics.Debug.WriteLine("limpiar pr");


                loadall();
                ajustador = false;
                pr = true;
                UserList.SelectedValue = "0";
            }
            
        }
        protected void loadajustador()
        {

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                    "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            // Define a query
            String consulta = "";
           // System.Diagnostics.Debug.WriteLine("ajustador");

            
            consulta = "Select DISTINCT name,firstname, idemployee from users join role_user on users.id=role_user.user_id where role_id<9 order by idemployee";
            ListItem all = new ListItem("Todos los Ajustadores", "0");
            UserList.Items.Insert(0, all);
            
            NpgsqlCommand command = new NpgsqlCommand(consulta, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                ListItem lst = new ListItem(dr[2].ToString() + " " + dr[0].ToString() + " " + dr[1].ToString(), dr[2].ToString());
                UserList.Items.Insert(UserList.Items.Count - 1, lst);
            }

            conn.Close();
        }
        protected void loadall()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                  "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            // Define a query
            String consulta = "Select name,firstname, idemployee from users order by idemployee";
           // System.Diagnostics.Debug.WriteLine("pr");

            NpgsqlCommand command = new NpgsqlCommand(consulta, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                UserList.SelectedIndex = 0;
                ListItem lst = new ListItem(dr[2].ToString() + " " + dr[0].ToString() + " " + dr[1].ToString(), dr[2].ToString());
                UserList.Items.Insert(UserList.Items.Count - 1, lst);
            }
            conn.Close();
        }
        protected void loadTable()
        {
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }
        protected void loadData()
        {          
            String tipo = Request.Form["Tipo"];
            if (tipo == "1"){
                produccion();
            }
            if (tipo == "0"){
                herramentaje();
            }
            if (tipo == "2"){
               ajustes();
            }


        }
        public String Createquery()
        {
            String tipo = Request.Form["Tipo"];
            String User = Request.Form["UserList"];
            String Job = Request.Form["JobNumber"];
            String Customer = Request.Form["Customer"];
            String PartNumber = Request.Form["Partn"];
            String FechaF = Request.Form["Fecha_final"];
            String FechaI = Request.Form["Fecha_inicio"];
            String queryF = "select users.idemployee, users.name,  operations.job, clients.customer, pieces.part_number, operations.operation_service, workcenters.num_machine, activity1.created_at, process_logs.created_at, users.firstname,activity1.activity ,process_logs.activity" +
                " from process_logs" +
                " join processes on process_logs.process_id = processes.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id" +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id" +
                " join process_logs as activity1 on activity1.id = process_logs.parent_id" +
                " join users on process_logs.user_id = users.id" +
                " where (activity1.activity = 'ih' and(process_logs.activity = 'crp' or process_logs.activity = 'fh'))";
      
            if (User != "0"){
                queryF += " and users.idemployee= '" + User + "' ";              
            }
            if (Request.Form["prueba23"] != ""){
                queryF += " and operations.job= '" + Request.Form["prueba23"].Trim() + "' ";   
            }
            if (Customer != "") {
                queryF += " and clients.customer= '" + Customer.Trim() + "' ";   
            }
            if (PartNumber != "") {
                queryF += " and  pieces.part_number= '" + PartNumber.Trim() + "' ";
            }
          
            if (FechaI != ""){
                  queryF += " and (activity1.created_at >= '" + FechaI + "' ) ";
                }

            if ( FechaF != ""){
                DateTime fs = DateTime.Parse(FechaF);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);

                queryF += " and (process_logs.created_at < '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "' ) ";
            }               
                query = queryF;
           
                return queryF;
         }
        public String Createquery2()
        {
            String tipo = Request.Form["Tipo"];
            String User = Request.Form["UserList"];
            String Job = Request.Form["JobNumber"];
            String Customer = Request.Form["Customer"];
            String PartNumber = Request.Form["Partn"];
            String FechaF = Request.Form["Fecha_final"];
            String FechaI = Request.Form["Fecha_inicio"];
            String queryF = "select users.idemployee, users.name,  operations.job, clients.customer, pieces.part_number, operations.operation_service, " +
                " workcenters.num_machine, activity1.created_at, process_logs.created_at, processes.status, users.firstname,activity1.activity ," +
                " process_logs.activity, user_processes.id, operations.standar_time, mealtime, mealtime2, user_processes.productivity" +
                " from process_logs" +
                " join processes on process_logs.process_id = processes.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id" +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id" +
                " join process_logs as activity1 on activity1.id = process_logs.parent_id" +
                " join users on process_logs.user_id = users.id" +
                " join user_processes on process_logs.user_process_id= user_processes.id " +
                " where activity1.activity = 'pr' and(process_logs.activity = 'crp' or process_logs.activity = 'cr')";

            if (User != "0")
            {
                queryF += " and users.idemployee= '" + User + "' ";
            }
            if (Request.Form["prueba23"] != "")
            {
                queryF += " and operations.job= '" + Request.Form["prueba23"].Trim() + "' ";
            }
            if (Customer != "")
            {
                queryF += " and clients.customer= '" + Customer.Trim() + "' ";
            }
            if (PartNumber != "")
            {
                queryF += " and  pieces.part_number= '" + PartNumber.Trim() + "' ";
            }

            if (FechaI != "")
            {
                queryF += " and (activity1.created_at >= '" + FechaI + "' ) ";
            }

            if (FechaF != "")
            {
                DateTime fs = DateTime.Parse(FechaF);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);

                queryF += " and (process_logs.created_at < '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "' ) ";
            }
            query = queryF;
            System.Diagnostics.Debug.WriteLine("consulta " + queryF);

            return queryF;
        }
        public String Createquery3()
        {
            String tipo = Request.Form["Tipo"];
            String User = Request.Form["UserList"];
            String Job = Request.Form["JobNumber"];
            String Customer = Request.Form["Customer"];
            String PartNumber = Request.Form["Partn"];
            String FechaF = Request.Form["Fecha_final"];
            String FechaI = Request.Form["Fecha_inicio"];

            String queryF = "select users.idemployee, users.name,  operations.job, clients.customer, pieces.part_number, operations.operation_service, " +
                "workcenters.num_machine, activity1.created_at, process_logs.created_at, processes.status, users.firstname,activity1.activity ," +
                "process_logs.activity, user_processes.id, rejected_processes.aceptada" +
                " from process_logs" +
                " join processes on process_logs.process_id = processes.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id" +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id" +
                " join process_logs as activity1 on activity1.id = process_logs.parent_id" +
                " join users on process_logs.user_id = users.id" +
                " join user_processes on process_logs.user_process_id= user_processes.id " +
                " JOIN rejected_processes on rejected_processes.process_id=processes.id" +
                " where (activity1.activity = 'ia' and(process_logs.activity = 'crp' or process_logs.activity = 'fa') or ((activity1.activity = 'il' or activity1.activity = 'ial') and(process_logs.activity = 'ls' or process_logs.activity = 'ln' ) ) ) ";
            if (User != "0")
            {
                queryF += " and users.idemployee= '" + User + "' ";
            }
            if (Request.Form["prueba23"] != "")
            {
                queryF += " and operations.job= '" + Request.Form["prueba23"].Trim() + "' ";
            }
            if (Customer != "")
            {
                queryF += " and clients.customer= '" + Customer.Trim() + "' ";
            }
            if (PartNumber != "")
            {
                queryF += " and  pieces.part_number= '" + PartNumber.Trim() + "' ";
            }

            if (FechaI != "")
            {
                queryF += " and (activity1.created_at >= '" + FechaI + "' ) ";
            }

            if (FechaF != "")
            {
                DateTime fs = DateTime.Parse(FechaF);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);

                queryF += " and (process_logs.created_at < '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "' ) ";
            }
            query = queryF;
            System.Diagnostics.Debug.WriteLine(query);
            return queryF;
        }
        protected void herramentaje()
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                   "Password=tecmaq;Database=tecmaq;");
                conn.Open();
            // Define a query
            System.Diagnostics.Debug.WriteLine("Consulta: " + Createquery());

            NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                tabla2.Controls.Clear();
                StringBuilder t = new StringBuilder();

            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("</tr>");
                t.Append("</thead>");
                t.Append("<tbody>");
                bool f1 = false;
                bool f2 = false;
                while (dr.Read())
                {
                    t.Append("<tr>");
                    t.Append("<td>" + dr[0]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[1]+" "+dr[9]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[2]);
                    t.Append("</td>");
                    t.Append("<td>" + dr[3]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[4]);
                    t.Append("</td>");
                    t.Append("<td>" + dr[5]);
                    t.Append("</td>");
                    t.Append("<td>" + dr[6]);
                    t.Append("</td>");
                   
                    DateTime d = DateTime.Now;
                    try
                    {
                        d = DateTime.Parse(dr[7].ToString());
                        t.Append("<td>" + d.ToString("dd/MM/yyyy"));
                        t.Append("</td>");
                        
                        t.Append("<td>" + $"{ d: HH: mm: ss}");
                        t.Append("</td>");
                    f1 = true;
                    } catch (FormatException)
                    {
                        t.Append("<td>");
                        t.Append("</td>");
                       
                        t.Append("<td>");
                        t.Append("</td>");

                }
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d2 = DateTime.Parse(dr[8].ToString());
                        t.Append("<td>" + d2.ToString("dd/MM/yyyy"));
                        t.Append("</td>");
                        t.Append("</td>");
                        t.Append("<td>" + $"{ d2: HH: mm: ss}");
                        f2 = true;
                    }
                    catch (FormatException)
                    {
                        t.Append("<td> ");
                        t.Append("</td>");
                        t.Append("</td>");
                        t.Append("<td>");

                    }
                    if (f1 == true && f2 == true)
                    {
                        t.Append("</td>");
                        t.Append("</td>");
                        t.Append("<td> " + (d2 - d).ToString() + "");
                        t.Append("</td>");
                    }
                    else
                    {

                        t.Append("</td>");
                        t.Append("</td>");
                        t.Append("<td>");
                        t.Append("</td>");
                    }
                }


                t.Append("</tr>");
                t.Append("</tbody>");
                t.Append("<tfoot>");
                t.Append("<tr>");

                t.Append("<tr>");

            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IH");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FH");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("</td>");
                t.Append("</tr>");
                t.Append("</tfoot>");
                t.Append("</table>");

                Literal s = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s);

                conn.Close();
            }
        protected void produccion()
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                   "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                // Define a query
                NpgsqlCommand command = new NpgsqlCommand(Createquery2(), conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                tabla2.Controls.Clear();
                StringBuilder t = new StringBuilder();

                t.Append("<table class='table-players display table-striped nowrap 'cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
                t.Append("<thead>");
                t.Append("<tr>");
                t.Append("<td class='t_header'>ID");
                t.Append("</td>");
                t.Append("<td class='t_header'>NOMBRE");
                t.Append("</td>");
                t.Append("<td class='t_header'>JOB");
                t.Append("</td>");
                t.Append("<td class='t_header'>CUSTOMER");
                t.Append("</td>");
                t.Append("<td class='t_header'>PART NUMBER");
                t.Append("</td>");
                t.Append("<td class='t_header'>OP");
                t.Append("</td>");
                t.Append("<td class='t_header'>CT");
                t.Append("</td>");
                t.Append("<td class='t_header'>FECHA PR");
                t.Append("</td>");
                t.Append("<td class='t_header'>HORA PR");
                t.Append("</td>");
                t.Append("<td class='t_header'>FECHA CRP/CR");
                t.Append("</td>");
                t.Append("<td class='t_header'>HORA CRP/CR");
                t.Append("</td>");
                t.Append("<td class='t_header'>TOTAL");
                t.Append("</td>");
                t.Append("<td class='t_header'>EFICIENCIA");
                t.Append("</td>");
                t.Append("</tr>");
                t.Append("</thead>");
                t.Append("<tbody>");

                while (dr.Read())
                {
                    String Activity = dr[12].ToString();
                    String Activity2 = dr[11].ToString();
                    DateTime d2 = DateTime.Parse(dr[7].ToString());     
                     if (dr[8].ToString() != "")
                        {
                            d2 = DateTime.Parse(dr[8].ToString());
                        }
                    DateTime m1 = Convert.ToDateTime(dr[15].ToString());
                    DateTime m2 = Convert.ToDateTime(dr[16].ToString());
                   
                    t.Append("<tr>");

                    t.Append("<td>" + dr[0]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[1]+" "+dr[10]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[2]);
                    t.Append("</td>");
                    t.Append("<td>" + dr[3]);
                    t.Append("</td>");
                    t.Append("<td> " + dr[4]);
                    t.Append("</td>");
                    t.Append("<td>" + dr[5]);
                    t.Append("</td>");
                    t.Append("</td>");
                    t.Append("<td>" + dr[6]);
                    t.Append("</td>");
                    t.Append("</td>");
                   DateTime d = new DateTime();
                try
                {
                    d = DateTime.Parse(dr[7].ToString());
                    t.Append("<td>" + d.ToString("dd/MM/yyyy"));
                    t.Append("</td>");
                    t.Append("<td>" + $"{ d: HH: mm: ss}");
                    t.Append("</td>");
                    d2 = DateTime.Parse(dr[8].ToString());
                    t.Append("<td>" + d2.ToString("dd/MM/yyyy"));
                    t.Append("</td>");
                    t.Append("<td>" + $"{ d2: HH: mm: ss}");
                    t.Append("</td>");
                    TimeSpan result = d2.Subtract(d);
                    t.Append("<td>"+result);
                    t.Append("</td>");

                }
                    catch (FormatException)
                    {
                        t.Append("<td>");
                        t.Append("</td>");
                        t.Append("<td>");
                        t.Append("</td>");
                        t.Append("<td>");
                        t.Append("</td>");
                        t.Append("<td>");
                        t.Append("</td>");
                        t.Append("<td>");
                        t.Append("</td>");
                 
                }
             
              //  double getEfi = getEficiencia(cantidad(getCantidad(dr[13].ToString(), dr[11].ToString()), dr[11].ToString()), getDouble(dr[14].ToString()), Truncate(result.TotalMinutes, 2), Activity2, d2, d, m1, m2, Activity2);

                    t.Append("<td> " + Decimal.Round(Convert.ToDecimal(Convert.ToDouble(dr[17]))) + "%");
                    t.Append("</td>");

                }


                t.Append("</tr>");
                t.Append("</tbody>");
                t.Append("<tfoot>");
                t.Append("<tr>");

                t.Append("<tr>");

                t.Append("<td class='t_header'>ID");
                t.Append("</td>");
                t.Append("<td class='t_header'>NOMBRE");
                t.Append("</td>");
                t.Append("<td class='t_header'>JOB");
                t.Append("</td>");
                t.Append("<td class='t_header'>CUSTOMER");
                t.Append("</td>");
                t.Append("<td class='t_header'>PART NUMBER");
                t.Append("</td>");
                t.Append("<td class='t_header'>OP");
                t.Append("</td>");
                t.Append("<td class='t_header'>CT");
                t.Append("</td>");
                t.Append("<td class='t_header'>FECHA PR");
                t.Append("</td>");
                t.Append("<td class='t_header'>HORA PR");
                t.Append("</td>");
                t.Append("<td class='t_header'>FECHA CRP/CR");
                t.Append("</td>");
                t.Append("<td class='t_header'>HORA CRP/CR");
                t.Append("</td>");
                t.Append("<td class='t_header'>TOTAL");
                t.Append("</td>");
                t.Append("<td class='t_header'>EFICIENCIA");
                t.Append("</td>");
                t.Append("</tr>");
                t.Append("</tfoot>");
                t.Append("</table>");

                Literal s = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s);

                conn.Close();
            }
        protected void ajustes()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
               "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            // Define a query
            NpgsqlCommand command = new NpgsqlCommand(Createquery3(), conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            System.Diagnostics.Debug.WriteLine(Createquery3());
            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server'id='table_players'  style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA INICIO IA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FINAL FA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>RESULTADO");
            t.Append("</td>");
        
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            bool f1 = false;
            bool f2 = false;
            while (dr.Read())
            {
                t.Append("<tr>");
                t.Append("<td>" + dr[0]);
                t.Append("</td>");
                t.Append("<td> " + dr[1] + " "+ dr[10]);
                t.Append("</td>");
                t.Append("<td> " + dr[2]);
                t.Append("</td>");
                t.Append("<td>" + dr[3]);
                t.Append("</td>");
                t.Append("<td> " + dr[4]);
                t.Append("</td>");
                t.Append("<td>" + dr[5]);
                t.Append("</td>");
                t.Append("<td>" + dr[6]);
                t.Append("</td>");
                
                DateTime d = DateTime.Now;
                try
                {
                    d = DateTime.Parse(dr[7].ToString());
                    t.Append("<td>" + d.ToString("dd/MM/yyyy"));
                    t.Append("</td>");
                   
                    t.Append("<td>" + $"{ d: HH: mm: ss}");
                    f1 = true;
                }
                catch (FormatException)
                {
                    t.Append("<td>");
                    t.Append("</td>");
                   
                    t.Append("<td>");

                }
                DateTime d2 = DateTime.Now;
                try
                {
                    d2 = DateTime.Parse(dr[8].ToString());
                    t.Append("<td>" + d2.ToString("dd/MM/yyyy"));
                    t.Append("</td>");
                 
                    t.Append("<td>" + $"{ d2: HH: mm: ss}");
                    f2 = true;
                }
                catch (FormatException)
                {
                    t.Append("<td> ");
                    t.Append("</td>");
                   
                    t.Append("<td>");

                }
                if (f1 == true && f2 == true)
                {
                    t.Append("</td>");
                    t.Append("</td>");
                    t.Append("<td> " + (d2 - d).ToString() + "");
                    t.Append("</td>");
                }
                else
                {

                    t.Append("</td>");
                 
                    t.Append("<td>");
                    t.Append("</td>");
                }
                if (dr[14].ToString() == "") {
                    t.Append("<td>" + "crp" + "");
                }
                else{

                if (Convert.ToBoolean(dr[14])){
                    t.Append("<td>" + "ls" + "");
                }
                else{
                    t.Append("<td>" + "ln" + "");
                }
                }
                t.Append("</td>");
            }

            
            t.Append("</tr>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
       
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA IA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO IA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO FA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>RESULTADO");
            t.Append("</td>");            
            t.Append("</tfoot>");
            t.Append("</table>");

            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);

            conn.Close();
        }
        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Eficiencias Operador");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de eficiencias por tipos";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);

            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
            if (Request.Form["Tipo"] == "0")
            {
                using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
                {
                    Rng.Value = "REPORTE EFICIENCIAS DE HERRAMENTAJE";
                    Rng.Style.Font.Size = 20;
                    Rng.Style.Font.Bold = true;
                }

            }
            if (Request.Form["Tipo"] == "1")
            {
                using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
                {
                    Rng.Value = "REPORTE EFICIENCIAS DE PRODUCCIÓN";
                    Rng.Style.Font.Size = 20;
                    Rng.Style.Font.Bold = true;
                }

            }
            if (Request.Form["Tipo"] == "2")
            {
                using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
                {
                    Rng.Value = "REPORTE EFICIENCIAS DE AJUSTES";
                    Rng.Style.Font.Size = 20;
                    Rng.Style.Font.Bold = true;
                }

            }

            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }


            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda

            encabezadoExcel(ws);


            //encabezados de la tabla documento
            ExcelRange parametros = ws.Cells["A3:S3"];
            ExcelRange encabezado = ws.Cells["A8:S8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
            ws.Cells["B8"].Value = "ID";
            ws.Cells["C8"].Value = "NOMBRE";
            ws.Cells["D8"].Value = "JOB";
            ws.Cells["E8"].Value = "CUSTOMER";
            ws.Cells["F8"].Value = "PART NUMBER";
            ws.Cells["G8"].Value = "OP";
            ws.Cells["H8"].Value = "CT";

            if (Request.Form["Tipo"] == "0")
            {
                ws.Cells["I8"].Value = "FECHA IH";
                ws.Cells["J8"].Value = "TIEMPO IH";
                ws.Cells["K8"].Value = "FECHA FH";
                ws.Cells["L8"].Value = "TIEMPO IH";
                ws.Cells["M8"].Value = "TOTAL";
            }
            if (Request.Form["Tipo"] == "1")
            {
                ws.Cells["I8"].Value = "FECHA PR";
                ws.Cells["J8"].Value = "HORA PR";
                ws.Cells["K8"].Value = "FECHA CRP/CR";
                ws.Cells["L8"].Value = "HORA CRP/CR";
                ws.Cells["M8"].Value = "TOTAL";
                ws.Cells["N8"].Value = "EFICIENCIA";
            }
            if (Request.Form["Tipo"] == "2")
            {
                ws.Cells["I8"].Value = "FECHA IA";
                ws.Cells["J8"].Value = "TIEMPO IA";
                ws.Cells["K8"].Value = "FECHA FA";
                ws.Cells["L8"].Value = "TIEMPO FA";
                ws.Cells["M8"].Value = "TOTAL";
                ws.Cells["N8"].Value = "STATUS";
            }

            parametros.AutoFitColumns();
            encabezado.AutoFitColumns();

            dataExcel(ws);

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteEficiencia" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }
        public void dataExcel(ExcelWorksheet ws)
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                             "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            // Define a query
            String queryf = "";
            if (Request.Form["Tipo"] == "0") {
                queryf = Createquery();
            }
            if (Request.Form["Tipo"] == "1")
            {
                queryf = Createquery2();
            }
            if (Request.Form["Tipo"] == "2")
            {
                queryf = Createquery3();
            }
                NpgsqlCommand command = new NpgsqlCommand(queryf, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            int excel = 9;
            if (Request.Form["Tipo"]=="0")
            {
                bool f1 = false;
                bool f2 = false;
                while (dr.Read())
                {
                    ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                    ws.Cells[String.Format("C{0}", excel)].Value = dr[1] + " " + dr[10];
                    ws.Cells[String.Format("D{0}", excel)].Value = dr[2];
                    ws.Cells[String.Format("E{0}", excel)].Value = dr[3];
                    ws.Cells[String.Format("F{0}", excel)].Value = dr[4];
                    ws.Cells[String.Format("G{0}", excel)].Value = dr[5];
                    ws.Cells[String.Format("H{0}", excel)].Value = dr[6];
                    DateTime d = DateTime.Now;
                    try
                    {
                        d = DateTime.Parse(dr[7].ToString());
                        ws.Cells[String.Format("I{0}", excel)].Value = d.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("J{0}", excel)].Value = $"{ d: HH: mm: ss}";
                        f1 = true;
                    }
                    catch (FormatException) { }
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d2 = DateTime.Parse(dr[8].ToString());
                        ws.Cells[String.Format("K{0}", excel)].Value = d2.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("L{0}", excel)].Value = $"{ d2: HH: mm: ss}";
                        f2 = true;
                    }
                    catch (FormatException) { }
                    if (f1 == true && f2 == true)
                    {
                        ws.Cells[String.Format("M{0}", excel)].Value = (d2 - d).ToString() + "";
                    }
                    excel++;
                }
            }
            if (Request.Form["Tipo"]=="1")
            {
                bool f1 = false;
                bool f2 = false;
                while (dr.Read())
                {
                    ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                    ws.Cells[String.Format("C{0}", excel)].Value = dr[1] + " " + dr[10];
                    ws.Cells[String.Format("D{0}", excel)].Value = dr[2];
                    ws.Cells[String.Format("E{0}", excel)].Value = dr[3];
                    ws.Cells[String.Format("F{0}", excel)].Value = dr[4];
                    ws.Cells[String.Format("G{0}", excel)].Value = dr[5];
                    ws.Cells[String.Format("H{0}", excel)].Value = dr[6];
                    DateTime d = DateTime.Now;
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d = DateTime.Parse(dr[7].ToString());
                        d2 = DateTime.Parse(dr[8].ToString());
                        ws.Cells[String.Format("I{0}", excel)].Value = d.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("J{0}", excel)].Value = $"{ d: HH: mm: ss}";
                        ws.Cells[String.Format("K{0}", excel)].Value = d2.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("L{0}", excel)].Value = $"{ d2: HH: mm: ss}";
                        TimeSpan result = d2.Subtract(d);
                        ws.Cells[String.Format("M{0}", excel)].Value =OutTime(result);
                        f1 = true;
                    }
                    catch (FormatException) { }
                    ws.Cells[String.Format("N{0}", excel)].Value = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(dr[17]))) + "%";
                    excel++;
                }
            }
            if (Request.Form["Tipo"]=="2")
            {
                bool f1 = false;
                bool f2 = false;
                while (dr.Read())
                {
                    ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                    ws.Cells[String.Format("C{0}", excel)].Value = dr[1]+" "+dr[10];
                    ws.Cells[String.Format("D{0}", excel)].Value = dr[2];
                    ws.Cells[String.Format("E{0}", excel)].Value = dr[3];
                    ws.Cells[String.Format("F{0}", excel)].Value = dr[4];
                    ws.Cells[String.Format("G{0}", excel)].Value = dr[5];
                    ws.Cells[String.Format("H{0}", excel)].Value = dr[6];
                    DateTime d = DateTime.Now;
                    try
                    {
                        d = DateTime.Parse(dr[7].ToString());
                        ws.Cells[String.Format("I{0}", excel)].Value = d.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("J{0}", excel)].Value = $"{ d: HH: mm: ss}";
                        f1 = true;
                    }
                    catch (FormatException) { }
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d2 = DateTime.Parse(dr[8].ToString());
                        ws.Cells[String.Format("K{0}", excel)].Value = d2.ToString("dd/MM/yyyy");
                        ws.Cells[String.Format("L{0}", excel)].Value = $"{ d2: HH: mm: ss}";
                        f2 = true;
                    }
                    catch (FormatException) { }
                    if (f1 == true && f2 == true)
                    {
                        ws.Cells[String.Format("M{0}", excel)].Value = (d2 - d).ToString() + "";
                    }
                    ws.Cells[String.Format("N{0}", excel)].Value = dr[9];
                    excel++;
                }
            }



            conn.Close();
        }
        public void encabezadoExcel(ExcelWorksheet ws)
        {
            String tipo = Request.Form["Tipo"];
            String User = Request.Form["UserList"];
            String Job = Request.Form["JobNumber"];
            String Customer = Request.Form["Customer"];
            String PartNumber = Request.Form["Partn"];
            String FechaF = Request.Form["Fecha_final"];
            String FechaI = Request.Form["Fecha_inicio"];


            if (tipo == "0")
            {
                ws.Cells["C3"].Value = "TIPO:";
                ws.Cells["D3"].Value = "HERRAMENTAJE";
            }
            if (tipo == "1")
            {
                ws.Cells["C3"].Value = "TIPO:";
                ws.Cells["D3"].Value = "PRODUCCIÓN";
            }

            if (User != "0")
            {
                ws.Cells["F3"].Value = "ID EMPLEADO:";
                ws.Cells["G3"].Value = User;
            }
            else
            {
                ws.Cells["F3"].Value = "ID EMPLEADO:";
                ws.Cells["G3"].Value = "TODOS LOS EMPLEADOS";
            }
            if (Request.Form["prueba23"] != "")
            {
                ws.Cells["C4"].Value = "JOB:";
                ws.Cells["D4"].Value = Request.Form["prueba23"].Trim();
            }
            else
            {
                ws.Cells["C4"].Value = "JOB:";
                ws.Cells["D4"].Value = "N/A";
            }
            if (Customer != "")
            {
                ws.Cells["C5"].Value = "CUSTOMER:";
                ws.Cells["D5"].Value = Customer;
            }
            else
            {
                ws.Cells["C5"].Value = "CUSTOMER:";
                ws.Cells["D5"].Value = "N/A";
            }

            if (PartNumber != "")
            {
                ws.Cells["C6"].Value = "PART NUMBER:";
                ws.Cells["D6"].Value = PartNumber;
            }
            else
            {
                ws.Cells["C6"].Value = "PART NUMBER:";
                ws.Cells["D6"].Value = "N/A";
            }
            if (FechaI != "" && FechaF != "")
            {
                ws.Cells["F4"].Value = "DESDE:";
                ws.Cells["G4"].Value = FechaI;
                ws.Cells["F5"].Value = "HASTA:";
                ws.Cells["G5"].Value = FechaF;
            }
            if (FechaI != "" && FechaF == "")
            {
                ws.Cells["F4"].Value = "DESDE:";
                ws.Cells["G4"].Value = FechaI;
                ws.Cells["F5"].Value = "HASTA:";
                ws.Cells["G5"].Value = "N/A";
            }

            if (FechaI == "" && FechaF != "")
            {
                ws.Cells["F4"].Value = "DESDE:";
                ws.Cells["G4"].Value = "N/A";
                ws.Cells["F5"].Value = "HASTA:";
                ws.Cells["G5"].Value = FechaF;
            }
            if (FechaI == "" && FechaF == "")
            {
                ws.Cells["F4"].Value = "DESDE:";
                ws.Cells["G4"].Value = "N/A";
                ws.Cells["F5"].Value = "HASTA:";
                ws.Cells["G5"].Value = "N/A";
            }

            ws.Cells["M3"].Value = "SOLICITADO POR:";
            ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];

        }
        protected void GeneratePDF(object sender, System.EventArgs e)
        {
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                           "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                // Define a query
                String queryf = "";
                if (Request.Form["Tipo"] == "0")
                {
                    queryf = Createquery();
                }
                if (Request.Form["Tipo"] == "1")
                {
                    queryf = Createquery2();
                }
                else
                {
                    queryf = Createquery3();
                }
                NpgsqlCommand command = new NpgsqlCommand(queryf, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();

                Document document = new Document(PageSize.A4, 10, 10, 10, 10);

                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
               
                string imageURL = Server.MapPath(".") + "/Img/logoc.png";
            
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                //Resize image depend upon your need
                jpg.ScaleToFit(140f, 120f);
                //Give space before image
                jpg.SpacingBefore = 10f;
                //Give some space after the image
                jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_LEFT;
                Paragraph paragraph = null; 
                if (Request.Form["tipo"] == "0")
                {
                paragraph = new Paragraph("REPORTE EFICIENCIAS DE HERRAMENTAJE");

                }
                if (Request.Form["tipo"] == "1")
                {
                paragraph = new Paragraph("REPORTE EFICIENCIAS DE PRODUCCIÓN");
                }
                if (Request.Form["tipo"] == "2")
                {
                paragraph = new Paragraph("REPORTE EFICIENCIAS DE AJUSTES");
                }

                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Font.Size = 15;
                document.Add(jpg);
                document.Add(paragraph);

                Paragraph sesion = new Paragraph("Solicitó: " + Session["User"] + " " + Session["LastName"]);
                sesion.Alignment = Element.ALIGN_RIGHT;
                sesion.Font.Size = 12;
                document.Add(sesion);
                String tipo = "Producción";
                int columnas = 9;
                if (Request.Form["Tipo"] == "0")
                {
                    tipo = "Herramentaje";
                    columnas = 12;
                }
                if (Request.Form["Tipo"] == "1")
                {
                    tipo = "Producción";
                    columnas = 13;
                }
                if (Request.Form["Tipo"] == "2")
                {
                    tipo = "Ajustes";
                    columnas = 13;
                }
                document.Add(Chunk.NEWLINE);
                PdfPTable parametros = new PdfPTable(7);
                parametros.AddCell("Tipo");
                parametros.AddCell("Job");              
                parametros.AddCell("Customer");
                parametros.AddCell("Part Number");
                parametros.AddCell("Empleado");
                parametros.AddCell("Desde");
                parametros.AddCell("Hasta");
                parametros.AddCell(tipo);
                parametros.AddCell(Request.Form["prueba23"]);
                parametros.AddCell(Request.Form["Customer"]);
                parametros.AddCell(Request.Form["Partn"]);
                parametros.AddCell(Request.Form["UserList"]);
                parametros.AddCell(Request.Form["Fecha_inicio"]);
                parametros.AddCell(Request.Form["Fecha_final"]);

                document.Add(parametros);
              
             
                document.Add(Chunk.NEWLINE);


                //header de la tabla
                PdfPTable table = new PdfPTable(columnas);
                   table.AddCell("ID");
                   table.AddCell("Nombre");
                   table.AddCell("Job");
                   table.AddCell("Customer");
                   table.AddCell("Part Number");
                   table.AddCell("OP");
                   table.AddCell("CT");
                   if (Request.Form["Tipo"] == "0")
                   {
                       table.AddCell("Fecha IH");
                       table.AddCell("Tiempo IH");
                       table.AddCell("Fecha FH");
                       table.AddCell("Tiempo FH");
                       table.AddCell("Total");
                   }
                    if (Request.Form["Tipo"] == "1")
                    {
                           table.AddCell("FFECHA PR");
                    table.AddCell("HORA PR");
                    table.AddCell("FFECHA CRP/CR");
                    table.AddCell("HORA CRP/CR");
                    table.AddCell("TOTAL");
                    table.AddCell("Eficiencia");

                    }
                    if (Request.Form["Tipo"] == "2")
                    {
                        table.AddCell("Fecha IA");
                        table.AddCell("Tiempo IA");
                        table.AddCell("Fecha FA");
                        table.AddCell("Tiempo FA");
                        table.AddCell("Total");
                        table.AddCell("Status");
                }

                // Esta es la primera fila


                //cuerpa de la tabla

                while (dr.Read())
                {
                    DateTime d2 = DateTime.Parse(dr[7].ToString());
                    if (dr[8].ToString() != "")
                    {
                        d2 = DateTime.Parse(dr[8].ToString());
                    }
                    table.AddCell("" + dr[0].ToString());
                    table.AddCell("" + dr[1].ToString());
                    table.AddCell("" + dr[2].ToString());
                    table.AddCell("" + dr[3].ToString());
                    table.AddCell("" + dr[4].ToString());
                    table.AddCell("" + dr[5].ToString());
                    table.AddCell("" + dr[6].ToString());
                    if (Request.Form["Tipo"] == "0")
                    {
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + $"{ d: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }
                        try
                        {
                            d2 = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d2.ToString("d"));
                            table.AddCell("" + $"{ d2: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }

                        table.AddCell(""+ (d2-d).ToString());
                    }
                    if (Request.Form["Tipo"] == "1"){
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + $"{ d: HH: mm: ss}");
                            d2 = DateTime.Parse(dr[8].ToString());
                            table.AddCell("" + d2.ToString("d"));
                            table.AddCell("" + $"{ d2: HH: mm: ss}");
                            TimeSpan result = d2.Subtract(d);
                            table.AddCell("" + OutTime(result));
                            table.AddCell("" + dr[8].ToString()+"%");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("" + dr[8].ToString() + "%");
                        }
                    }
                    if (Request.Form["Tipo"] == "2")
                    {
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + $"{ d: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }
                        try
                        {
                            d2 = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d2.ToString("d"));
                            table.AddCell("" + $"{ d2: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }

                        table.AddCell("" + (d2 - d).ToString());
                        table.AddCell("" + Decimal.Round(Convert.ToDecimal(Convert.ToDouble(dr[17]))));
                    }
                }
                conn.Close();
                document.Add(table);
                //fin del cuerpo del pdf
                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";

                
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "Reporte eficiencia de operador "+DateTime.Now.ToString("d")+ ".pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
        }
        public double getEficiencia(double cantidad, double taktime, double total, String activity, DateTime inicio, DateTime final, DateTime m1, DateTime m2, String activityprev)
        {
            System.Globalization.CultureInfo.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            TimeSpan meal1 = Convert.ToDateTime(m1.ToString()).TimeOfDay;
            TimeSpan meal2 = Convert.ToDateTime(m2.ToString()).TimeOfDay;
            TimeSpan meal1a = Convert.ToDateTime(m1.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan meal2a = Convert.ToDateTime(m2.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan init = inicio.TimeOfDay;
            TimeSpan fin = final.TimeOfDay;
            double eficiencia = 0.0;

            if (activity == "pr" || (activity == "crp" && activityprev == "pr"))
            {
                if (cantidad > 0)
                {
                    if (total >= 1.0)
                    {
                        // System.Diagnostics.Debug.WriteLine("dentro");
                        // System.Diagnostics.Debug.WriteLine(eficiencia);
                        if (TimeSpan.Compare(init, meal1) == (-1) && TimeSpan.Compare(fin, meal2a) == (1))
                        {
                            total = total - 60;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                            //  System.Diagnostics.Debug.WriteLine(init + " 2 comidas" + " " + cantidad + " " + taktime + " " + total+" "+ eficiencia+"act "+activity );

                        }
                        else if (TimeSpan.Compare(init, meal1) == (-1) && TimeSpan.Compare(fin, meal1a) == (1) && (TimeSpan.Compare(fin, meal2) == (-1)))
                        {
                            total = total - 30;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                            //   System.Diagnostics.Debug.WriteLine(init +" 1er comidas" + " "+cantidad+" "+taktime+" "+total + " " + eficiencia + "act " + activity);

                        }
                        else if (TimeSpan.Compare(init, meal2) == (-1) && TimeSpan.Compare(fin, meal2a) == (1) && TimeSpan.Compare(init, meal1a) == (1))
                        {
                            total = total - 30;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                            //  System.Diagnostics.Debug.WriteLine(init + " 2da comidas" + " " + cantidad + " " + taktime + " " + total + " " + eficiencia + "act " + activity);

                        }
                        else
                        {
                            eficiencia = (((cantidad + 1) * taktime) / total) * 100;
                            //   System.Diagnostics.Debug.WriteLine(init + " sin comidas" + " " + cantidad + " " + taktime + " " + eficiencia + "act " + activity);

                        }

                    }
                }
            }
            if (eficiencia == 0)
            {
                return 0.0;
            }
            else
            {
                return Truncate(eficiencia, 2);
            }

        }
        public String getCantidad(String userp, String activity)
        {
            String con = "0";
            if (activity == "pr" || activity == "crp")
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand("SELECT COUNT(id)/2 FROM cycles WHERE user_process_id='" + userp + "'", conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                if (dr.Read())
                {
                    con = dr[0].ToString();
                    // System.Diagnostics.Debug.WriteLine("Cantidad");
                }
                conn.Close();
            }
            return con;
        }
        public int cantidad(String cant, String activity)
        {
            int cantidad = 0;
            if (activity == "pr" || activity == "crp")
            {
                cantidad = Convert.ToInt32(cant);
            }
            return cantidad;
        }
        public double getDouble(String s)
        {
            double numero = 0.0;
            if (s != "")
            {
                numero = Convert.ToDouble(s);
            }

            return numero;
        }
        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }
}