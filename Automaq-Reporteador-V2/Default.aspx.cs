﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class _Default : Page
    {
        static IMongoClient client = new MongoClient("mongodb://localhost:27017");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                Response.Redirect("Jobs.aspx");
            }
           // update();
        }

        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }

        public void Authentication(object sender, EventArgs e)
        {

            String u = Request.Form["txt_user"];
            String p = Request.Form["txt_pass"];
            var database = client.GetDatabase("admin");
            var user = database.GetCollection<BsonDocument>("users");
            var queryable = user.AsQueryable();
            var filter = Builders<BsonDocument>.Filter.Eq("idemployee", u);
            var res = user.Find(filter).FirstOrDefault();
            if (res != null)
            {
                Checkpass(p, res.GetValue(7).ToString(), res, "Admin");
            }
            else
            {
                Response.Write("<script>alert('Usuario no existe favor de verificar');</script>");
            }

        }
        public void update()
        {
            var database = client.GetDatabase("admin");
            var user = database.GetCollection<BsonDocument>("Update");
            var filter = Builders<BsonDocument>.Filter.Eq("id", 1);
            var res = user.Find(filter).FirstOrDefault();
            DateTime f1 = res.GetValue(2).ToLocalTime();
            DateTime f2 = res.GetValue(3).ToLocalTime();


            Update.Text =f1.ToString() ;
            Update2.Text = f2.ToString();

        }

        public bool Checkpass(String mypass, String dbpass, BsonDocument usuario, String role)
        {
            bool validate = BCrypt.Net.BCrypt.Verify(mypass, dbpass);
            if (validate == true)
            {
                Session["User"]         = usuario.GetValue(2);
                Session["LastName"]     = usuario.GetValue(3);
                Session["IdEmployee"]   = usuario.GetValue(4);
                Session["Role"]         = role;
                Session["param"]        = "";
                Response.Redirect("Jobs.aspx");

            }
            else
            {
                Response.Write("<script>alert('Contraseña incorrecta favor de verificar');</script>");
            }
            return validate; ;
        }

        public bool Checkpass(String mypass, String dbpass,String user, String last, String employee, String role)
        {
            bool validate = BCrypt.Net.BCrypt.Verify(mypass, dbpass);
            if (validate == true)
            {
                Session["User"] = user;
                Session["LastName"] = last;
                Session["IdEmployee"] = employee;
                Session["Role"] = role;
                Session["param"] = "";
                Response.Redirect("Jobs.aspx");

            }
            else
            {
                Response.Write("<script>alert('Contraseña incorrecta favor de verificar');</script>");
            }
            return validate; ;
        }
    }

    }