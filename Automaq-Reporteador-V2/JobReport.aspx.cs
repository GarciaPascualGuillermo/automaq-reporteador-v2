﻿using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class JobReport : System.Web.UI.Page
    {
         List<String> ops = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
              //  System.Diagnostics.Debug.WriteLine(ops.Count+"holi");
               
                    SessionDetails();
                    loadPlantilla();
                    loadTable();
                
            }
            else {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }
        public void SessionDetails()
        {
            String IdU = Request.QueryString["JN"];
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
            hea.InnerHtml = "REPORTE JOB " + IdU;
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {

        }

        public void Find(object sender, EventArgs e)
        {
          
        }

        public void Atras(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }

        public String Createquery()
        {
            String query = "SELECT distinct idemployee,users.name, users.firstname, num_machine,operations.job, customer, pieces.part_number, operations.operation_service," +
                " (user_processes.end - user_processes.start) as totol, user_processes.scraps, user_processes.productivity, process_logs.parent_id, process_logs.created_at, process_logs.id, process_logs.activity," +
                " operations.standar_time, process_logs.user_process_id, mealtime, mealtime2, actividad.created_at, actividad.activity" +
            " FROM user_processes" +
            " full join process_logs on process_logs.user_process_id = user_processes.id " +
            " full join processes on processes.id = process_logs.process_id" +
            " join operations on processes.operation_id = operations.id" +
            " join pieces on operations.piece_id = pieces.id" +
            " join clients on pieces.client_id = clients.id" +
            " join users on process_logs.user_id = users.id" +
            " join workcenters on process_logs.workcenter_id = workcenters.id" +
            " full join process_logs as actividad on process_logs.parent_id= actividad.id " +
            " where operations.job='"+ Request.QueryString["JN"] + "'  and (process_logs.activity = 'ih' or process_logs.activity = 'fh' or process_logs.activity = 'ia'  or process_logs.activity = 'fa'  or process_logs.activity = 'il'  or process_logs.activity = 'ls'  or process_logs.activity = 'ln'  or process_logs.activity = 'cr'  or process_logs.activity = 'ial' or process_logs.activity = 'pr' or process_logs.activity = 'crp') ORDER BY process_logs.created_at";
            System.Diagnostics.Debug.WriteLine(query);
            return query;
           
        }


        protected void loadTable()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            
            // Execute the query and obtain a result set
           
            
            StringBuilder t = new StringBuilder();
            StringBuilder herramentaje = new StringBuilder();
            StringBuilder ajuste = new StringBuilder();
            StringBuilder produccion = new StringBuilder();

            TimeSpan hf = new TimeSpan();
            TimeSpan af = new TimeSpan();
            TimeSpan pf = new TimeSpan();
            TimeSpan tm = new TimeSpan();
            int piezas  = 0;
            int scraps  = 0;

            
            tabla2.Controls.Clear();
            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server' style='width: 100%;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td >NUM");
            t.Append("</td>");
            t.Append("<td >NOMBRE");
            t.Append("</td>");           
            t.Append("<td >CT");
            t.Append("</td>");
            t.Append("<td >OP");
            t.Append("</td>");
            t.Append("<td >ACT");
            t.Append("</td>");
            t.Append("<td >FECHA");
            t.Append("</td>");
            t.Append("<td >HORA");
            t.Append("</td>");
            t.Append("<td >TIEMPO");
            t.Append("</td>");
            t.Append("<td >EFI");
            t.Append("</td>");
            t.Append("<td >CANT");
            t.Append("</td>");
            t.Append("<td >SCRAPS");
            t.Append("</td>");
            t.Append("<td >PAROS");
            t.Append("</td>");
            t.Append("<td >TIEMPO MUERTO");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
            t.Clear();
            for(int i=0; i<ops.Count; i++)
            {
                TimeSpan he = new TimeSpan();
                TimeSpan aj = new TimeSpan();
                TimeSpan pr = new TimeSpan();
                String ope = "";
                int cant = 0;
                int scr = 0;
                int prueba = 0;
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
                NpgsqlDataReader tr = command.ExecuteReader();
               // System.Diagnostics.Debug.WriteLine(i+"---------------------------------");
                NpgsqlDataReader dr = tr;
                herramentaje.Append("<tr  class='table-dark text-dark'>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td>"+ "</td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("<td></td>");
                herramentaje.Append("</tr>");
                bool flag = false;
                while (dr.Read())
                {
                    
                    if (dr[7].ToString() == ops[i])
                    {
                        flag = true;
                       // System.Diagnostics.Debug.WriteLine(dr[7] + " y " + ops[i] + " cuenta" + ops.Count);
                        String Activity = dr[14].ToString();
                        String Activity2 = dr[20].ToString();
                        DateTime d2 = DateTime.Parse(dr[12].ToString());
                        if (Activity == "ih" || Activity == "fh" || (Activity == "crp" && dr[20].ToString() == "ih"))
                        {
                            if (dr[20].ToString() != ""){
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);
                           // System.Diagnostics.Debug.WriteLine("fecha inicio: " + d2 + " fecha final " + d);
                            herramentaje.Append("<tr>");
                            herramentaje.Append("<td>" + dr[0] + "");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>" + dr[1] + " " + dr[2] + " ");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>" + dr[3]);
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>" + dr[7] + "");
                            herramentaje.Append("</td>");
                            ope = dr[7].ToString();
                            herramentaje.Append("<td>" + dr[14].ToString().ToUpper() + "");
                            herramentaje.Append("</td>");
                            //herramentaje.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                            herramentaje.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>" + $"{ d: HH: mm: ss}");
                            herramentaje.Append("</td>");
                            if (Activity == "fh" || Activity == "crp")
                            {
                                herramentaje.Append("<td>" + OutTime(result) + "");
                                herramentaje.Append("</td>");
                                he = he + result;
                                hf = hf + result;
                            }
                            else
                            {
                                herramentaje.Append("<td>");
                                herramentaje.Append("</td>");
                            }

                            herramentaje.Append("<td>");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>");
                            herramentaje.Append("</td>");
                            herramentaje.Append("<td>");
                            herramentaje.Append("</td>");
                            herramentaje.Append("</tr>");
                        }
                        if (Activity=="ia" || Activity == "fa" || Activity == "il" || Activity == "ial" || Activity == "ls" || Activity == "ln" )
                        {
                            if (dr[20].ToString() != "")
                            {
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);
                          //  System.Diagnostics.Debug.WriteLine("fecha inicio: " + d2 + " fecha final " + d);
                            ajuste.Append("<tr>");
                            ajuste.Append("<td>" + dr[0] + "");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + dr[1] + " " + dr[2] + " ");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + dr[3]);
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + dr[7] + "");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + dr[14].ToString().ToUpper() + "");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>" + $"{ d: HH: mm: ss}");
                            ajuste.Append("</td>");
                            if (Activity == "fa" || Activity == "crp" || Activity == "ls" || Activity == "ln")
                            {
                                ajuste.Append("<td>" + OutTime(result) + "");
                                ajuste.Append("</td>");
                                aj = aj + result;
                                af = af + result;
                            }
                            else
                            {
                                ajuste.Append("<td>");
                                ajuste.Append("</td>");
                            }
                            ajuste.Append("<td>");
                            ajuste.Append("</td>");
                          
                            if (Activity == "fa" ){
                                ajuste.Append("<td style='text-align:center'>1");
                                ajuste.Append("</td>");
                                cant = cant + 1;
                                
                                prueba++;
                            }
                            else{
                                ajuste.Append("<td>");
                                ajuste.Append("</td>");
                            }
                            ajuste.Append("<td>");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>");
                            ajuste.Append("</td>");
                            ajuste.Append("<td>");
                            ajuste.Append("</td>");
                            ajuste.Append("</tr>");
                        }
                        if (Activity == "pr" || Activity == "cr" || (Activity == "crp" && dr[20].ToString() == "pr"))
                        {
                            if (dr[20].ToString() != "")
                            {
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);
                           // System.Diagnostics.Debug.WriteLine("fecha inicio: " + d2 + " fecha final " + d);
                            produccion.Append("<tr>");
                            produccion.Append("<td>" + dr[0] + "");
                            produccion.Append("</td>");
                            produccion.Append("<td>" + dr[1] + " " + dr[2] + " ");
                            produccion.Append("</td>");
                            produccion.Append("<td>" + dr[3]);
                            produccion.Append("</td>");
                            produccion.Append("<td>" + dr[7] + "");
                            produccion.Append("</td>");
                            produccion.Append("<td>" + dr[14].ToString().ToUpper() + "");
                            produccion.Append("</td>");
                            produccion.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                            produccion.Append("</td>");
                            produccion.Append("<td>" + $"{ d: HH: mm: ss}");
                            produccion.Append("</td>");
                            if (Activity == "cr" || Activity == "crp") {
                                produccion.Append("<td>" + OutTime(result) + "");
                                produccion.Append("</td>");
                                pr = pr + result;
                                pf = pf + result;
                            }
                            else {
                                produccion.Append("<td>");
                                produccion.Append("</td>");
                            }
                            double getEfi = getEficiencia(cantidad(getCantidad(dr[16].ToString(), dr[20].ToString()), dr[20].ToString()), getDouble(dr[15].ToString()), Truncate(result.TotalMinutes, 2), dr[20].ToString(), d2, d, m1, m2, Activity2);
                             if(getEfi<= 0)
                            {
                                produccion.Append("<td style='text-align:center'>");
                                produccion.Append("</td>");
                            }
                            else
                            {
                                // produccion.Append("<td style='text-align:center'>" + Decimal.Round(Convert.ToDecimal(getEfi)) + "%");
                                produccion.Append("<td style='text-align:center'>" + Decimal.Round(Convert.ToDecimal(Convert.ToDouble(dr[10]))) + "%");
                                produccion.Append("</td>");
                            }
                               
                                                  
                            if(Activity== "pr" )
                            {
                                produccion.Append("<td style='text-align:center'>" );
                                produccion.Append("</td>");
                            }
                            else
                            {
                                cant = cant + Convert.ToInt32(getCantidad(dr[16].ToString(), dr[20].ToString()));

                                if (Convert.ToInt32(getCantidad(dr[16].ToString(), dr[20].ToString())) > 0)
                                {
                                    produccion.Append("<td style='text-align:center'>" + getCantidad(dr[16].ToString(), dr[20].ToString()));

                                }
                                else
                                {
                                    produccion.Append("<td style='text-align:center'>" );

                                }
                                produccion.Append("</td>");
                            }
                            if (dr[9].ToString() != "")
                            {

                            
                            if (Convert.ToInt32(dr[9].ToString()) > 0)
                            {
                                produccion.Append("<td style='text-align:center'>" + dr[9] + "");
                            }
                            else
                            {
                                produccion.Append("<td style='text-align:center'>");
                            }
                            scr = scr + Convert.ToInt32(dr[9].ToString());
                            }
                            else
                            {
                                produccion.Append("<td style='text-align:center'>");
                                scr = scr ;
                            }
                            
                            produccion.Append("</td>");
                            produccion.Append("<td>");
                            produccion.Append("</td>");
                            produccion.Append("<td>");
                            produccion.Append("</td>");
                            produccion.Append("</tr>");
                        }
                    }
                }
                if (flag){
                    herramentaje.Append("<tr  class='resume'>");
                    herramentaje.Append("<td >HERRAMENTAJES</td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td>" + OutTime(he) + "</td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td></td>");
                    herramentaje.Append("<td style='text-align:center'>" + getTiempoMuerto(ope, Request.QueryString["JN"]) +"</td>");
                    tm = tm + getTiempoMuerto(ope, Request.QueryString["JN"]);
                    herramentaje.Append("</tr>");
                    ajuste.Append("<tr  class='resume'>");
                    ajuste.Append("<td>AJUSTES</td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td style='text-align:center'></td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td>" + OutTime(aj) + "</td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td style='text-align:center'>" + prueba + "</td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td></td>");
                    ajuste.Append("<td style='text-align:center'>" + OutTime(getTiempoMuerto2(ope, Request.QueryString["JN"])) + "</td>");
                    tm = tm + getTiempoMuerto2(ope, Request.QueryString["JN"]);
                    ajuste.Append("</tr>");
                    produccion.Append("<tr  class='resume'>");
                    produccion.Append("<td>PRODUCCIÓN</td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td>" +OutTime( pr) + "</td>");
                    produccion.Append("<td></td>");
                    produccion.Append("<td style='text-align:center'>" + cant+"</td>");
                    piezas =  cant;
                    produccion.Append("<td style='text-align:center'>" + scr+"</td>");
                    scraps = scraps + scr; 
                    produccion.Append("<td></td>");
                    produccion.Append("<td></td>");
                    produccion.Append("</tr>");
                    produccion.Append("<tr  class='table-dark text-dark'>");
                    produccion.Append("<td colspan='13'>&nbsp;</td>");
                    produccion.Append("</tr>");
                    Literal h=new Literal { Text = herramentaje.ToString() };
                    tabla2.Controls.Add(h);
                    Literal a = new Literal { Text = ajuste.ToString() };
                    tabla2.Controls.Add(a);
                    Literal p = new Literal { Text = produccion.ToString() };
                    tabla2.Controls.Add(p);
                    herramentaje.Clear();
                    ajuste.Clear();
                    produccion.Clear();
                }
                    conn.Close();              
            }
            t.Append("<tr  class=''>");
            t.Append("<td colspan='13'>&nbsp;</td>");
            t.Append("</tr>");
            t.Append("<tr class='footer'>");
            t.Append("<td colspan='13'style='text-align:center; vertical-align:middle' ><H4>RESUMEN JOB</H4></td>");                     
            t.Append("</tr>");
            t.Append("<tr >");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >HERRAMENTAJES</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+OutTime(hf)+"</td>");
            t.Append("</tr>");
            t.Append("<tr>");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >AJUSTE</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+OutTime(af)+"</td>");
            t.Append("</tr>");
            t.Append("<tr >");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >PRODUCCION</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+OutTime(pf)+"</td>");
            t.Append("</tr>");
            t.Append("<tr >");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >TIEMPOS MUERTOS</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+OutTime(tm)+"</td>");
            t.Append("</tr>");
            t.Append("<tr >");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >PIEZAS HECHAS</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+piezas+"</td>");
            t.Append("</tr>");
            t.Append("<tr >");
            t.Append("<td colspan='6'style='text-align:center; vertical-align:middle' >SCRAPS</td>");
            t.Append("<td colspan='7'style='text-align:center; vertical-align:middle' >"+scraps+"</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            t.Append("");
            t.Append("");
            Literal s1= new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s1);
            
        }

        //PLANTILLA
        protected void loadPlantilla()
        {
            String job = Request.QueryString["JN"];
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT distinct work_center, customer ,(operation_service), operations.description, standar_time, run_method, operations.sequence,operations.job, jobs.quantity, pieces.part_number, operations.created_at" +
                " , job_operation FROM operations " +
                " join pieces on pieces.id = operations.piece_id " +
                " join clients on pieces.client_id = clients.id" +
                " join jobs on jobs.num_job= operations.job " +
                " where operations.job='"+job+ "' order by job_operation asc";
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            Operations.Controls.Clear();
           
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players2 display table-striped nowrap cellspacing='0' runat='server' style='width: 60%;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");           
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>DESCRIPTION");
            t.Append("</td>");
            t.Append("<td class='t_header'>TAK TIME");
            t.Append("</td>");
            t.Append("<td class='t_header'>RUN METHOD");
            t.Append("</td>");
            t.Append("<td class='t_header'>STATUS");
            t.Append("</td>");         
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            while (dr.Read())
            {
                resumen.Text = "&nbsp; &nbsp; &nbsp; &nbsp; CUSTOMER: " + dr[1] + ", JOB: " + dr[7] + ", PART NUMBER: " + dr[9] + ", QUANTITY: " + dr[8];
                
                t.Append("<tr>");
                t.Append("<td>" + dr[0] + "");
                t.Append("</td>");
               
                t.Append("<td>" + dr[2] + "");
                t.Append("</td>");
                ops.Add(dr[2].ToString());
                t.Append("<td>" + dr[3] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[4] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[5] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[6] + "");
                t.Append("</td>");
                t.Append("</tr>");

            }
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            Operations.Controls.Add(s);
            conn.Close();
        }
        //PLANTILLA FINAL


        public double getEficiencia(double cantidad, double taktime, double total, String activity, DateTime inicio, DateTime final, DateTime m1, DateTime m2, String activityprev)
        {
            System.Globalization.CultureInfo.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            TimeSpan meal1 = Convert.ToDateTime(m1.ToString()).TimeOfDay;
            TimeSpan meal2 = Convert.ToDateTime(m2.ToString()).TimeOfDay;
            TimeSpan meal1a = Convert.ToDateTime(m1.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan meal2a = Convert.ToDateTime(m2.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan init = inicio.TimeOfDay;
            TimeSpan fin = final.TimeOfDay;
            double eficiencia = 0.0;
            
            if (activity == "pr" || (activity == "crp" && activityprev=="pr")){
                if (cantidad > 0){                  
                    if (total >= 1.0) {
                       // System.Diagnostics.Debug.WriteLine("dentro");
                       // System.Diagnostics.Debug.WriteLine(eficiencia);
                        if (TimeSpan.Compare(init, meal1) == (-1) && TimeSpan.Compare(fin, meal2a) == (1))
                        {
                            total = total - 60;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                          //  System.Diagnostics.Debug.WriteLine(init + " 2 comidas" + " " + cantidad + " " + taktime + " " + total+" "+ eficiencia+"act "+activity );

                        }
                        else if (TimeSpan.Compare(init, meal1) == (-1) && TimeSpan.Compare(fin, meal1a) == (1) && (TimeSpan.Compare(fin, meal2) == (-1)))
                        {
                            total = total - 30;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                          //   System.Diagnostics.Debug.WriteLine(init +" 1er comidas" + " "+cantidad+" "+taktime+" "+total + " " + eficiencia + "act " + activity);

                        }
                        else if (TimeSpan.Compare(init, meal2) == (-1) && TimeSpan.Compare(fin, meal2a) == (1) && TimeSpan.Compare(init, meal1a) == (1))
                        {
                            total = total - 30;
                            eficiencia = (((cantidad) * taktime) / total) * 100;
                            //  System.Diagnostics.Debug.WriteLine(init + " 2da comidas" + " " + cantidad + " " + taktime + " " + total + " " + eficiencia + "act " + activity);

                        }
                        else
                        {
                            eficiencia = (((cantidad + 1) * taktime) / total) * 100;
                           //   System.Diagnostics.Debug.WriteLine(init + " sin comidas" + " " + cantidad + " " + taktime + " " + eficiencia + "act " + activity);

                        }

                    }
                }
            }
            if (eficiencia == 0)
            {
                return 0.0;
            }
            else
            {
               return Truncate(eficiencia, 2);
            }
           
        }


        public TimeSpan getTiempoMuerto(String op, String job)
        {
            String query = "SELECT  operations.job,  operations.operation_service, process_logs.created_at, process_logs.activity, actividad.created_at, actividad.activity" +
                " FROM user_processes" +
                " join process_logs on process_logs.user_process_id = user_processes.id" +
                " join processes on processes.id = user_processes.process_id" +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on operations.piece_id = pieces.id" +
                " join clients on pieces.client_id = clients.id" +
                " join users on process_logs.user_id = users.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id" +
                " join process_logs as actividad on actividad.id = process_logs.parent_id" +
                " where(process_logs.activity = 'ia') and actividad.activity != 'crp' and actividad.activity != 'ia' and operations.operation_service='"+op+"' and  operations.job = '"+job+"'" +
                " order by user_processes.created_at desc ";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            TimeSpan s = new TimeSpan();
            while (dr.Read())
            {
                DateTime f1 = DateTime.Parse(dr[2].ToString());
                DateTime f2 = DateTime.Parse(dr[4].ToString());
                s = s + f1.Subtract(f2);
            }
            conn.Close();

            return s;
        }

        public TimeSpan getTiempoMuerto2(String op, String job)
        {
            String query = "SELECT  operations.job,  operations.operation_service, process_logs.created_at, process_logs.activity, actividad.created_at, actividad.activity" +
                 " FROM user_processes" +
                " join process_logs on process_logs.user_process_id = user_processes.id" +
                " join processes on processes.id = user_processes.process_id" +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on operations.piece_id = pieces.id" +
                " join clients on pieces.client_id = clients.id" +
                " join users on process_logs.user_id = users.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id" +
                " join process_logs as actividad on actividad.id = process_logs.parent_id" +
                " where(process_logs.activity = 'pr') and actividad.activity != 'crp' and actividad.activity != 'ia' and operations.operation_service='" + op + "' and  operations.job = '" + job + "'" +
                " order by user_processes.created_at desc ";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            TimeSpan s = new TimeSpan();
            while (dr.Read())
            {
                DateTime f1 = DateTime.Parse(dr[2].ToString());
                DateTime f2 = DateTime.Parse(dr[4].ToString());
                s = s + f1.Subtract(f2);
            }
            conn.Close();
            return s;
        }


        public String getCantidad(String userp, String activity)
        {
            String con = "0";
            if ((activity == "pr" || activity == "crp") && userp != "")
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand("SELECT COUNT(id) FROM cycles WHERE user_process_id='" + userp + "' and  message ='12345'", conn);
                // Execute the query and obtain a result set
                System.Diagnostics.Debug.WriteLine("SELECT COUNT(id) FROM cycles WHERE user_process_id='" + userp + "' and  message ='12345'");
                NpgsqlDataReader dr = command.ExecuteReader();
                if (dr.Read())
                {
                    con = dr[0].ToString();
                   // System.Diagnostics.Debug.WriteLine("Cantidad");
                }
                conn.Close();
            }
            return con;
        }
        public int cantidad(String cant, String activity)
        {
            int cantidad = 0;
            if (activity == "pr" || activity == "crp")
            {
                cantidad = Convert.ToInt32(cant);
            }
            return cantidad;
        }
        public double getDouble(String s)
        {
            double numero = 0.0;
            if (s != "")
            {
                numero = Convert.ToDouble(s);
            }

            return numero;
        }

        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }

        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de JOB");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de JOB";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);
            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
            using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
            {
                String job = Request.QueryString["JN"];
                Rng.Value = "REPORTE DE JOB "+job+"";
                Rng.Style.Font.Size = 20;
                Rng.Style.Font.Bold = true;
            }
            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }
            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda
             //ws.View.ShowGridLines = false;

             encabezadoExcel(ws, 9);
            

            //encabezados de la PLANTILLA
     
            

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteJOB"+ Request.QueryString["JN"] +"_" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }

        public void encabezadoExcel(ExcelWorksheet ws, int excel)
        {
            String job = Request.QueryString["JN"];
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT distinct work_center, customer , operation_service, operations.description, standar_time, run_method, operations.sequence,operations.job, jobs.quantity, pieces.part_number" +
                " FROM operations " +
                " join pieces on pieces.id = operations.piece_id " +
                " join clients on pieces.client_id = clients.id" +
                " join jobs on jobs.num_job= operations.job " +
                " where operations.job='" + job + "' order by operation_service";
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();

            ExcelRange encabezado = ws.Cells["A8:G8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.BlueViolet);
            ws.Cells["B8"].Value = "CT";
            ws.Cells["C8"].Value = "OP";
            ws.Cells["D8"].Value = "DESCRIPTION";
            ws.Cells["E8"].Value = "TAK TIME";
            ws.Cells["F8"].Value = "RUN METHOD";
            ws.Cells["G8"].Value = "STATUS";                  
            encabezado.AutoFitColumns();
            while (dr.Read())
            {
                ws.Cells["C3"].Value = "CUSTOMER:";
                ws.Cells["D3"].Value = dr[1];
                ws.Cells["F3"].Value = "JOB:";
                ws.Cells["G3"].Value = dr[7];
                ws.Cells["C4"].Value = "PART NUMBER:";
                ws.Cells["D4"].Value = dr[9];
                ws.Cells["C5"].Value = "QUANTITY:";
                ws.Cells["D5"].Value = dr[8];               
                ws.Cells["M3"].Value = "SOLICITADO POR:";
                ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];
                ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                ws.Cells[String.Format("C{0}", excel)].Value = dr[2];
                ws.Cells[String.Format("D{0}", excel)].Value = dr[3];
                ws.Cells[String.Format("E{0}", excel)].Value = dr[4];
                ws.Cells[String.Format("F{0}", excel)].Value = dr[5];
                ws.Cells[String.Format("G{0}", excel)].Value = dr[6];
                excel++;

            }

            conn.Close();
            dataExcel(ws, excel);
        }


        public void dataExcel(ExcelWorksheet ws, int excel)
        {
            //System.Diagnostics.Debug.WriteLine(excel);
            excel = excel + 3;
            TimeSpan hf = new TimeSpan();
            TimeSpan af = new TimeSpan();
            TimeSpan pf = new TimeSpan();
            TimeSpan tm = new TimeSpan();
            int piezas = 0;
            int scraps = 0;
            List<List<Object>> herramentaje = new List<List<object>>();
            List<List<Object>> ajuste = new List<List<object>>();
            List<List<Object>> produccion = new List<List<object>>();

            for (int i = 0; i < ops.Count; i++)
            {

                TimeSpan he = new TimeSpan();
                TimeSpan aj = new TimeSpan();
                TimeSpan pr = new TimeSpan();
                String ope = "";
                int cant = 0;
                int scr = 0;
                int prueba = 0;
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " + "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
                NpgsqlDataReader tr = command.ExecuteReader();
                // System.Diagnostics.Debug.WriteLine(i+"---------------------------------");
                NpgsqlDataReader dr = tr;
                bool flag = false;
               
                while (dr.Read())
                {
                   
                    if (dr[7].ToString() == ops[i])
                    {
                        List<Object> h = new List<object>();
                        List<Object> a = new List<object>();
                        List<Object> p = new List<object>();
                        flag = true;
                        String Activity = dr[14].ToString();
                        String Activity2 = dr[20].ToString();
                        DateTime d2 = DateTime.Parse(dr[12].ToString());
                        if (Activity == "ih" || Activity == "fh" || (Activity == "crp" && dr[20].ToString() == "ih"))
                        {
                            if (dr[20].ToString() != "")
                            {
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);

                            // System.Diagnostics.Debug.WriteLine("fecha inicio: " + d2 + " fecha final " + d);
                            
                            h.Add(dr[0]);
                            h.Add(dr[1] + " " + dr[2] + " ");
                            h.Add(dr[3]);
                            h.Add(dr[7] + "");
                            ope = dr[7].ToString();
                            h.Add(dr[14].ToString().ToUpper() + "");
                            h.Add(d.ToString("dd/MM/yyyy") );
                            h.Add( $"{ d: HH: mm: ss}");
                            if (Activity == "fh" || Activity == "crp")
                            {
                                h.Add(OutTime(result));
                                he = he + result;
                                hf = hf + result;
                            }
                            else
                            {
                                h.Add("");                            
                            }
                            h.Add("");
                            h.Add("");
                            h.Add("");
                            h.Add("");
                            h.Add("");
                            herramentaje.Add(h);
                          
                        }
                        if (Activity == "ia" || Activity == "fa" || Activity == "il" || Activity == "ial" || Activity == "ls" || Activity == "ln" || (Activity == "crp" && dr[20].ToString() == "ia"))
                        {
                            if (dr[20].ToString() != "")
                            {
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);
                      
                            
                            a.Add(dr[0]);
                            a.Add(dr[1] + " " + dr[2] );
                            a.Add(dr[3]);
                            a.Add(dr[7] + "");
                            a.Add(dr[14].ToString().ToUpper());
                            a.Add(d.ToString("dd/MM/yyyy") + "");
                            a.Add($"{ d: HH: mm: ss}");
                            if (Activity == "fa" || Activity == "crp" || Activity == "ls" || Activity == "ln")
                            {
                                a.Add(OutTime(result));
                                aj = aj + result;
                                af = af + result;
                            }
                            else
                            {
                                a.Add("");
                               
                            }
                            a.Add("");
                            

                            if (Activity == "fa" )
                            {
                                a.Add("1");
                                cant = cant + 1;

                                prueba++;
                            }
                            else
                            {
                                a.Add("");
                            }
                            
                            a.Add("");
                            a.Add("");
                            a.Add("");
                            ajuste.Add(a);
                        }
                        if (Activity == "pr" || Activity == "cr" || (Activity == "crp" && dr[20].ToString() == "pr"))
                        {
                            if (dr[20].ToString() != "")
                            {
                                d2 = DateTime.Parse(dr[19].ToString());
                            }
                            DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                            DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                            DateTime d = DateTime.Parse(dr[12].ToString());
                            TimeSpan result = d.Subtract(d2);
                            // System.Diagnostics.Debug.WriteLine("fecha inicio: " + d2 + " fecha final " + d);
                            
                            p.Add(dr[0]);
                            p.Add(dr[1] + " " + dr[2] + " ");
                            p.Add(dr[3]);
                            p.Add(dr[7] + "");
                            p.Add(dr[14].ToString().ToUpper() + "");
                            p.Add(d.ToString("dd/MM/yyyy") + "");
                            p.Add($"{ d: HH: mm: ss}");
                            if (Activity == "cr" || Activity == "crp")
                            {
                                p.Add(OutTime(result));
                                pr = pr + result;
                                pf = pf + result;
                            }
                            else
                            {
                                p.Add("");
                            }
                            double getEfi = getEficiencia(cantidad(getCantidad(dr[16].ToString(), dr[20].ToString()), dr[20].ToString()), getDouble(dr[15].ToString()), Truncate(result.TotalMinutes, 2), dr[20].ToString(), d2, d, m1, m2, Activity2);
                            if (getEfi <= 0)
                            {
                                p.Add("");
                            }
                            else
                            {
                                p.Add(Decimal.Round(Convert.ToDecimal(getEfi)) + "%");
                            }
                            if (Activity == "pr")
                            {
                                p.Add("");
                            }
                            else
                            {
                                cant = cant + Convert.ToInt32(getCantidad(dr[16].ToString(), dr[20].ToString()));
                                if (Convert.ToInt32(getCantidad(dr[16].ToString(), dr[20].ToString())) > 0)
                                {
                                    p.Add(getCantidad(dr[16].ToString(), dr[20].ToString()));
                                }
                                else
                                {
                                    p.Add("");
                                }
                            }
                            p.Add(dr[9]);
                            scr = Convert.ToInt32(dr[9].ToString());
                            p.Add("");
                            p.Add("");
                            produccion.Add(p);
                           // p.Clear();
                        }
                    }
                }
                if (flag)
                {
                    List<Object> h = new List<object>();
                    List<Object> a = new List<object>();
                    List<Object> p = new List<object>();
                    h.Add("HERRAMENTAJES");
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add(he.ToString());
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add("");
                    h.Add(getTiempoMuerto(ope, Request.QueryString["JN"]).ToString());
                    tm = tm + getTiempoMuerto(ope, Request.QueryString["JN"]);
                    herramentaje.Add(h);
                    a.Add("AJUSTES");
                    a.Add("");
                    a.Add("");
                    a.Add("");
                    a.Add("");
                    a.Add("");
                    a.Add("");
                    a.Add(""+ aj.ToString());
                    a.Add("");
                    a.Add(""+prueba);
                    a.Add("");
                    a.Add("");
                    a.Add( getTiempoMuerto2(ope, Request.QueryString["JN"]).ToString());
                    ajuste.Add(a);
                    tm = tm + getTiempoMuerto2(ope, Request.QueryString["JN"]);
                    p.Add("PRODUCCIÓN");
                    p.Add("");
                    p.Add("");
                    p.Add("");
                    p.Add("");
                    p.Add("");
                    p.Add("");
                    p.Add(pr.ToString());
                    p.Add("");
                    p.Add(cant);
                    piezas =  cant;
                    p.Add(scr);
                    scraps = scraps + scr;
                    p.Add("");
                    p.Add("");
                    produccion.Add(p);
                    //p.Clear();

                    /*                   
                    aqui se imprimen los arrays 
                    
                    */
                    ExcelRange encabezado = ws.Cells["A"+excel+":N"+excel];
                    encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
                    encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[String.Format("B{0}", excel)].Value = "NUM";
                    ws.Cells[String.Format("C{0}", excel)].Value = "NOMBRE";
                    ws.Cells[String.Format("D{0}", excel)].Value = "CT";
                    ws.Cells[String.Format("E{0}", excel)].Value = "OP";
                    ws.Cells[String.Format("F{0}", excel)].Value = "ACT";
                    ws.Cells[String.Format("G{0}", excel)].Value = "FECHA";
                    ws.Cells[String.Format("H{0}", excel)].Value = "HORA";
                    ws.Cells[String.Format("I{0}", excel)].Value = "TIEMPO";
                    ws.Cells[String.Format("J{0}", excel)].Value = "EFI";
                    ws.Cells[String.Format("K{0}", excel)].Value = "CANT";
                    ws.Cells[String.Format("L{0}", excel)].Value = "SCRAPS";
                    ws.Cells[String.Format("M{0}", excel)].Value = "PAROS";
                    ws.Cells[String.Format("N{0}", excel)].Value = "TIEMPO MUERTO";
                    encabezado.AutoFitColumns();


                    excel++;
                    foreach (List<Object> x in herramentaje)
                    {
                        if (x.Count() > 0)
                        {
                            ws.Cells[String.Format("B{0}", excel)].Value = x[0];
                            ws.Cells[String.Format("C{0}", excel)].Value = x[1];
                            ws.Cells[String.Format("D{0}", excel)].Value = x[2];
                            ws.Cells[String.Format("E{0}", excel)].Value = x[3];
                            ws.Cells[String.Format("F{0}", excel)].Value = x[4];
                            ws.Cells[String.Format("G{0}", excel)].Value = x[5];
                            ws.Cells[String.Format("H{0}", excel)].Value = x[6];
                            ws.Cells[String.Format("I{0}", excel)].Value = x[7];
                            ws.Cells[String.Format("J{0}", excel)].Value = x[8];
                            ws.Cells[String.Format("K{0}", excel)].Value = x[9];
                            ws.Cells[String.Format("L{0}", excel)].Value = x[10];
                            ws.Cells[String.Format("M{0}", excel)].Value = x[11];
                            ws.Cells[String.Format("N{0}", excel)].Value = x[12];
                            excel++;
                        }
                    }
                    excel++;
                    foreach (List<Object> x in ajuste)
                    {
                        if (x.Count() > 0)
                        {
                            ws.Cells[String.Format("B{0}", excel)].Value = x[0];
                            ws.Cells[String.Format("C{0}", excel)].Value = x[1];
                            ws.Cells[String.Format("D{0}", excel)].Value = x[2];
                            ws.Cells[String.Format("E{0}", excel)].Value = x[3];
                            ws.Cells[String.Format("F{0}", excel)].Value = x[4];
                            ws.Cells[String.Format("G{0}", excel)].Value = x[5];
                            ws.Cells[String.Format("H{0}", excel)].Value = x[6];
                            ws.Cells[String.Format("I{0}", excel)].Value = x[7];
                            ws.Cells[String.Format("J{0}", excel)].Value = x[8];
                            ws.Cells[String.Format("K{0}", excel)].Value = x[9];
                            ws.Cells[String.Format("L{0}", excel)].Value = x[10];
                            ws.Cells[String.Format("M{0}", excel)].Value = x[11];
                            ws.Cells[String.Format("N{0}", excel)].Value = x[12];
                            excel++;
                        }
                    }
                    excel++;
                    foreach (List<Object> x in produccion)
                    {
                        if (x.Count() > 0)
                        {
                            ws.Cells[String.Format("B{0}", excel)].Value = x[0];
                            ws.Cells[String.Format("C{0}", excel)].Value = x[1];
                            ws.Cells[String.Format("D{0}", excel)].Value = x[2];
                            ws.Cells[String.Format("E{0}", excel)].Value = x[3];
                            ws.Cells[String.Format("F{0}", excel)].Value = x[4];
                            ws.Cells[String.Format("G{0}", excel)].Value = x[5];
                            ws.Cells[String.Format("H{0}", excel)].Value = x[6];
                            ws.Cells[String.Format("I{0}", excel)].Value = x[7];
                            ws.Cells[String.Format("J{0}", excel)].Value = x[8];
                            ws.Cells[String.Format("K{0}", excel)].Value = x[9];
                            ws.Cells[String.Format("L{0}", excel)].Value = x[10];
                            ws.Cells[String.Format("M{0}", excel)].Value = x[11];
                            ws.Cells[String.Format("N{0}", excel)].Value = x[12];
                            excel++;
                        }
                    }
                    excel++;
                    herramentaje.Clear();
                    ajuste.Clear();
                    produccion.Clear();
                }
            

                conn.Close();
            }

            using (ExcelRange Rng = ws.Cells["F" + excel + ":F" + excel])
            {
                String job = Request.QueryString["JN"];
                Rng.Value = "RESUMEN ";
                Rng.Style.Font.Size = 20;
                Rng.Style.Font.Bold = true;
            }
            ExcelRange resumen = ws.Cells["A" + excel + ":N" + excel];
            resumen.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            resumen.Style.Fill.PatternType = ExcelFillStyle.Solid;
            resumen.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
            excel++;

            ws.Cells[String.Format("F{0}", excel)].Value = "HERRAMENTAJES";
            ws.Cells[String.Format("I{0}", excel)].Value = hf.ToString();
            excel++;
            ws.Cells[String.Format("F{0}", excel)].Value = "AJUSTE";
            ws.Cells[String.Format("I{0}", excel)].Value = af.ToString();
            excel++;
            ws.Cells[String.Format("F{0}", excel)].Value = "PRODUCCION";
            ws.Cells[String.Format("I{0}", excel)].Value = pf.ToString();
            excel++;
            ws.Cells[String.Format("F{0}", excel)].Value = "TIEMPOS MUERTOS";
            ws.Cells[String.Format("I{0}", excel)].Value = tm.ToString();
            excel++;
            ws.Cells[String.Format("F{0}", excel)].Value = "PIEZAS HECHAS";
            ws.Cells[String.Format("I{0}", excel)].Value = piezas.ToString();
            excel++;
            ws.Cells[String.Format("F{0}", excel)].Value = "SCRAPS";
            ws.Cells[String.Format("I{0}", excel)].Value = scraps.ToString();

        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
           // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
           // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
           // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
           // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10){
                h = "0" + totalhours;
            }else{
                h = ""+totalhours;
            }if (minutes < 10){
                m = "0"+ minutes ;
            }else{
                m= ""+minutes;
            }if (seconds < 10){
                se = "0" + seconds;
            }else{
                se=""+seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }   
}