﻿using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class Conteos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                if (Session["param"].ToString() != "")
                {
                   // loadData(Createquery2(Session["param"].ToString()));

                }
                else
                {
                    loadTable();
                }

            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
            // Session["param"] = "";
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
            Session["param"] = "";
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
            Session["param"] = "";
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Conteos.aspx");

        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {

            Job.Value = "";
            Customer.Value = "";
            Partn.Value = "";
            Fecha_final.Value = "";
            Fecha_inicio.Value = "";
        }

        public void Find(object sender, EventArgs e)
        {
            //Createquery();
            //Response.Redirect("Conteos.aspx");
            loadData();
        }

        public void loadEmployee()
        {

        }

        protected void loadTable()
        {
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display  nowrap' cellspacing='0' id='table_players'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD RECHAZADAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD PRODUCIDAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>% COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
         
            
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD RECHAZADAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD PRODUCIDAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>% COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");

            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");

            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }

        protected void loadData()
        {
            String param = "";

            if (Request.Form["Fecha_inicio"] != "")
            {

                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                //fs = fs.AddHours(24).AddMinutes(59).AddSeconds(59);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "")
            {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != "")
            {
                param += " and  pieces.part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }
            if (Request.Form["Job"] != "")
            {
                param += " and  num_job= '" + Request.Form["Job"].Trim() + "' ";
            }
            if (Request.Form["limite"] != "")
            {
                param += "order by jobs.created_at desc ";
            }

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "select distinct num_job, jobs.created_at" +
                " from jobs " +
                " join operations on operations.job=num_job" +
                " JOIN processes on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id" +
                " where num_job is not null " + param;
              
            // System.Diagnostics.Debug.WriteLine("" + Createquery());
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            tabla2.Controls.Clear();
            int limit = Convert.ToInt32(Request.Form["limite"].ToString());
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display  nowrap' cellspacing='0' id='table_player' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD RECHAZADAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD PRODUCIDAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>% COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            int i = 0;
            Literal s1 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s1);
            t.Clear();
            int indice = 0;
            while (dr.Read())
            {
                int x= operations( dr[0].ToString(),  i, indice);
                indice = x;
                if (indice >= limit)
                {
                    break;
                }
                i++;
            }
            t.Append("");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD RECHAZADAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD PRODUCIDAS");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>% COMPLETADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");

            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }


        public int operations( String job, int i, int limit)
        {
            String param = "";

            if (Request.Form["Fecha_inicio"] != "")
            {

                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                //fs = fs.AddHours(24).AddMinutes(59).AddSeconds(59);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "")
            {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != "")
            {
                param += " and  pieces.part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }
          
          

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "select num_job, max(customer), max(jobs.created_at), max(pieces.part_number), operations.operation_service, max(operations.work_center), jobs.quantity, sum(processes.scraps) as scarpt, sum(processes.total_piece), count(cycles.id)/2" +
                " from jobs " +
                " full join operations on operations.job = num_job " +
                " JOIN processes on processes.operation_id = operations.id " +
                " join pieces on pieces.id = operations.piece_id " +
                " join clients on pieces.client_id = clients.id " +
                " join user_processes on processes.id = user_processes.process_id " +
                " join workcenters on processes.workcenter_id = workcenters.id " +
                " join cycles on cycles.user_process_id = user_processes.id " +
                " WHERE num_job is not null "+param+ " and num_job= '"+job+"' "+
                " GROUP BY num_job, jobs.quantity, operations.operation_service, num_machine " +
                " ORDER BY num_job, operation_service; " ;
            //System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            StringBuilder t1=new StringBuilder();
            StringBuilder t = new StringBuilder();
            String[] data = new string[11];
            data[8] = "0";
            Boolean flag = false;
            int count = 0;
            while (dr.Read())
            {
                if (dr[0].ToString() != "")
                {
                    data[0] = job;
                    t1.Append("<tr id='demo" + i + "' class='collapse cell-1 row-child'>");
                    t1.Append("<td class='des' colspan='1'><i class='fa fa-angle-up'></i></td>");
                    t1.Append("<td class='des' colspan='1'>" + dr[1] + "</td>");
                    data[1] = dr[1].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[2] + "</td>");
                    data[2] = dr[2].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[3] + "</td>");
                    data[3] = dr[3].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[4] + "</td>");

                    t1.Append("<td class='des' colspan='1'>" + dr[5] + "</td>");
                    t1.Append("<td class='des' colspan='1'>" + dr[6] + "</td>");
                    data[4] = dr[6].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[7] + "</td>");
                    if (dr[7].ToString() == "")
                    {
                        data[5] = "0";
                    }
                    else
                    {
                        data[5] = data[5];
                    }
                    t1.Append("<td class='des' colspan='1'>" + dr[9] + "</td>");
                    data[6] = dr[9].ToString();
                    t1.Append("<td class='des' colspan='1'>" + (Convert.ToInt32(data[5]) + Convert.ToInt32(dr[9])) + "</td>");
                    data[7] = (Convert.ToInt32(data[5]) + Convert.ToInt32(dr[9])).ToString();
                    double por = Truncate(((Convert.ToDouble(data[5]) + Convert.ToDouble(dr[9])) / (Convert.ToDouble(dr[6])) * 100), 2);
                    if (por <= 75)
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-danger'>" + por + "% </span></td>");
                    }
                    else if (por < 90 && por >75)
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-warning'>" + por + "% </span></td>");
                    }
                    else
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-success'>" + por + "% </span></td>");
                    }
                    //System.Diagnostics.Debug.WriteLine(data[0] + " op "+por);
                    t1.Append("<td class='des'></td>");

                    data[8] = (por+(Convert.ToDouble(data[8]))).ToString();
                    t1.Append("</tr>");
                    flag = true;
                    count++;
                }
            }
            if (flag)
            {
                t.Append("<tr data-toggle='collapse' data-target='#demo" + i + "'>");
                t.Append("<td class='text-center'>" + data[0] + "</td>");
                t.Append("<td class='text-center'>" + data[1] + "</td>");
                t.Append("<td class='text-center'>" + data[2] + "</td>");
                t.Append("<td class='text-center'>" + data[3] + "</td>");
                t.Append("<td class='text-center'>-</td>");
                t.Append("<td class='text-center'>-</td>");
                t.Append("<td class='text-center'>" + data[4] + "</td>");
                t.Append("<td class='text-center'>" + data[5] + "</td>");
                t.Append("<td class='text-center'>" + data[6] + "</td>");
                t.Append("<td class='text-center'>" + data[7] + "</td>");
                double porcentaje = Truncate(Convert.ToDouble(data[8]) / count,2);
                if (porcentaje <= 75)
                {
                    
                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-danger'>" + porcentaje + "% </span></td>");
                }
                else if (porcentaje <= 90 && porcentaje > 75)
                {
                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-warning'>" + porcentaje + "% </span></td>");
                }
                else if (porcentaje > 90)
                {
                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-success'>" + porcentaje + "% </span></td>");
                }
                t.Append("<td class='table-elipse' data-toggle='collapse' data-target='#demo" + i + "'><i class='fa fa-ellipsis-h text-black-50'></i></td>");
                t.Append("</tr>");
                Literal s = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s);
                t.Clear();
                Literal s1 = new Literal { Text = t1.ToString() };
                tabla2.Controls.Add(s1);
                t1.Clear();
                limit++;
               
                //System.Diagnostics.Debug.WriteLine("indice "+ i);
            }
           
            conn.Close();
            return limit;
        }

        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }



        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de Conteos Jobs");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de Conteos Jobs";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);
            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
            using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
            {
                Rng.Value = "REPORTE CONTEOS JOBS";
                Rng.Style.Font.Size = 20;
                Rng.Style.Font.Bold = true;
            }
            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }
            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda

            encabezadoExcel(ws);


            //encabezados de la tabla documento
            ExcelRange parametros = ws.Cells["B3:K3"];
            ExcelRange encabezado = ws.Cells["B8:K8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
            ws.Cells["B8"].Value = "JOB";
            ws.Cells["C8"].Value = "CUSTOMER";
            ws.Cells["D8"].Value = "FECHA ORDEN";
            ws.Cells["E8"].Value = "PART NUMBER";
            ws.Cells["F8"].Value = "OP";
            ws.Cells["G8"].Value = "TIPO";
            ws.Cells["H8"].Value = "CANTIDAD ORDEN";
            ws.Cells["I8"].Value = "CANTIDAD RECHAZADA";
            ws.Cells["J8"].Value = "CANTIDAD PRODUCIDA";
            ws.Cells["K8"].Value = "% COMPLETADA";

            parametros.AutoFitColumns();
            encabezado.AutoFitColumns();

        

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteCTS" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }

        public void encabezadoExcel(ExcelWorksheet ws)
        {
            ws.Cells["C3"].Value = "NUM DE MAQUINA:";
            ws.Cells["D3"].Value = Request.Form["cts"];
            ws.Cells["C4"].Value = "JOB:";
            ws.Cells["D4"].Value = Request.Form["Job"];
            ws.Cells["C5"].Value = "CUSTOMER:";
            ws.Cells["D5"].Value = Request.Form["Customer"];
            ws.Cells["C6"].Value = "PART NUMBER:";
            ws.Cells["D6"].Value = Request.Form["Partn"]; ;
            ws.Cells["F4"].Value = "DESDE:";
            ws.Cells["G4"].Value = Request.Form["Fecha_inicio"];
            ws.Cells["F5"].Value = "HASTA:";
            ws.Cells["G5"].Value = Request.Form["Fecha_final"];
            ws.Cells["M3"].Value = "SOLICITADO POR:";
            ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];
        }

        protected void writeExcel(ExcelWorksheet ws, String ct, String model, int excel)
        {
            String param = "";
            if (Request.Form["Fecha_inicio"] != "")
            {

                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                //fs = fs.AddHours(24).AddMinutes(59).AddSeconds(59);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "")
            {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != "")
            {
                param += " and  pieces.part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }
            if (Request.Form["Job"] != "")
            {
                param += " and  num_job= '" + Request.Form["Job"].Trim() + "' ";
            }
            if (Request.Form["limite"] != "")
            {
                param += "order by jobs.created_at desc ";
            }

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "select distinct num_job, jobs.created_at" +
                " from jobs " +
                " join operations on operations.job=num_job" +
                " JOIN processes on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id" +
                " where num_job is not null " + param;

            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            int limit = Convert.ToInt32(Request.Form["limite"].ToString());
            int i = 0;
            int indice = 0;
            while (dr.Read())
            {
                int x = operations(dr[0].ToString(), i, indice, ws);
                indice = x;
                if (indice >= limit)
                {
                    break;
                }
                i++;
            }
        }

        public int operations(String job, int i, int limit, ExcelWorksheet ws)
        {
            String param = "";
            int excel = 9;
            if (Request.Form["Fecha_inicio"] != "")
            {

                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                //fs = fs.AddHours(24).AddMinutes(59).AddSeconds(59);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "")
            {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != "")
            {
                param += " and  pieces.part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }



            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "select num_job, max(customer), max(jobs.created_at), max(pieces.part_number), operations.operation_service," +
                " max(operations.work_center), jobs.quantity, sum(processes.scraps) as scarpt, sum(processes.total_piece), count(cycles.id)/2" +
                " from jobs " +
                " full join operations on operations.job = num_job " +
                " JOIN processes on processes.operation_id = operations.id " +
                " join pieces on pieces.id = operations.piece_id " +
                " join clients on pieces.client_id = clients.id " +
                " join user_processes on processes.id = user_processes.process_id " +
                " join workcenters on processes.workcenter_id = workcenters.id " +
                " join cycles on cycles.user_process_id = user_processes.id " +
                " WHERE num_job is not null " + param + " and num_job= '" + job + "' and message='12345'" +
                " GROUP BY num_job, jobs.quantity, operations.operation_service, num_machine " +
                " ORDER BY num_job, operation_service; ";
            //System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            StringBuilder t1 = new StringBuilder();
            StringBuilder t = new StringBuilder();
            String[] data = new string[11];
            data[8] = "0";
            Boolean flag = false;
            int count = 0;
            List<List<Object>> rows = new List<List<object>>();
            while (dr.Read())
            {
                if (dr[0].ToString() != "")
                {
                    data[0] = job;
                    t1.Append("<tr id='demo" + i + "' class='collapse cell-1 row-child'>");
                    t1.Append("<td class='des' colspan='1'><i class='fa fa-angle-up'></i></td>");
                    t1.Append("<td class='des' colspan='1'>" + dr[1] + "</td>");
                    data[1] = dr[1].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[2] + "</td>");
                    data[2] = dr[2].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[3] + "</td>");
                    data[3] = dr[3].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[4] + "</td>");

                    t1.Append("<td class='des' colspan='1'>" + dr[5] + "</td>");
                    t1.Append("<td class='des' colspan='1'>" + dr[6] + "</td>");
                    data[4] = dr[6].ToString();
                    t1.Append("<td class='des' colspan='1'>" + dr[7] + "</td>");
                    if (dr[7].ToString() == "")
                    {
                        data[5] = "0";
                    }
                    else
                    {
                        data[5] = data[5];
                    }
                    t1.Append("<td class='des' colspan='1'>" + dr[9] + "</td>");
                    data[6] = dr[9].ToString();
                    t1.Append("<td class='des' colspan='1'>" + (Convert.ToInt32(data[5]) + Convert.ToInt32(dr[9])) + "</td>");
                    data[7] = (Convert.ToInt32(data[5]) + Convert.ToInt32(dr[9])).ToString();
                    double por = Truncate(((Convert.ToDouble(data[5]) + Convert.ToDouble(dr[9])) / (Convert.ToDouble(dr[6])) * 100), 2);
                    if (por <= 75)
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-danger'>" + por + "% </span></td>");
                    }
                    else if (por < 90 && por > 75)
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-warning'>" + por + "% </span></td>");
                    }
                    else
                    {
                        t1.Append("<td class='des' colspan='1'><span class='badge badge-success'>" + por + "% </span></td>");
                    }
                    //System.Diagnostics.Debug.WriteLine(data[0] + " op "+por);
                    t1.Append("<td class='des'></td>");

                    data[8] = (por + (Convert.ToDouble(data[8]))).ToString();
                    t1.Append("</tr>");
                    flag = true;
                    count++;
                }
            }
            if (flag)
            {
                t.Append("<tr data-toggle='collapse' data-target='#demo" + i + "'>");
                t.Append("<td class='text-center'>" + data[0] + "</td>");
                t.Append("<td class='text-center'>" + data[1] + "</td>");
                t.Append("<td class='text-center'>" + data[2] + "</td>");
                t.Append("<td class='text-center'>" + data[3] + "</td>");
                t.Append("<td class='text-center'>-</td>");
                t.Append("<td class='text-center'>-</td>");
                t.Append("<td class='text-center'>" + data[4] + "</td>");
                t.Append("<td class='text-center'>" + data[5] + "</td>");
                t.Append("<td class='text-center'>" + data[6] + "</td>");
                t.Append("<td class='text-center'>" + "sdfzgvzdfgv" + "</td>");
                double porcentaje = Truncate(Convert.ToDouble(data[8]) / count, 2);
                if (porcentaje <= 75)
                {

                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-danger'>" + porcentaje + "% </span></td>");
                }
                else if (porcentaje <= 90 && porcentaje > 75)
                {
                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-warning'>" + porcentaje + "% </span></td>");
                }
                else if (porcentaje > 90)
                {
                    t.Append("<td class='text-center' colspan='1'><span class='badge badge-success'>" + porcentaje + "% </span></td>");
                }
                t.Append("<td class='table-elipse' data-toggle='collapse' data-target='#demo" + i + "'><i class='fa fa-ellipsis-h text-black-50'></i></td>");
                t.Append("</tr>");
                Literal s = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s);
                t.Clear();
                Literal s1 = new Literal { Text = t1.ToString() };
                tabla2.Controls.Add(s1);
                t1.Clear();
                limit++;

                //System.Diagnostics.Debug.WriteLine("indice "+ i);
            }

            conn.Close();
            return limit;
        }

        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }

}