﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mantenimientos.aspx.cs" Inherits="Automaq_Reporteador_V2.Mantenimientos" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">

    <!-- Estilos css y estilos DataTables -->
    <link rel="stylesheet" href="css/nav-bar.css">   
    <link rel="stylesheet" href="css/sidebar.css">
     

    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/mantenimiento.css">
    <link rel="stylesheet" href="css/descarga-logs.css">
    <link rel="stylesheet" href="css/modal-usuario.css">
    <link rel="stylesheet" href="css/DataTables.css">
    <link rel="stylesheet" href="css/DataTables.Select.min.css">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap4.min.css">

    <title>Bienvenido a Automaq Reporteador</title>
   
</head>

<body class="background-dark">

    <form id="f_menuPage" runat="server">
    <!-- Barra de navegación -->
    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>
        <img class="img-logo" src="Img/logotecmaq.png" alt="">
        <!--<label class="logo">Reporteador</label> -->
        
        <ul>
            <li><asp:LinkButton runat="server" CssClass="hyperlink active" NavigateUrl="#" ID="home"  Text="Inicio"/></li>
            <!-- Codigo para ejecutar funciones Javascript desde la propiedad href de los elementos 'hyperlink' -->
            <li><asp:HyperLink runat="server" CssClass="hyperlink" NavigateUrl="javascript:show_propiedadesUser();" ID="user_prop_hyper" Text="Mi perfil"/></li>
            <!-- //Fin de código -->
            <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" onclick="Asistencias1" ID="Asistencias" Text="ASISTENCIAS"/></li> <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" onclick="Platform" ID="admin_grupos_hyper" Text="Automaq Plataforma"/></li>
            <li><asp:LinkButton ID="link_button" CssClass="hyperlink" runat="server" onclick="Logout"  Text="Cerrar Sesión" /></li>
        </ul>
    </nav>
      

    <!-- Barra lateral de grupos -->
    
       <div class="sidebar2" id="slide"  runat="server" >
        <h1 id="minimize" class="w3-button w3-teal w3-xlarge w3-right w3-hide-large" style="color:black" font-size="16px" onclick="w3_close();">&#8854;</h1>
    </div>

       <div class="sidebar" id="contenedor"  runat="server" >
        
       <br />
         <br /> <br /> <br />
        <h1>&nbsp;Reportes</h1>
        
        <asp:PlaceHolder id="gru" runat="server"></asp:PlaceHolder>    
        <li><asp:LinkButton runat="server" ID="t1" OnClick="Jobs1"      text="Jobs" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t3" OnClick="Conteos1"   text="Conteos" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t4" OnClick="Eficiencia1" text="Eficiencia" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t5" OnClick="Supervisor1" text="Supervisor" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t6" OnClick="TiemposMuertos1" text="Tiempos Muertos" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t7" OnClick="CTS1" text="CT'S" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t8" OnClick="CTSDetallados1" text="CT'S Detallados" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t9" OnClick="Mantenimientos1" text="Mantenimientos" CssClass="hyperlink  active"/> </li>
        <li><asp:LinkButton runat="server" ID="t10" OnClick="Calidad1" text="Calidad" CssClass="hyperlink"/> </li>
            
    </div>

        <div class="titulo">
               <h2 style="color:black" id="header">REPORTE MANTENIMIENTOS</h2>
    </div>


        <div class="table-container2" id="con" > 
            <h1 class="w3-button w3-teal w3-xlarge w3-right w3-hide-large" style="color:black" font-size="16px" onclick="w3_close();">&#9776;</h1>
        </div>


        <div class="eticon" id="eticont">
            <p class="parrafo">CT: </p>
            <p class="parrafo">Técnico: </p>
            <p class="parrafo">Tipo de Falla: </p>
            <p class="parrafo">Concepto: </p>
        </div> 

            <div class="eticon2" id="eticont2">
            <p class="parrafo">Empleado: </p>
            <p class="parrafo">Tipo Mant.: </p>
            <p class="parrafo">Desde: </p>
            <p class="parrafo">Hasta: </p>
            
            
        </div> 

         <div class="controles1" id="controles">
            <!-- Aqui van los controles de busqueda -->
             <asp:DropDownList id="cts"  CssClass="entrada_text2"  runat="server">
             <asp:ListItem Selected="True" Value="0"> Todos los CT'S </asp:ListItem>            
             </asp:DropDownList>

            <asp:DropDownList id="Tecnico"  CssClass="entrada_text2"  runat="server">
             <asp:ListItem Selected="True" Value="0"> Todos los tecnicos </asp:ListItem>            
             </asp:DropDownList>       

            <asp:DropDownList id="TipoFalla"  CssClass="entrada_text2"  runat="server">
             <asp:ListItem Selected="True" Value="0"> Todas las Fallas </asp:ListItem> 
                
             <asp:ListItem  Value="Mecánicas"> Mecánicas </asp:ListItem>
             <asp:ListItem  Value="Eléctricas/Electrónicas"> Eléctricas/Electrónicas </asp:ListItem>
             </asp:DropDownList>        
            <asp:DropDownList id="Concepto"  CssClass="entrada_text2"  runat="server">
             <asp:ListItem Selected="True" Value="0"> Todos los Conceptos </asp:ListItem>
             <asp:ListItem  Value="0"> Todos los Conceptos </asp:ListItem>
            <asp:ListItem  Value="ATC"> ATC </asp:ListItem>
            <asp:ListItem  Value="Bancada"> Bancada </asp:ListItem>
            <asp:ListItem  Value="Baterias"> Baterias </asp:ListItem>
            <asp:ListItem  Value="Brazo Q-Setter"> Brazo Q-Setter </asp:ListItem>
            <asp:ListItem  Value="Cabina"> Cabina </asp:ListItem>
            <asp:ListItem  Value="Chiller"> Chiller </asp:ListItem>
            <asp:ListItem  Value="Chuck"> Chuck </asp:ListItem>
            <asp:ListItem  Value="Conectores"> Conectores  </asp:ListItem>
            <asp:ListItem  Value="Contactores, Interruptores y Relevadores"> Contactores, Interruptores y Relevadores </asp:ListItem>
            <asp:ListItem  Value="Contrapunto"> Contrapunto </asp:ListItem>
            <asp:ListItem  Value="Conveyor"> Conveyor  </asp:ListItem>
            <asp:ListItem  Value="Corto Circuito"> Corto Circuito </asp:ListItem>
            <asp:ListItem  Value="Drive de Ejes"> Drive de Ejes </asp:ListItem>
            <asp:ListItem  Value="Drive de Husillo"> Drive de Husillo </asp:ListItem>
            <asp:ListItem  Value="Eje A"> Eje A  </asp:ListItem>
            <asp:ListItem  Value="Eje B" > Eje B </asp:ListItem>
            <asp:ListItem  Value="Eje C"> Eje C  </asp:ListItem>
            <asp:ListItem  Value="Eje W"> Eje W  </asp:ListItem>
            <asp:ListItem  Value="Eje X"> Eje X </asp:ListItem>
            <asp:ListItem  Value="Eje Y"> Eje Y  </asp:ListItem>
            <asp:ListItem  Value="Eje Z"> Eje Z </asp:ListItem>
            <asp:ListItem  Value="Encoders"> Encoders </asp:ListItem>
            <asp:ListItem  Value="Fuente de Poder"> Fuente de Poder </asp:ListItem>
            <asp:ListItem  Value="Fugas de Aceite"> Fugas de Aceite </asp:ListItem>
            <asp:ListItem  Value="Fugas de Aire"> Fugas de Aire  </asp:ListItem>
            <asp:ListItem  Value="Fugas de Soluble"> Fugas de Soluble  </asp:ListItem>
            <asp:ListItem  Value="Fusibles"> Fusibles  </asp:ListItem>
            <asp:ListItem  Value="Guardas Telescopicas"> Guardas Telescopicas </asp:ListItem>
            <asp:ListItem  Value="Husillo"> Husillo </asp:ListItem>
            <asp:ListItem  Value="Iluminacion"> Iluminacion </asp:ListItem>
            <asp:ListItem  Value="Luneta"> Luneta  </asp:ListItem>
            <asp:ListItem  Value="Magazine"> Magazine  </asp:ListItem>
            <asp:ListItem  Value="Otro"> Otro </asp:ListItem>
            <asp:ListItem  Value="Pallet"> Pallet </asp:ListItem>
            <asp:ListItem  Value="Puente"> Puente  </asp:ListItem>
            <asp:ListItem  Value="Sensores"> Sensores </asp:ListItem>
            <asp:ListItem  Value="Servomotores"> Servomotores </asp:ListItem>
            <asp:ListItem  Value="Sistema de Enfriamiento (Soluble)"> Sistema de Enfriamiento (Soluble) </asp:ListItem>
            <asp:ListItem  Value="Sistema de Lubricacion"> Sistema de Lubricacion </asp:ListItem>
            <asp:ListItem  Value="Sistema hidraúlico"> Sistema Hidraulico </asp:ListItem>
            <asp:ListItem  Value="Tarjetas Electronicas"> Tarjetas Electronicas </asp:ListItem>
            <asp:ListItem  Value="Torreta"> Torreta </asp:ListItem>
            <asp:ListItem  Value="Valvulas Solenoides"> Valvulas Solenoides </asp:ListItem>
            <asp:ListItem  Value="Ventiladores"> Ventiladores  </asp:ListItem>               
             </asp:DropDownList>         
         </div>

        <div class="controles2" id="con2">
        <asp:DropDownList id="UserList" CssClass="entrada_text2" runat="server">
        <asp:ListItem  Value="0" Selected="True"> Todos los empleados</asp:ListItem>      
        </asp:DropDownList>
        <asp:DropDownList id="TipoMantenimiento" CssClass="entrada_text2"  runat="server">
        <asp:ListItem Selected="True" Value="0"> Todos los Mantenimientos </asp:ListItem>
        <asp:ListItem Value="Correctivo permanente"> Correctivo permanente </asp:ListItem>
        <asp:ListItem Value="Correctivo temporal"> Correctivo temporal </asp:ListItem>
        <asp:ListItem Value="Preventivo"> Preventivo </asp:ListItem>
        </asp:DropDownList> 

         <input placeholder="Desde"  type="date" runat="server" name="inicio_fecha" id="Fecha_inicio" class="entrada_text2" >
         <input placeholder="Hasta"  type="date" runat="server" name="inicio_fecha" id="Fecha_final" class="entrada_text2" >
        </div>
        <!-- Finaliza -->   
      
     
           <div class="table-container4" id="controles2">
          <asp:Button ID="Buscar" CssClass="btn btn-dark btn-sm"  runat="server" onclick="Find" Text="Buscar Registros" />
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          <asp:Button ID="Reset" CssClass="btn btn-dark btn-sm"     runat="server"  onclick="resetFilter" Text="Resetear Filtros" />
               </div>
      
        <div class="table-container5" id="controles3">
           <asp:Button ID="Excel" CssClass="btn btn-dark btn-sm" onclick="ExportExcel" runat="server"  Text="Exportar a Excel" />
            <p>&nbsp;&nbsp;&nbsp;</p>
          <asp:Button ID="Pdf" CssClass="btn btn-dark btn-sm"   onclick="GeneratePDF"  runat="server"  Text="Exportar a PDF" />
        </div>
        <div class="table-container" id="tabla">
            
        
                   <asp:PlaceHolder id="tabla2" runat="server"></asp:PlaceHolder>
           
          
        </div>
       

       

    <!-- MODAL PARA VER LAS PROPIEDADES DE LA SESION -->

    <div class="modal-user" id="modal_propiedades_user" runat="server">
        <div class="modal-user-content">
            <div class="modal-user-body">
                <h2>Propiedades del Usuario</h2>
                    <br />
                    <p><b>Numero de Nomina:</b> <asp:Label runat="server" ID="lb_userID">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Nombre:</b> <asp:Label runat="server" ID="lb_username">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Apellido:</b> <asp:Label runat="server" ID="lb_userlastname">sdgsd</asp:Label></p>
                    <br />  
                    <p><b>Rol:</b> <asp:Label runat="server" ID="lb_role">sdgsd</asp:Label></p>
                    <br />
                    <button class="button-inverse" id="x" onclick="hide_propiedadesUser();">Aceptar</button>
            </div>
        </div>
         
    </div>

   
    <!-- //FIN DE MODAL PARA VER LAS PROPIEDADES DE LA SESION -->


   <script>


  function w3_close() {
      if (document.getElementById("contenedor").style.display == "block") {
          document.getElementById("contenedor").style.display = "none";
          document.getElementById("controles").style.paddingLeft = '0px';
          document.getElementById("controles2").style.paddingLeft = '0px';
          document.getElementById("con2").style.paddingLeft = '0px';
          document.getElementById("eticont").style.paddingLeft = '0px';
          document.getElementById("eticont2").style.paddingLeft = '0px';
          document.getElementById("slide").style.paddingLeft = '0px';
          document.getElementById("header").style.paddingLeft = '0px';  
          document.getElementById("minimize").style.paddingRight = '00px';
          document.getElementById("tabla").style.paddingLeft = '0px';
          document.getElementById("slide").style.display = "none";
          
      } else {
          document.getElementById("contenedor").style.display = "block";
          document.getElementById("controles").style.paddingLeft = '140px';
          document.getElementById("controles2").style.paddingLeft = '140px';
          document.getElementById("tabla").style.paddingLeft = '140px';
          document.getElementById("con2").style.paddingLeft = '140px';
          document.getElementById("eticont").style.paddingLeft = '140px';
          document.getElementById("eticont2").style.paddingLeft = '140px';
          document.getElementById("slide").style.paddingRight = '185px';
          document.getElementById("minimize").style.paddingRight = '160px';
          document.getElementById("header").style.paddingLeft= '160px';
          document.getElementById("slide").style.display = "flex";

          //document.getElementById("slide").style.paddingLeft = '160px';
      }
  
  }

</script>

    <!-- JS, Popper.js, and jQuery -->
    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/popper.min.js"></script>
    <script type="text/javascript" src="Scripts/a076d05399.js"></script>

    <!-- JS de DataTables -->
    <script type="text/javascript" src="js/table-players.js"></script>
    <script type="text/javascript" src="js/descarga-logs.js"></script>
    <script type="text/javascript" src="js/modal-usuario.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dataTables.select.min.js"></script>

        </form>
</body>


</html>
