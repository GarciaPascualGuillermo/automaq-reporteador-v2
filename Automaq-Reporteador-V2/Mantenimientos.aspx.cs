﻿using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Automaq_Reporteador_V2
{
    public partial class Mantenimientos : System.Web.UI.Page
    {
       static bool filter = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                LoadFilter();       
                loadTable();              
            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }
        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {
            UserList.SelectedValue = "0";
            cts.SelectedValue = "0";
            TipoFalla.SelectedValue = "0";
            TipoMantenimiento.SelectedValue = "0";
            Tecnico.SelectedValue = "0";
            Concepto.SelectedValue = "0";
            Fecha_inicio.Value = "";
            Fecha_final.Value = "";
        }

        public void Find(object sender, EventArgs e)
        {
            loadData();
            Excel.Visible = true;
            Pdf.Visible = true;
        }
      

        public void LoadFilter()
        {
            if (filter == false)
            {
                loadCTS();
                loadEmployee();
                loadMante();
                Excel.Visible = false;
                Pdf.Visible = false;
            } 
        }

        protected void loadTable()
        {
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped wrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>REPORTÓ");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA INICIO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FIN MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA FIN");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO EN ATENDER \n REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TÉCNICO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CONCEPTO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>COMENTARIO");
            t.Append("</td>");
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>REPORTÓ");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA INICIO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FIN MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA FIN");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO EN ATENDER REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TÉCNICO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CONCEPTO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>COMENTARIO");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }

        public void loadCTS()
        {
            if (cts.Items.Count < 2)
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
                conn.Open();
                String query = "SELECT id, num_machine FROM workcenters ORDER BY num_machine";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    System.Web.UI.WebControls.ListItem lst = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[1].ToString());
                    cts.Items.Insert(cts.Items.Count - 1, lst);
                }

                conn.Close();
            }
        }
        protected void loadData()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            System.Diagnostics.Debug.WriteLine("" + Createquery());
            NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped wrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>REPORTÓ");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA INICIO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FIN MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO EN ATENDER");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>TÉCNICO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CONCEPTO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>COMENTARIO");
            t.Append("</td>");
            t.Append("</thead>");
            t.Append("<tbody>");
            while (dr.Read())
            {
                t.Append("<tr>");
                t.Append("<td>" + dr[0] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[1] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[2] + "");
                t.Append("</td>");
                DateTime d2 = DateTime.Parse(dr[3].ToString());
                t.Append("<td>" + d2.ToString("dd/MM/yyyy") + "");
                t.Append("</td>");
                t.Append("<td>" + $"{ d2: HH: mm: ss}");
                t.Append("</td>");
                DateTime d3 = DateTime.Parse(dr[4].ToString());
                t.Append("<td>" + d3.ToString("dd/MM/yyyy") + "");
                t.Append("</td>");
                t.Append("<td>"+ $"{ d3: HH: mm: ss}");
                t.Append("</td>");
                DateTime d4 = DateTime.Parse(dr[5].ToString());
                t.Append("<td>" + d4.ToString("dd/MM/yyyy") + "");
                t.Append("</td>");
                t.Append("<td>" + $"{ d4: HH: mm: ss}");
                t.Append("</td>");
                t.Append("<td>"+ Truncate(Convert.ToDouble(dr[11].ToString()),2)+" ");
                t.Append("</td>");
                t.Append("<td>" + Truncate(Convert.ToDouble(dr[12].ToString()), 2) + " ");
                t.Append("</td>");
                t.Append("<td>"+ Truncate(Convert.ToDouble(dr[13].ToString()), 2) + " ");
                t.Append("</td>");
                t.Append("<td> "+dr[6]+"");
                t.Append("</td>");
                t.Append("<td> "+dr[7]+"");
                t.Append("</td>");
                t.Append("<td> "+dr[8]+"");            
                t.Append("</td>");
                t.Append("<td> " + dr[9] + "");
                t.Append("</td>");
                t.Append("<td> " + dr[10] + "");
                t.Append("</td>");
               
                t.Append("</tr>");
            }
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>REPORTÓ");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA REPORTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA INICIO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA FIN MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO EN ATENDER");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIEMPO TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>TÉCNICO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO DE MTO.");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CONCEPTO FALLA");
            t.Append("</td>");
            t.Append("<td class='t_header'>COMENTARIO");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
            conn.Close();
        }
        protected void loadEmployee()
        {
            if (UserList.Items.Count < 2)
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                      "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                // Define a query
                String consulta = "Select name,firstname, idemployee from users order by idemployee";
                System.Diagnostics.Debug.WriteLine("pr");

                NpgsqlCommand command = new NpgsqlCommand(consulta, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {

                    System.Web.UI.WebControls.ListItem lst = new System.Web.UI.WebControls.ListItem(dr[2].ToString() + " " + dr[0].ToString() + " " + dr[1].ToString(), dr[2].ToString());
                    UserList.Items.Insert(UserList.Items.Count - 1, lst);
                }
                conn.Close();
            }
        }

        protected void loadMante()
        {
            if (Tecnico.Items.Count < 2)
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                      "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                // Define a query
                String consulta = "Select DISTINCT name,firstname, idemployee from users join role_user on users.id=role_user.user_id where role_id=15 or role_id=16 order by idemployee";
                System.Diagnostics.Debug.WriteLine("pr");

                NpgsqlCommand command = new NpgsqlCommand(consulta, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    System.Web.UI.WebControls.ListItem lst = new System.Web.UI.WebControls.ListItem(dr[2].ToString() + " " + dr[0].ToString() + " " + dr[1].ToString(), dr[2].ToString());
                    Tecnico.Items.Insert(Tecnico.Items.Count - 1, lst);
                }
                conn.Close();
            }
        }
        public String Createquery()
        {
            String param= "";
            String consulta = "SELECT num_machine, user_id, operador.name, pauses.created_at, pauses.started_at, pauses.ended_at, tecnico.name, "+
            " pauses.maintenance_type, pauses.type, pauses.concept, pauses.comments," +
            "extract(epoch from pauses.started_at  - pauses.created_at ) / 360 as reporte,"+
            "extract(epoch from pauses.ended_at  -pauses.started_at ) / 360 as mantenimiento,"+
            "((extract(epoch from pauses.started_at  -pauses.created_at ) / 360) + (extract(epoch from pauses.ended_at  -pauses.started_at ) / 360)) as total" +           
            " FROM pauses" +
            " JOIN users as operador on pauses.user_id = operador.id" +
            " JOIN workcenters on pauses.workcenter_id = workcenters.id"+
            " JOIN users as tecnico on pauses.operador_id = tecnico.id ";
            
            if (Request.Form["Fecha_inicio"] == "")
            {
               
                param += "where pauses.created_at >= '04/04/2020'";
            }
            if (Request.Form["Fecha_inicio"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);               
                param += "where (pauses.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                param += " and (pauses.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["UserList"] != "0")
            {                
                param += " and (idemployee= '" + Request.Form["UserList"] + "')";
            }
            if (Request.Form["Tecnico"] != "0")
            {
                param += " and (idemployee= '" + Request.Form["Tecnico"] + "')";
            }
            if (Request.Form["cts"] != "0")
            {
                param += " and (num_machine= '"+ Request.Form["cts"]+"')";
            }
            if (Request.Form["TipoFalla"] != "0")
            {
                param += " and (type= '" + Request.Form["TipoFalla"] + "')";
            }
            if (Request.Form["Concepto"] != "0")
            {
                param += " and (concept= '" + Request.Form["Concepto"] + "')";
            }
            if (Request.Form["cts"] != "0")
            {
                param += " and (num_machine= '" + Request.Form["cts"] + "')";
            }
            if (Request.Form["TipoMantenimiento"] != "0")
            {
                param += " and (maintenance_type= '" + Request.Form["TipoMantenimiento"] + "')";
            }
            return consulta+param;
        }

      
        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }

        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte Mantenimientos");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de Mantenimientos";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);

            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
          
                using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
                {
                    Rng.Value = "REPORTE MANTENIMIENTOS";
                    Rng.Style.Font.Size = 20;
                    Rng.Style.Font.Bold = true;
                }

            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }


            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda

            encabezadoExcel(ws);


            //encabezados de la tabla documento
            ExcelRange parametros = ws.Cells["A3:S3"];
            ExcelRange encabezado = ws.Cells["A8:S8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
            ws.Cells["B8"].Value = "CT";
            ws.Cells["C8"].Value = "NUM";
            ws.Cells["D8"].Value = "NOMBRE";
            ws.Cells["E8"].Value = "FECHA REPORTE";
            ws.Cells["F8"].Value = "HORA REPORTE";
            ws.Cells["G8"].Value = "FECHA INICIO";
            ws.Cells["H8"].Value = "HORA INICIO MTO";
            ws.Cells["I8"].Value = "FECHA CIERRE";
            ws.Cells["J8"].Value = "HORA CIERRE MTO";
            ws.Cells["K8"].Value = "TIEMPO EN ATENDER REPORTE";
            ws.Cells["L8"].Value = "TIEMPO MTO EFECTIVO";
            ws.Cells["M8"].Value = "TIEMPO TOTAL";
            ws.Cells["N8"].Value = "TÉCNICO DE MTO";
            ws.Cells["O8"].Value = "TIPO DE MTO";
            ws.Cells["P8"].Value = "TIPO DE FALLA";
            ws.Cells["Q8"].Value = "CONCEPTO FALLA";
            ws.Cells["R8"].Value = "COMENTARIOS";
            parametros.AutoFitColumns();
            encabezado.AutoFitColumns();

            dataExcel(ws);

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteMantenimientos" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }
        public void dataExcel(ExcelWorksheet ws)
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                             "Password=tecmaq;Database=tecmaq;");
            conn.Open();
          
            NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            int excel = 9;  
                while (dr.Read())
                {
                
                    ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                    ws.Cells[String.Format("C{0}", excel)].Value = dr[1];
                    ws.Cells[String.Format("D{0}", excel)].Value = dr[2];
                    DateTime d2 = DateTime.Parse(dr[3].ToString());
                    ws.Cells[String.Format("E{0}", excel)].Value = d2.ToString("dd/MM/yyyy");
                    ws.Cells[String.Format("F{0}", excel)].Value = $"{ d2: HH: mm: ss}";
                    DateTime d3 = DateTime.Parse(dr[4].ToString());
                    ws.Cells[String.Format("G{0}", excel)].Value = d3.ToString("dd/MM/yyyy");
                    ws.Cells[String.Format("H{0}", excel)].Value = $"{ d3: HH: mm: ss}";
                    DateTime d4 = DateTime.Parse(dr[5].ToString());
                    ws.Cells[String.Format("I{0}", excel)].Value = d4.ToString("dd/MM/yyyy");
                    ws.Cells[String.Format("J{0}", excel)].Value = $"{ d4: HH: mm: ss}";
                    ws.Cells[String.Format("K{0}", excel)].Value = Truncate(Convert.ToDouble(dr[11].ToString()), 2);
                    ws.Cells[String.Format("L{0}", excel)].Value = Truncate(Convert.ToDouble(dr[12].ToString()), 2);
                    ws.Cells[String.Format("M{0}", excel)].Value = Truncate(Convert.ToDouble(dr[13].ToString()), 2);
                    ws.Cells[String.Format("N{0}", excel)].Value = dr[6];
                    ws.Cells[String.Format("O{0}", excel)].Value = dr[7];
                    ws.Cells[String.Format("P{0}", excel)].Value = dr[8];
                    ws.Cells[String.Format("Q{0}", excel)].Value = dr[9];
                    ws.Cells[String.Format("R{0}", excel)].Value = dr[10];
                excel++;               
                }
            conn.Close();
        }
        public void encabezadoExcel(ExcelWorksheet ws)
        {
            ws.Cells["C3"].Value = "NUM DE MAQUINA:";
            ws.Cells["D3"].Value = Request.Form["cts"];           
            ws.Cells["F3"].Value = "ID EMPLEADO:";
            ws.Cells["G3"].Value = Request.Form["UserList"];
            ws.Cells["C4"].Value = "TÉCNICO:";
            ws.Cells["D4"].Value = Request.Form["Tecnico"];     
            ws.Cells["C5"].Value = "TIPO DE FALLA:";
            ws.Cells["D5"].Value = Request.Form["TipoFalla"];          
            ws.Cells["C6"].Value = "CONCEPTO:";
            ws.Cells["D6"].Value = Request.Form["Concepto"]; ;
            ws.Cells["F4"].Value = "DESDE:";
            ws.Cells["G4"].Value = Request.Form["Fecha_inicio"];
            ws.Cells["F5"].Value = "HASTA:";
            ws.Cells["G5"].Value = Request.Form["Fecha_final"];
            ws.Cells["F6"].Value = "TIPO MANTENIMIENTO:";
            ws.Cells["G6"].Value = Request.Form["TipoMantenimiento"];


            ws.Cells["M3"].Value = "SOLICITADO POR:";
            ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];

        }
        protected void GeneratePDF(object sender, System.EventArgs e)
        {
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
                           "Password=tecmaq;Database=tecmaq;");
                conn.Open();
                // Define a query          
                NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                Document document = new Document(PageSize.A4, 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                string imageURL = Server.MapPath(".") + "/Img/logoc.png";
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                //Resize image depend upon your need
                jpg.ScaleToFit(140f, 120f);
                //Give space before image
                jpg.SpacingBefore = 10f;
                //Give some space after the image
                jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_LEFT;
                Paragraph paragraph = null;             
                paragraph = new Paragraph("REPORTE MANTENIMIENTOS");                            
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Font.Size = 15;
                document.Add(jpg);
                document.Add(paragraph);

                Paragraph sesion = new Paragraph("Solicitó: " + Session["User"] + " " + Session["LastName"]);
                sesion.Alignment = Element.ALIGN_RIGHT;
                sesion.Font.Size = 12;
                document.Add(sesion);
                String tipo = "Producción";
                int columnas = 9;
                if (Request.Form["Tipo"] == "0")
                {
                    tipo = "Herramentaje";
                    columnas = 12;
                }
                if (Request.Form["Tipo"] == "1")
                {
                    tipo = "Producción";
                    columnas = 9;
                }
                if (Request.Form["Tipo"] == "2")
                {
                    tipo = "Ajustes";
                    columnas = 13;
                }
                document.Add(Chunk.NEWLINE);
                PdfPTable parametros = new PdfPTable(7);
                parametros.AddCell("Tipo");
                parametros.AddCell("Job");
                parametros.AddCell("Customer");
                parametros.AddCell("Part Number");
                parametros.AddCell("Empleado");
                parametros.AddCell("Desde");
                parametros.AddCell("Hasta");
                parametros.AddCell(tipo);
                parametros.AddCell(Request.Form["prueba23"]);
                parametros.AddCell(Request.Form["Customer"]);
                parametros.AddCell(Request.Form["Partn"]);
                parametros.AddCell(Request.Form["UserList"]);
                parametros.AddCell(Request.Form["Fecha_inicio"]);
                parametros.AddCell(Request.Form["Fecha_final"]);

                document.Add(parametros);


                document.Add(Chunk.NEWLINE);


                //header de la tabla
                PdfPTable table = new PdfPTable(columnas);
                table.AddCell("ID");
                table.AddCell("Nombre");
                table.AddCell("Job");
                table.AddCell("Customer");
                table.AddCell("Part Number");
                table.AddCell("OP");
                table.AddCell("CT");
                if (Request.Form["Tipo"] == "0")
                {
                    table.AddCell("Fecha IH");
                    table.AddCell("Tiempo IH");
                    table.AddCell("Fecha FH");
                    table.AddCell("Tiempo FH");
                    table.AddCell("Total");
                }
                if (Request.Form["Tipo"] == "1")
                {
                    table.AddCell("Fecha");
                    table.AddCell("Eficiencia");
                }
                if (Request.Form["Tipo"] == "2")
                {
                    table.AddCell("Fecha IA");
                    table.AddCell("Tiempo IA");
                    table.AddCell("Fecha FA");
                    table.AddCell("Tiempo FA");
                    table.AddCell("Total");
                    table.AddCell("Status");
                }

                // Esta es la primera fila


                //cuerpa de la tabla

                while (dr.Read())
                {
                    table.AddCell("" + dr[0].ToString());
                    table.AddCell("" + dr[1].ToString());
                    table.AddCell("" + dr[2].ToString());
                    table.AddCell("" + dr[3].ToString());
                    table.AddCell("" + dr[4].ToString());
                    table.AddCell("" + dr[5].ToString());
                    table.AddCell("" + dr[6].ToString());
                    if (Request.Form["Tipo"] == "0")
                    {
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + $"{ d: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }
                        DateTime d2 = DateTime.Now;
                        try
                        {
                            d2 = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d2.ToString("d"));
                            table.AddCell("" + $"{ d2: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }

                        table.AddCell("" + (d2 - d).ToString());
                    }
                    if (Request.Form["Tipo"] == "1")
                    {
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + dr[8].ToString() + "%");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("" + dr[8].ToString() + "%");
                        }
                    }
                    if (Request.Form["Tipo"] == "2")
                    {
                        DateTime d = DateTime.Now;
                        try
                        {
                            d = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d.ToString("d"));
                            table.AddCell("" + $"{ d: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }
                        DateTime d2 = DateTime.Now;
                        try
                        {
                            d2 = DateTime.Parse(dr[7].ToString());
                            table.AddCell("" + d2.ToString("d"));
                            table.AddCell("" + $"{ d2: HH: mm: ss}");
                        }
                        catch (FormatException)
                        {
                            table.AddCell("");
                            table.AddCell("");
                        }

                        table.AddCell("" + (d2 - d).ToString());
                        table.AddCell("" + dr[9].ToString());
                    }
                }
                conn.Close();
                document.Add(table);
                //fin del cuerpo del pdf
                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";


                Response.AddHeader("Content-Disposition", "attachment; filename=" + "Reporte eficiencia de operador " + DateTime.Now.ToString("d") + ".pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }
}