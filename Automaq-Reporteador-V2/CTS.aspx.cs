﻿using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class CT_S : System.Web.UI.Page
    {

        static List<String> ct = new List<String>();
        static List<String> model = new List<String>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                loadFilter();

            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {
            tipo.SelectedValue = "0";
            cts.SelectedValue = "0";
            Fecha_final.Value = "";
            Fecha_inicio.Value = "";
            Excel.Visible = false;
            Pdf.Visible = false;
        }
        public void loadFilter()
        {
            if (cts.Items.Count == 1)
            {
                loadCTS();
                loadTipo();
                loadTable();
            }

        }


        public void search(object sender, EventArgs e)
        {
            loadData();
            Minutes();
        }

        public void loadCTS()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT id, num_machine, work_center FROM workcenters ORDER BY num_machine";
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            ct.Clear();
            while (dr.Read())
            {
                if (dr[1].ToString() != "")
                {
                    ListItem lst = new ListItem(dr[1].ToString(), dr[1].ToString());
                    ct.Add(dr[1].ToString());
                    model.Add(dr[2].ToString());
                    cts.Items.Insert(cts.Items.Count - 1, lst);
                }

            }
            // //system.diagnostics.Debug.WriteLine("cts: " +ct.Count);
            conn.Close();
        }
        public void loadTipo()
        {

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT DISTINCT work_center FROM workcenters ORDER BY work_center";
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {

                ListItem lst = new ListItem(dr[0].ToString(), dr[0].ToString());
                tipo.Items.Insert(tipo.Items.Count - 1, lst);
            }

            conn.Close();
        }

        protected void loadTable()
        {

            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFI. PONDERADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>HERRAMENTAJE");
            t.Append("</td>");
            t.Append("<td class='t_header'>AJUSTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>PRODUCCION");
            t.Append("</td>");
            t.Append("<td class='t_header'>MANTENIMIENTO");
            t.Append("</td>");
            t.Append("<td class='t_header'>PAROS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SIN USO");
            t.Append("</td>");
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>ID");
            t.Append("</td>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFI. PONDERADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>HERRAMENTAJE");
            t.Append("</td>");
            t.Append("<td class='t_header'>AJUSTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>PRODUCCION");
            t.Append("</td>");
            t.Append("<td class='t_header'>MANTENIMIENTO");
            t.Append("</td>");
            t.Append("<td class='t_header'>PAROS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SIN USO");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }

        protected void loadData()
        {
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped wrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFI. PONDERADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>HERRAMENTAJE");
            t.Append("</td>");
            t.Append("<td class='t_header'>AJUSTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>PRODUCCION");
            t.Append("</td>");
            t.Append("<td class='t_header'>MANTENIMIENTO");
            t.Append("</td>");
            t.Append("<td class='t_header'>PAROS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SIN USO");
            t.Append("</td>");
            t.Append("</thead>");
            t.Append("<tbody>");
            if (Request.Form["cts"] != "0")
            {
                writeData(t, Request.Form["cts"], "");
            }
            else
            {
                for (int i = 0; i < ct.Count; i++)
                {
                    writeData(t, ct[i], model[i]);
                }
            }

            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>TIPO");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFI. PONDERADA");
            t.Append("</td>");
            t.Append("<td class='t_header'>HERRAMENTAJE");
            t.Append("</td>");
            t.Append("<td class='t_header'>AJUSTE");
            t.Append("</td>");
            t.Append("<td class='t_header'>PRODUCCION");
            t.Append("</td>");
            t.Append("<td class='t_header'>MANTENIMIENTO");
            t.Append("</td>");
            t.Append("<td class='t_header'>PAROS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SIN USO");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);

        }

        public List<Object> times(String ct)
        {
            List<Object> conteos = new List<Object>();
            String parameter = "";
            String parameter2 = "";
            String Tipo = Request.Form["tipo"];
            String CT = ct;
            String FF = Request.Form["Fecha_final"];
            String FI = Request.Form["Fecha_inicio"];

            if (Tipo != "0")
            {
                parameter += " and work_center='" + Tipo + "' ";
                parameter2 += " and work_center='" + Tipo + "' ";
            }
            if (CT != "0")
            {
                parameter += " and num_machine='" + CT + "' ";
                parameter2 += " and num_machine='" + CT + "' ";
            }
            if (FI != "")
            {
                DateTime fs = DateTime.Parse(FI);

                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                parameter += "and (actividad.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
                parameter2 += " and (pauses.started_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "') ";
            }
            if (FF != "")
            {
                DateTime fs = DateTime.Parse(FF);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                parameter += " and (actividad.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "') ";
                parameter2 += " and (pauses.started_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "') ";

            }

            double herramentaje = 0.0;
            double ajuste = 0.0;
            double produccion = 0.0;
            double mantenimiento = 0.0;
            double paros = 0.0;
            String num = "";
            String modelo = "";
            //consultas sql 
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT num_machine,workcenters.work_center,  sum ( ( extract(epoch from process_logs.created_at - actividad.created_at)/60 )  ) AS tiempo_herramentaje FROM workcenters " +
                " join process_logs on workcenters.id = process_logs.workcenter_id" +
                " join process_logs as actividad on process_logs.parent_id = actividad.id" +
                " where((process_logs.activity = 'fh' and actividad.activity = 'ih') or(process_logs.activity = 'ih' and actividad.activity = 'crp') or(process_logs.activity = 'crp' and actividad.activity = 'fh')) " +
                 parameter +
                "group by num_machine, work_center ORDER BY num_machine; ";
            ////system.diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                num = ct;
                modelo = dr[1].ToString();
                herramentaje = Convert.ToDouble(dr[2].ToString());

            }
            conn.Close();
            conn.Open();
            String query2 = "SELECT sum ( ( extract(epoch from process_logs.created_at - actividad.created_at)/60 )  ) AS tiempo_ajuste FROM workcenters " +
                " join process_logs on workcenters.id = process_logs.workcenter_id" +
                " join process_logs as actividad on process_logs.parent_id = actividad.id" +
                " where((process_logs.activity = 'fa' and actividad.activity = 'ia') or(process_logs.activity = 'fa' and actividad.activity = 'crp') or(process_logs.activity = 'crp' and actividad.activity = 'ia') or(process_logs.activity = 'ls' and actividad.activity = 'il') or(process_logs.activity = 'ln' and actividad.activity = 'il') or(process_logs.activity = 'ls' and actividad.activity = 'ial') or(process_logs.activity = 'ln' and actividad.activity = 'ial'))" +
                 parameter +
                " group by num_machine ORDER BY num_machine;";
            NpgsqlCommand command2 = new NpgsqlCommand(query2, conn);
            // Execute the query and obtain a result set
            ////system.diagnostics.Debug.WriteLine(parameter);

            NpgsqlDataReader dr2 = command2.ExecuteReader();
            if (dr2.Read())
            {
                //system.diagnostics.Debug.WriteLine("tiempo ajuste "+dr2[0]);
                ajuste = Convert.ToDouble(dr2[0].ToString());

            }
            conn.Close();
            conn.Open();
            String query3 = "SELECT sum ( ( extract(epoch from process_logs.created_at - actividad.created_at)/60 )  ) AS tiempo_ajuste FROM workcenters " +
            " join process_logs on workcenters.id = process_logs.workcenter_id" +
            " join process_logs as actividad on process_logs.parent_id = actividad.id" +
            " where  ((process_logs.activity='cr' and actividad.activity='pr') or (process_logs.activity='crp' and actividad.activity='pr') or (process_logs.activity='cr' and actividad.activity='crp')) " +
            parameter +
            "group by num_machine ORDER BY num_machine;";


            NpgsqlCommand command3 = new NpgsqlCommand(query3, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr3 = command3.ExecuteReader();
            //system.diagnostics.Debug.WriteLine(parameter);

            if (dr3.Read())
            {
                produccion = Convert.ToDouble(dr3[0].ToString());

            }
            conn.Close();
            conn.Open();
            String query4 = "SELECT num_machine, 	" +
                " sum ( CASE motivo WHEN 'mantenimiento' THEN ( ( extract(epoch from ended_at  - started_at  ) / 60 )  )  END ) AS tiempo_mantenimiento, " +
                " sum (CASE motivo  WHEN 'herramienta' THEN( ( extract( epoch from ended_at  - started_at  ) / 60 ) ) " +
                " WHEN 'material' THEN(extract(epoch from ended_at - started_at ) / 60 )  END ) AS tiempo_paros from workcenters full join pauses on pauses.workcenter_id= workcenters.id where num_machine is not null " +
                parameter2 +
                " GROUP BY num_machine; ";
            NpgsqlCommand command4 = new NpgsqlCommand(query4, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr4 = command4.ExecuteReader();
            if (dr4.Read())
            {
                if (dr4[1].ToString() == "")
                {
                    mantenimiento = 0;
                }
                else
                {
                    mantenimiento = Convert.ToDouble(dr4[1].ToString());
                }
                if (dr4[2].ToString() == "")
                {
                    paros = 0;
                }
                else
                {
                    paros = Convert.ToDouble(dr4[2].ToString());
                }

            }
            conn.Close();
            conteos.Add(ct);
            conteos.Add(modelo);
            conteos.Add(herramentaje);
            conteos.Add(ajuste);
            conteos.Add(produccion);
            conteos.Add(mantenimiento);
            conteos.Add(paros);
            return conteos;
        }

        protected void writeData(StringBuilder t, String ct, String model)
        {
            List<Object> l = times(ct);
            double total = Minutes();
            double efipondera = EfiPonderada(total, ct);
            String num = ct;
            String mod = model;
            String herra = l[2].ToString();
            String ajuste = l[3].ToString();
            String pr = l[4].ToString();
            String man = l[5].ToString();
            String paros = l[6].ToString();
            t.Append("<tr>");
            t.Append("<td>" + num + "");
            t.Append("</td>");
            t.Append("<td>" + mod + "");
            t.Append("</td>");
            t.Append("<td>" + efipondera + "%");
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(herra, total) + "%");
            //system.diagnostics.Debug.WriteLine("Herra: " + herra);
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(ajuste, total) + "%");
            //system.diagnostics.Debug.WriteLine("Ajuste: " + ajuste);
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(pr, total) + "%");
            //system.diagnostics.Debug.WriteLine("pr: " + pr);
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(man, total) + "%");
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(paros, total) + "%");
            t.Append("</td>");
            t.Append("<td>" + getPorcentaje(((total) - Convert.ToDouble(paros) - Convert.ToDouble(man) - Convert.ToDouble(pr) - Convert.ToDouble(ajuste) - Convert.ToDouble(herra)).ToString(), total) + "%");
            t.Append("</td>");
            t.Append("</tr>");

        }
        protected double Minutes()
        {
            DateTime d = Convert.ToDateTime("2020-04-01");
            DateTime d2 = DateTime.Now;
            if (Request.Form["Fecha_inicio"] != "")
            {
                d = DateTime.Parse(Request.Form["Fecha_inicio"]);
                d = d.AddHours(-11);

            }
            if (Request.Form["Fecha_final"] != "")
            {
                d2 = DateTime.Parse(Request.Form["Fecha_final"]);
                d2 = d2.AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            TimeSpan diferencia = d2.Subtract(d);
            double days = diferencia.TotalDays;
            double minutos = days * 1005;

            //system.diagnostics.Debug.WriteLine("diferencia de minutos: "+ minutos);
            return minutos;
        }

        protected double getPorcentaje(String p1, double total)
        {
            double part = 0.00;
            if (p1 != "")
            {
                if (p1 != null)
                {
                    part = Convert.ToDouble(p1);
                }
            }

            double baseP = (part / total) * 100;
            //double porcentaje = part / baseP;
            return Truncate(baseP, 2);
        }

        protected double EfiPonderada(double tiempo, String ct)
        {
            String parameter = "";
            String Tipo = Request.Form["tipo"];

            String FF = Request.Form["Fecha_final"];
            String FI = Request.Form["Fecha_inicio"];

            if (Tipo != "0")
            {
                parameter += " and work_center='" + Tipo + "' ";
            }
            if (ct != "0")
            {
                parameter += " and num_machine='" + ct + "' ";
            }
            if (FI != "")
            {
                DateTime fs = DateTime.Parse(FI);
                fs = fs.AddHours(5);
                parameter += " and (actividad.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "') ";
            }
            if (FF != "")
            {
                DateTime fs = DateTime.Parse(FF);
                    fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                parameter += " and (actividad.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "') ";

            }
            double efiponderada = 0.0;
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT num_machine,sum((extract(epoch from process_logs.created_at - actividad.created_at)/60)), (((abs(sum(user_processes.end - user_processes.start)))*(max(processes.standar_time)))/(sum((extract(epoch from process_logs.created_at - actividad.created_at)/60))))*100 as eficiencia" +
                " FROM workcenters" +
                " join process_logs on workcenters.id = process_logs.workcenter_id" +
                " join user_processes on process_logs.user_process_id = user_processes.id" +
                " join processes on processes.id = user_processes.process_id" +
                " join process_logs as actividad on process_logs.parent_id = actividad.id" +
                " where((process_logs.activity = 'cr' and actividad.activity = 'pr') or(process_logs.activity = 'crp' and actividad.activity = 'pr') or(process_logs.activity = 'cr' and actividad.activity = 'crp')) " +
                parameter +
                " group by process_logs.user_process_id, num_machine, work_center order by num_machine; ";
            //system.diagnostics.Debug.WriteLine(parameter);

            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                double tiempopor = ((Convert.ToDouble(dr[1].ToString())) / tiempo) * 100;
                double efiponderadasum = ((Convert.ToDouble(dr[2].ToString())) * tiempopor) / 100;
                efiponderada = efiponderada + efiponderadasum;
                //system.diagnostics.Debug.WriteLine("tiempo "+ tiempopor +" efiponderada: "+ efiponderada + " eficiencia: "+ dr[2].ToString());

            }
            conn.Close();
            return Truncate(efiponderada, 2);

        }

        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }


        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de Centros de Trabajo");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de Centros de Trabajo";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);
            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
            using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
            {
                Rng.Value = "REPORTE Centros de trabajo";
                Rng.Style.Font.Size = 20;
                Rng.Style.Font.Bold = true;
            }
            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }
            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda

            encabezadoExcel(ws);


            //encabezados de la tabla documento
            ExcelRange parametros = ws.Cells["A3:S3"];
            ExcelRange encabezado = ws.Cells["A8:S8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
            ws.Cells["B8"].Value = "CT";
            ws.Cells["C8"].Value = "TIPO";
            ws.Cells["D8"].Value = "EFI. PONDERADA";
            ws.Cells["E8"].Value = "HERRAMENTAJE";
            ws.Cells["F8"].Value = "AJUSTE";
            ws.Cells["G8"].Value = "PRODUCCIÓN";
            ws.Cells["H8"].Value = "MANTENIMIENTO";
            ws.Cells["I8"].Value = "PAROS";
            ws.Cells["J8"].Value = "SIN USO";
   
            parametros.AutoFitColumns();
            encabezado.AutoFitColumns();

            dataExcel(ws);

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteCTS" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }

        public void encabezadoExcel(ExcelWorksheet ws)
        {
            ws.Cells["C3"].Value = "NUM DE MAQUINA:";
            ws.Cells["D3"].Value = Request.Form["cts"];
            ws.Cells["F3"].Value = "ID EMPLEADO:";
            ws.Cells["G3"].Value = Request.Form["UserList"];
            ws.Cells["C4"].Value = "JOB:";
            ws.Cells["D4"].Value = Request.Form["Job"];
            ws.Cells["C5"].Value = "CUSTOMER:";
            ws.Cells["D5"].Value = Request.Form["Customer"];
            ws.Cells["C6"].Value = "PART NUMBER:";
            ws.Cells["D6"].Value = Request.Form["Partn"]; ;
            ws.Cells["F4"].Value = "DESDE:";
            ws.Cells["G4"].Value = Request.Form["Fecha_inicio"];
            ws.Cells["F5"].Value = "HASTA:";
            ws.Cells["G5"].Value = Request.Form["Fecha_final"];
            ws.Cells["M3"].Value = "SOLICITADO POR:";
            ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];

        }

        protected void writeExcel(ExcelWorksheet ws, String ct, String model, int excel)
        {
            List<Object> l = times(ct);
            double total = Minutes();
            double efipondera = EfiPonderada(total, ct);
            String num = ct;
            String mod = model;
            String herra = l[2].ToString();
            String ajuste = l[3].ToString();
            String pr = l[4].ToString();
            String man = l[5].ToString();
            String paros = l[6].ToString();
           
            ws.Cells[String.Format("B{0}", excel)].Value = num;
            ws.Cells[String.Format("C{0}", excel)].Value = mod;
            ws.Cells[String.Format("D{0}", excel)].Value = efipondera;
            ws.Cells[String.Format("E{0}", excel)].Value = getPorcentaje(herra, total) + "%";
            ws.Cells[String.Format("F{0}", excel)].Value = getPorcentaje(ajuste, total) + "%";
            ws.Cells[String.Format("G{0}", excel)].Value = getPorcentaje(pr, total) + "%";
            ws.Cells[String.Format("H{0}", excel)].Value = getPorcentaje(man, total) + "%";
            ws.Cells[String.Format("I{0}", excel)].Value = getPorcentaje(paros, total) + "%";
            ws.Cells[String.Format("J{0}", excel)].Value = getPorcentaje(((total) - Convert.ToDouble(paros) - Convert.ToDouble(man) - Convert.ToDouble(pr) - Convert.ToDouble(ajuste) - Convert.ToDouble(herra)).ToString(), total) + "%";
        }

        public void dataExcel(ExcelWorksheet ws)
        {

            int contador = 9;
            if (Request.Form["cts"] != "0")
            {
                writeExcel(ws, Request.Form["cts"], "", contador);
            }
            else
            {
                for (int i = 0; i < ct.Count; i++)
                {
                    writeExcel(ws, ct[i], model[i], contador);
                    contador++;
                }
            }


        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }

}