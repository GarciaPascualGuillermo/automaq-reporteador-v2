﻿function show_container_loading() {
    document.getElementById("container_loader").style.visibility = "visible";
    document.getElementById("container_loader").style.opacity = 1;
}

function hide_container_loading() {
    document.getElementById("container_loader").style.visibility = "hidden";
    document.getElementById("container_loader").style.opacity = 0;
}