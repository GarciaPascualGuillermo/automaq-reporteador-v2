﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class Menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }

        public void SessionDetails()
        {
            lb_userID.Text          = Session["IdEmployee"].ToString();
            lb_username.Text        = Session["User"].ToString();
            lb_userlastname.Text    = Session["LastName"].ToString();
            lb_role.Text            = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag= false;
            if(Session["User"]!= null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"]  = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }
    }
}