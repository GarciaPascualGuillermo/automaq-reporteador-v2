﻿using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class CT_SDetallados : System.Web.UI.Page
    {
        static bool filter = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                loadTable();
                LoadFilter();
                //filter = true;

            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
        }

        private void LoadFilter()
        {
            if (filter == false)
            {
                loadEmployee();
                loadCTS();
            }
            
        }
        public void loadCTS()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            if (cts.Items.Count < 2)
            {
                String query = "SELECT id, num_machine FROM workcenters ORDER BY num_machine";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    System.Web.UI.WebControls.ListItem lst = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[1].ToString());
                    cts.Items.Insert(cts.Items.Count - 1, lst);
                }
            }
            conn.Close();

        }
        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Response.Redirect("Conteos.aspx");
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {
            cts.SelectedValue = "0";
            UserList.SelectedValue = "0";
            Job.Value = "";
            Customer.Value = "";
            Partn.Value = "";
            Fecha_final.Value = "";
            Fecha_inicio.Value = "";
        }

        public void Find(object sender, EventArgs e)
        {
            getTable();
        }

        public void loadEmployee()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " +
              "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            // Define a query
            String consulta = "Select name,firstname, idemployee from users order by idemployee";
            System.Diagnostics.Debug.WriteLine("pr");
            if (UserList.Items.Count < 2)
            {
                NpgsqlCommand command = new NpgsqlCommand(consulta, conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {

                    ListItem lst = new ListItem(dr[2].ToString() + " " + dr[0].ToString() + " " + dr[1].ToString(), dr[2].ToString());
                    UserList.Items.Insert(UserList.Items.Count - 1, lst);
                }
                conn.Close();
            }
        }

        protected void loadTable()
        {
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped wrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>A1");
            t.Append("</td>");
            t.Append("<td class='t_header' title='AAAA/MM/DD'>FECHA A1 ");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A1");
            t.Append("</td>");
            t.Append("<td class='t_header' title='AAAA/MM/DD'>FECHA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD");
            t.Append("</td>");
            t.Append("<td class='t_header'>SCRAPS");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFICIENCIA");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD");
            t.Append("</td>");
            t.Append("<td class='t_header'>SCRAPS");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFICIENCIA");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }

   
        protected void getTable()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            System.Diagnostics.Debug.WriteLine("" + Createquery());
            NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display table-striped wrap' cellspacing='0' runat='server'id='table_players' style='width: 1375px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>A1");
            t.Append("</td>");
            t.Append("<td class='t_header' title='AAAA/MM/DD'>FECHA A1 ");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A1");
            t.Append("</td>");           
            t.Append("<td class='t_header'>A2");
            t.Append("</td>");
            t.Append("<td class='t_header' title='AAAA/MM/DD'>FECHA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD");
            t.Append("</td>");
            t.Append("<td class='t_header'>SCRAPS");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFICIENCIA");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");

                while (dr.Read())
                {
                   // List<Object> l = WriteData(dr[11].ToString());
                    DateTime d2 = DateTime.Parse(dr[19].ToString()); 
                    String Activity = dr[20].ToString();
                if (Activity != "crp" || Activity == dr[14].ToString())
                {

                   // WriteData(dr[11].ToString());
                    t.Append("<tr>");
                    t.Append("<td>" + dr[0] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[1] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[2] + " " + dr[3] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[4] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[5] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[6] + "");
                    t.Append("</td>");
                    t.Append("<td>" + dr[7] + "");
                    t.Append("</td>");
                    t.Append("<td>" + Activity.ToUpper() + "");
                    t.Append("</td>");
                    t.Append("<td>" + d2.ToString("dd/MM/yyyy") + "");
                    t.Append("</td>");
                    t.Append("<td>" + $"{ d2: HH: mm: ss}");
                    t.Append("</td>");
                    t.Append("<td>" + dr[14].ToString().ToUpper() + "");
                    t.Append("</td>");
                    DateTime d = DateTime.Parse(dr[12].ToString());
                    t.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                    t.Append("</td>");
                    t.Append("<td>" + $"{ d: HH: mm: ss}");
                    t.Append("</td>");
                    TimeSpan result = d.Subtract(d2);
                    t.Append("<td>" + OutTime(result) + "");
                    t.Append("</td>");
                    if ((Activity == "pr" && (dr[14].ToString() == "cr")||(dr[14].ToString() == "crp")) || (Activity != "crp" && dr[14].ToString() == "cr"))
                    {
                      
                        t.Append("<td> " + dr[21]);
                        t.Append("</td>");
                        t.Append("<td>" + dr[9] + "");
                        t.Append("</td>");
                        t.Append("<td>" + dr[10] + "%");
                        t.Append("</td>");
                    }
                    else
                    {
                        t.Append("<td> " );
                        t.Append("</td>");
                        t.Append("<td> ");
                        t.Append("</td>");
                        t.Append("<td> ");
                        t.Append("</td>");
                    }
                    
                  
                    t.Append("</tr>");
                }
                }
            
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>CT");
            t.Append("</td>");
            t.Append("<td class='t_header'>NUM");
            t.Append("</td>");
            t.Append("<td class='t_header'>NOMBRE");
            t.Append("</td>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>OP");
            t.Append("</td>");
            t.Append("<td class='t_header'>A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A1");
            t.Append("</td>");
            t.Append("<td class='t_header'>A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>HORA A2");
            t.Append("</td>");
            t.Append("<td class='t_header'>TOTAL");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD");
            t.Append("</td>");
            t.Append("<td class='t_header'>SCRAPS");
            t.Append("</td>");
            t.Append("<td class='t_header'>EFICIENCIA");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
            conn.Close();
        }

    

        public String Createquery()
        {
            String query = "select workcenters.num_machine, users.idemployee, users.name, users.firstname," +
                " operations.job, clients.customer, pieces.part_number, operations.operation_service, " +
                " user_processes.start, user_processes.scraps ,  productivity, process_logs.parent_id, process_logs.created_at,  " +
                " process_logs.id, process_logs.activity, operations.standar_time,process_logs.user_process_id, mealtime, mealtime2, activity1.created_at, " +
                " activity1.activity,c.piezas " +
                " from process_logs" +
                " join processes on process_logs.process_id = processes.id" +
                " join workcenters on process_logs.workcenter_id = workcenters.id " +
                " join operations on processes.operation_id = operations.id" +
                " join pieces on pieces.id = operations.piece_id" +
                " join clients on pieces.client_id = clients.id " +
                " join process_logs as activity1 on activity1.id = process_logs.parent_id" +
                " join users on process_logs.user_id = users.id" +
                " join user_processes on process_logs.user_process_id = user_processes.id" +
                " full join (SELECT COUNT(id) as piezas, user_process_id FROM cycles WHERE  message='12345' GROUP BY user_process_id) as c on c.user_process_id=user_processes.id " +
                " where ((activity1.activity = 'pr' and(process_logs.activity = 'crp' or process_logs.activity = 'cr')) or(activity1.activity = 'ia' and(process_logs.activity = 'crp' or process_logs.activity = 'fa') or((activity1.activity = 'il' or activity1.activity = 'ial') and(process_logs.activity = 'ls' or process_logs.activity = 'ln')) ) or(activity1.activity = 'ih' and(process_logs.activity = 'crp' or process_logs.activity = 'fh'))) ";
            String param="";
            if (Request.Form["Fecha_inicio"] == ""){
                param += "and process_logs.created_at >= '04/04/2020'";
            }
            if (Request.Form["Fecha_inicio"] != ""){

                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at >= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != ""){
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                //fs = fs.AddHours(24).AddMinutes(59).AddSeconds(59);
                  fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                System.Diagnostics.Debug.WriteLine(fs);
                param += "and (process_logs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "") {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != ""){
                param += " and  part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }
            if (Request.Form["Job"] != ""){
                param += " and  operations.job= '" + Request.Form["Job"].Trim() + "' ";
            }
            if (Request.Form["UserList"] != "0"){
                param += " and idemployee= '" + Request.Form["UserList"] + "' ";
            }
            if(Request.Form["cts"] != "0"){
                param += " and num_machine= '" + Request.Form["cts"] + "' ";
            }
            param += " order by user_processes.created_at desc";
            return query+ param;
        }

        public double getDouble(String s)
        {
            double numero = 0.0;
            if (s != "")
            {
                numero = Convert.ToDouble(s);
            }
            
            return numero;
        }

        public double getEficiencia(double cantidad, double taktime, double total, String activity, DateTime inicio, DateTime final, DateTime m1, DateTime m2)
        {
            TimeSpan meal1 = Convert.ToDateTime(m1.ToString()).TimeOfDay;
            TimeSpan meal2 = Convert.ToDateTime(m2.ToString()).TimeOfDay;
            TimeSpan meal1a = Convert.ToDateTime(m1.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan meal2a = Convert.ToDateTime(m2.ToString()).AddMinutes(30).TimeOfDay;
            TimeSpan init = inicio.TimeOfDay;
            TimeSpan fin = final.TimeOfDay;
            double eficiencia = 0.0;
            if (activity == "pr"|| activity== "crp")
            {
                if (cantidad > 0)
                {
                    if(total >= 1.0)
                    {
                        if(TimeSpan.Compare(init, meal1)==(-1) && TimeSpan.Compare(fin, meal2a) == (1))
                        {
                            total = total - 60;
                            eficiencia = (((cantidad+1) * taktime) / total) * 100;
                          //  System.Diagnostics.Debug.WriteLine(init + " 2 comidas" + " " + cantidad + " " + taktime + " " + total);

                        }
                        else if(TimeSpan.Compare(init, meal1) == (-1) && TimeSpan.Compare(fin, meal1a) == (1) && (TimeSpan.Compare(fin, meal2) == (-1)))
                        {
                           total = total - 30;
                            eficiencia = (((cantidad+1) * taktime) / total) * 100;
                           // System.Diagnostics.Debug.WriteLine(init +" 1er comidas" + " "+cantidad+" "+taktime+" "+total);

                        }
                        else if(TimeSpan.Compare(init, meal2) == (-1) && TimeSpan.Compare(fin, meal2a) == (1) && TimeSpan.Compare(init, meal1a) == (1))
                        {
                            total = total - 30;
                            eficiencia = (((cantidad+1) * taktime) / total) * 100;
                         //  System.Diagnostics.Debug.WriteLine(init + " 2da comidas" + " " + cantidad + " " + taktime + " " + total);

                        }
                        else
                        {
                            eficiencia = (((cantidad+1) * taktime) / total) * 100;
                         //  System.Diagnostics.Debug.WriteLine(init + " sin comidas" + " " + cantidad + " " + taktime + " " + total);

                        }

                    }
                }
            }         
            return Truncate(eficiencia, 2);
        }

        public int cantidad(String cant, String activity) {
            int cantidad = 0;
            if (activity == "pr" || activity == "crp")
            {
                cantidad = Convert.ToInt32(cant);
            }
            return cantidad;
         }

        public String getCantidad(String userp, String activity)
        {
            String con = "0";
            if (activity == "pr" || activity == "crp")
            {
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand("SELECT COUNT(id) FROM cycles WHERE user_process_id='" + userp + "' and message='12345'", conn);
                // Execute the query and obtain a result set
                NpgsqlDataReader dr = command.ExecuteReader();           
                if (dr.Read())
                {
                    con = dr[0].ToString();
                }
                conn.Close();
            }
            return con;
        }
     

        public static double Truncate(double value, int decimales)
        {
            double aux_value = Math.Pow(10, decimales);
            return (Math.Truncate(value * aux_value) / aux_value);
        }

        public void ExportExcel(object sender, EventArgs e)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de CTS Detallados");
            pck.Workbook.Properties.Author = "Automaq Reporteador V2";
            pck.Workbook.Properties.Title = "Reporte de CTS Detallados";
            pck.Workbook.Properties.Subject = "Tecnologia procesos y maquinados SA de CV";
            pck.Workbook.Properties.Created = DateTime.Now;

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            // ws.Cells["A1"].LoadFromDataTable(new System.Data.DataTable(), true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("Img/logoc.png"));
            var excelImage = ws.Drawings.AddPicture("My Logo", image);
            //add the image to row 20, column E
            excelImage.SetPosition(0, 0, 0, 0);
            //Format the header for column 1-3
            using (ExcelRange Rng = ws.Cells[1, 5, 1, 5])
            {
                Rng.Value = "REPORTE CT'S DETALLADOS";
                Rng.Style.Font.Size = 20;
                Rng.Style.Font.Bold = true;
            }
            using (ExcelRange Rng = ws.Cells[1, 13, 1, 13])
            {
                Rng.Value = "Fecha del Reporte: " + DateTime.Now.ToString();
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
                ws.Row(20).Height = 30;
            }
            //a partir de aqui son contenidos propios de cada reporte

            //parametros de busqueda

            encabezadoExcel(ws);


            //encabezados de la tabla documento
            ExcelRange parametros = ws.Cells["A3:S3"];
            ExcelRange encabezado = ws.Cells["A8:S8"];
            encabezado.Style.Font.Color.SetColor(System.Drawing.Color.Ivory);
            encabezado.Style.Fill.PatternType = ExcelFillStyle.Solid;
            encabezado.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
            ws.Cells["B8"].Value = "CT";
            ws.Cells["C8"].Value = "NUM";
            ws.Cells["D8"].Value = "NOMBRE";
            ws.Cells["E8"].Value = "JOB";
            ws.Cells["F8"].Value = "CUSTOMER";
            ws.Cells["G8"].Value = "PART NUMBER";
            ws.Cells["H8"].Value = "OP";
            ws.Cells["I8"].Value = "A1";
            ws.Cells["J8"].Value = "FECHA A1";
            ws.Cells["K8"].Value = "HORA A1";
            ws.Cells["L8"].Value = "A2";
            ws.Cells["M8"].Value = "FECHA A2";
            ws.Cells["N8"].Value = "HORA A2";
            ws.Cells["O8"].Value = "TOTAL";
            ws.Cells["P8"].Value = "CANTIDAD";
            ws.Cells["Q8"].Value = "SCRAPS";
            ws.Cells["R8"].Value = "EFICIENCIA";
            parametros.AutoFitColumns();
            encabezado.AutoFitColumns();

            dataExcel(ws);

            ws.Protection.IsProtected = false;
            ws.Protection.AllowSelectLockedCells = false;

            //Write it back to the client
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ReporteCTSDetallados" + DateTime.Now.ToString("d") + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
        }

        public void encabezadoExcel(ExcelWorksheet ws)
        {
            ws.Cells["C3"].Value = "NUM DE MAQUINA:";
            ws.Cells["D3"].Value = Request.Form["cts"];
            ws.Cells["F3"].Value = "ID EMPLEADO:";
            ws.Cells["G3"].Value = Request.Form["UserList"];
            ws.Cells["C4"].Value = "JOB:";
            ws.Cells["D4"].Value = Request.Form["Job"];
            ws.Cells["C5"].Value = "CUSTOMER:";
            ws.Cells["D5"].Value = Request.Form["Customer"];
            ws.Cells["C6"].Value = "PART NUMBER:";
            ws.Cells["D6"].Value = Request.Form["Partn"]; ;
            ws.Cells["F4"].Value = "DESDE:";
            ws.Cells["G4"].Value = Request.Form["Fecha_inicio"];
            ws.Cells["F5"].Value = "HASTA:";
            ws.Cells["G5"].Value = Request.Form["Fecha_final"];          
            ws.Cells["M3"].Value = "SOLICITADO POR:";
            ws.Cells["M4"].Value = Session["User"] + " " + Session["LastName"];

        }

        public void dataExcel(ExcelWorksheet ws)
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; " + "Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(Createquery(), conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            int excel = 9;
            while (dr.Read())
            {               
                DateTime d2 = DateTime.Parse(dr[19].ToString()); ;
                String Activity = dr[20].ToString();
                DateTime d = DateTime.Parse(dr[12].ToString());
                TimeSpan result = d.Subtract(d2);
                DateTime m1 = Convert.ToDateTime(dr[17].ToString());
                DateTime m2 = Convert.ToDateTime(dr[18].ToString());
                ws.Cells[String.Format("B{0}", excel)].Value = dr[0];
                ws.Cells[String.Format("C{0}", excel)].Value = dr[1];
                ws.Cells[String.Format("D{0}", excel)].Value = dr[2] + " " + dr[3];
                ws.Cells[String.Format("E{0}", excel)].Value = dr[4] ;
                ws.Cells[String.Format("F{0}", excel)].Value = dr[5];
                ws.Cells[String.Format("G{0}", excel)].Value = dr[6];
                ws.Cells[String.Format("H{0}", excel)].Value = dr[7];
                ws.Cells[String.Format("I{0}", excel)].Value = Activity;
                ws.Cells[String.Format("J{0}", excel)].Value = d2.ToString("dd/MM/yyyy");
                ws.Cells[String.Format("K{0}", excel)].Value = ""+$"{ d2: HH: mm: ss}";
                ws.Cells[String.Format("L{0}", excel)].Value = dr[14];              
                ws.Cells[String.Format("M{0}", excel)].Value = d.ToString("dd/MM/yyyy");
                ws.Cells[String.Format("N{0}", excel)].Value = "" + $"{ d: HH: mm: ss}";                
                ws.Cells[String.Format("O{0}", excel)].Value = ""+OutTime(result);
                if ((Activity == "pr" && (dr[14].ToString() == "cr") || (dr[14].ToString() == "crp")) || (Activity != "crp" && dr[14].ToString() == "cr"))
                {
                    ws.Cells[String.Format("P{0}", excel)].Value = dr[21];                   
                }
                else
                {
                    ws.Cells[String.Format("P{0}", excel)].Value = "";
                }
              
                ws.Cells[String.Format("Q{0}", excel)].Value = dr[9];               
                ws.Cells[String.Format("R{0}", excel)].Value = dr[10] + "%";
                excel++;
            }
            conn.Close();
        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }
}