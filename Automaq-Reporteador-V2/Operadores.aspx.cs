﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class Operadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Access();
            //
            //printUser(getUser());
            resume();
            //printCT(getCT("A"));        
            /*
            if (CheckSession())
            {
                SessionDetails();
                if (Session["param"].ToString() != ""){
                    loadData(Createquery2(Session["param"].ToString()));
                }else{
                    loadTable();
                }
            }
            else{
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
            // Session["param"] = "";
        */
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
            Session["param"] = "";
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
            Session["param"] = "";
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Conteos.aspx");

        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }
        public void Asistencias2(object sender, EventArgs e)
        {
            Session["param"] = "";
            System.Diagnostics.Debug.WriteLine("aqui");
            Response.Redirect("OperadoresB.aspx");
        }

    
        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }


      
        public void verUser(object sender, EventArgs e)
        {
            
            resume();
           // GetPlanta(getTurn());
           // GetSupervisor(getTurn(), getIdSupervisores(getTurn()));
        }


        public String getTurn()
        {
            String firstTurn="";
            String SecondTurn = "";
            String query = "SELECT DISTINCT first_turn, second_turn from schedules where schedules.week_number = EXTRACT(week from now())";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                firstTurn = dr[0].ToString();
                SecondTurn = dr[1].ToString();
            }
            conn.Close();
            int hour = DateTime.Now.Hour;
            if (hour >= 6 && hour < 15)
            {
                //System.Diagnostics.Debug.WriteLine("turn current: " + firstTurn);
                return firstTurn;
            }
            else
            {
                //System.Diagnostics.Debug.WriteLine("turn current: " + SecondTurn);
                return SecondTurn;
            }
        }
        //Resumen planta
        public List<List<String>> GetPlanta(String turno, List<List<String>> data)
        {
            int asistencias = 0;
            int faltas = 0;
            int carga = 0;
            List<String> info = new List<string>();
            info.Add("PLANTA");
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT count(idemployee) from employee WHERE inside= 1 and turno= '" + turno + "' and active=1 and supervisor is not null";
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                asistencias = Convert.ToInt32(dr[0].ToString());
                info.Add(dr[0].ToString());
            }
            conn.Close();
            conn.Open();
            query = "SELECT count(idemployee) from employee WHERE turno= '" + turno + "' and active=1 ";
            command = new NpgsqlCommand(query, conn);
            dr = command.ExecuteReader();
            if (dr.Read())
            {
                int total = Convert.ToInt32(dr[0].ToString());
                faltas = total - asistencias;
                info.Add(faltas.ToString());
            }
            conn.Close();
            conn.Open();
            query = "SELECT Count(distinct num_machine) FROM processes " +
                " JOIN workcenters on workcenters.id = processes.workcenter_id where(status <> 'cr' and processes.deleted_at is null or status is null and processes.\"order\" is not null) and processes.deleted_at is null ";
            command = new NpgsqlCommand(query, conn);
            dr = command.ExecuteReader();
            if (dr.Read())
            {
                carga = Convert.ToInt32(dr[0].ToString());              
                info.Add(dr[0].ToString());
            }
            conn.Close();
            info.Add((asistencias - carga).ToString());
            data.Add(info);
            return data;
        }

        //fin resumen planta
        //getSupervisores
        //Resumen planta
        public List<List<String>> GetSupervisor(String turno, List<List<String>> supervisores, List<List<String>> data)
        { 
          //  System.Diagnostics.Debug.WriteLine("tamaño: " +supervisores.Count);
            foreach (List<String> a in supervisores)
            {
                int asistencias = 0;
                int faltas = 0;
                int carga = 0;

                List<String> info = new List<string>();
              //  System.Diagnostics.Debug.WriteLine("Resumen Supervisor: "+a[2]);
                info.Add(a[2]+" "+a[3]+"." );
                NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
                conn.Open();
                String query = "SELECT count(idemployee) from employee WHERE inside= 1 and turno= '" + turno + "' and supervisor= " + a[1] + " and active=1";
              //  System.Diagnostics.Debug.WriteLine(query);
                NpgsqlCommand command = new NpgsqlCommand(query, conn);
                NpgsqlDataReader dr = command.ExecuteReader();
                if (dr.Read())
                {
                    asistencias = Convert.ToInt32(dr[0].ToString());
                    info.Add(dr[0].ToString());

                }
                conn.Close();
                conn.Open();
                query = "SELECT count(idemployee) from employee WHERE turno= '" + turno + "' and supervisor= " + a[1] + " and active=1";
                command = new NpgsqlCommand(query, conn);
               // System.Diagnostics.Debug.WriteLine(query);

                dr = command.ExecuteReader();
                if (dr.Read())
                {
                    int total = Convert.ToInt32(dr[0].ToString());
                    faltas = total - asistencias;
                    info.Add(faltas.ToString());

                }
                conn.Close();
                conn.Open();
                query = "SELECT count(distinct num_machine)" +
                    " FROM processes JOIN workcenters on workcenters.id = processes.workcenter_id join user_workcenter on user_workcenter.workcenter_id = workcenters.id join users on users.id = user_workcenter.user_id" +
                    " where(status <> 'cr' and processes.deleted_at is null or status is null and processes.\"order\" is not null) and processes.deleted_at is null  and users.id = '" + a[0]+ "' ";
                command = new NpgsqlCommand(query, conn);
                //System.Diagnostics.Debug.WriteLine(query);
                dr = command.ExecuteReader();
                if (dr.Read())
                {
                    carga = Convert.ToInt32(dr[0].ToString());

                    info.Add(dr[0].ToString());
                }
                conn.Close();
                info.Add((asistencias - carga).ToString());
                data.Add(info);
            }
            return data;
        }

        //fin resumen planta

        //print cards
        public void printCards(List<List<String>> users)
        {
            tabla2.Controls.Clear();
            tabla3.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<div class='row row-cols-2 row-cols-md-6 g-6'>");
            foreach(List<String> ct in users)
            {
                t.Append("<div class='col'>");
                t.Append("<div class='card text-center'>");
                t.Append("<div class='card-header' Style='font-size: 13px'>"+ ct[0]+"</div>");
                if (ct[0] == "PLANTA")
                {
                    t.Append("<div class='card-body' style='background-color:lightblue'>");
                }
                else
                {
                    t.Append("<div class='card-body'>");
                }
                t.Append("<h5 class='card-title'>Asistencias: "+ct[1]+"</h5>");
                t.Append("<h5 class='card-title'>Faltas: "+ct[2]+"</h5>");
                t.Append("<h5 class='card-title'>Maquinas con carga: "+ct[3]+"</h5>");
                //t.Append("<a href='#' class='btn btn-primary'>Button</a>");
                if (ct[0] == "PLANTA")
                {
                    t.Append("<a href='#' class='btn btn-primary'></a>");
                }
                else
                {
                    t.Append("<a href=\'#"+ct[0]+"\' class='btn btn-primary'>VER MAS</a>");
                }
              
                t.Append("</div>");
                t.Append("<div class='card-footer text-muted'>Sin asignatura: "+ct[4]+"</div>");
                t.Append("</div>");
                t.Append("</div>");
                t.Append("</br>");
            }
            t.Append("</div>");
            Literal s = new Literal { Text = t.ToString() };
            tabla3.Controls.Add(s);
        }

        //print cards fin

        private void resume()
        {
            List<List<String>> data = new List<List<string>>();
            GetPlanta(getTurn(), data);
            GetSupervisor(getTurn(), getIdSupervisores(getTurn()), data);
        
            printCards(data);
            printTablesData(getIdSupervisores(getTurn()));

        }

        public void printTablesData(List<List<String>> supervisores)
        {
            StringBuilder t = new StringBuilder();
            t.Append("</br>");
            t.Append("</br>");

            foreach (List<String> super in supervisores)
            {
                String superv = super[2] + " " + super[3];
                //printTablas(GetDataEmployee(getTurn(), super[1]), superv, GetDataEmployeeFalta(getTurn(), super[1]), super[0]);
                List<List<String>> s = combine(GetDataEmployee(getTurn(), super[1]), GetDataEmployeeFalta(getTurn(), super[1]),getPrestados(super[1]) ,getCTActives(super[1]));
                printTablas(s, superv); 

            }
          
        }

        //supervisores por turno
        public List<List<String>> getIdSupervisores(String turno)
        {
           List<List<String>> num = new List<List<string>>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT distinct users.id, \"Supervisor\".id ,name, firstname, users.idemployee from users join \"Supervisor\" on users.idemployee= \"Supervisor\".idemployee::VARCHAR where turno='" + turno+ "'";
          //  System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                List<String> super = new List<string>();

                //System.Diagnostics.Debug.WriteLine("Asistencias: " + dr[0].ToString());
                super.Add(dr[0].ToString());
                super.Add(dr[1].ToString());
                super.Add(dr[2].ToString());
                super.Add(dr[3].ToString());
                super.Add(dr[4].ToString());
                num.Add(super);
            }
            conn.Close();
            return num;
        }
        //supervisores por turno fin
        //Lista empleados por supervisor
        public List<List<String>> GetDataEmployee(String turno, String supervisor)
        {
            List<List<String>> employees = new List<List<string>>();

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT employee.idemployee, concat(users.name,' ', firstname) as nombre , activos.status, activos.num_machine from employee FULL JOIN(select users.idemployee, concat(name,' ', firstname) as nombre ,processes.status,workcenters.num_machine from  users " +
                " join user_processes on users.id = user_processes.user_id" +
                " join processes on processes.id = user_processes.process_id" +
                " join workcenters on processes.workcenter_id = workcenters.id" +
                " join employee on users.idemployee = (employee.idemployee::VARCHAR)" +
                " where user_processes.active = true  order by idemployee) as activos on activos.idemployee = employee.idemployee::VARCHAR" +
                " join users on users.idemployee = employee.idemployee::VARCHAR" +
                " WHERE inside = 1 and turno = '"+turno+"' and  supervisor=" + supervisor + " and active=1 order by idemployee; ";
           // System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                List<String> super = new List<string>();

                //System.Diagnostics.Debug.WriteLine("Asistencias: " + dr[0].ToString());
                super.Add(dr[0].ToString());
                super.Add(dr[1].ToString());
                super.Add(dr[2].ToString());
                super.Add(dr[3].ToString());
                employees.Add(super);
            }
            conn.Close();

            return employees;
        }
        public List<List<String>> GetDataEmployeeFalta(String turno, String supervisor)
        {
            List<List<String>> employees = new List<List<string>>();

            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT employee.idemployee, concat(users.name,' ', firstname) as nombre , activos.status, activos.num_machine from employee FULL JOIN(select users.idemployee, concat(name,' ', firstname) as nombre ,processes.status,workcenters.num_machine from  users " +
                " join user_processes on users.id = user_processes.user_id" +
                " join processes on processes.id = user_processes.process_id" +
                " join workcenters on processes.workcenter_id = workcenters.id" +
                " join employee on users.idemployee = (employee.idemployee::VARCHAR)" +
                " where user_processes.active = true  order by idemployee) as activos on activos.idemployee = employee.idemployee::VARCHAR" +
                " join users on users.idemployee = employee.idemployee::VARCHAR" +
                " WHERE inside = 2 and turno = '" + turno + "' and  supervisor=" + supervisor + " and active=1 order by idemployee; ";
           // System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
           // System.Diagnostics.Debug.WriteLine("Faltas");
            while (dr.Read())
                
               {
                List<String> super = new List<string>();
                //System.Diagnostics.Debug.WriteLine("Asistencias: " + dr[0].ToString());
                super.Add(dr[0].ToString());
                super.Add(dr[1].ToString());
                super.Add(dr[2].ToString());
                super.Add(dr[3].ToString());
                employees.Add(super);
               // System.Diagnostics.Debug.WriteLine(" --" + dr[0]+ " --" + dr[1] + " --" + dr[2] + " --" + dr[3] );
                }
            conn.Close();

            return employees;
        }

        //
        public void printTablas(List<List<String>> users, String su, List<List<String>> faltas, String idsuper)
        {
         //   tabla2.Controls.Clear();
          //  tabla3.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("</br>");
            t.Append("<div class='row row-cols-2 row-cols-md-6 g-6'>");
            t.Append("<div class='col'>");
            t.Append("<table class='table1' style='width: 500px;'>");
            t.Append("<caption class='hola'>"+su+"</caption>");
            t.Append("<thead style='background-color: lightblue'>");
            t.Append("<tr>");
            t.Append("<th>ID</th>");
            t.Append("<th>NAME</th>");
            t.Append("<th>ACTIVITY</th>");
            t.Append("<th>NUM_MACHINE</th>");
            t.Append("</tr></thead>");
            t.Append("<tbody>");

            foreach (List<String> ct in users)
            {
                t.Append("<tr>");
                t.Append("<th>" + ct[0] + "</th>");
                t.Append("<th>" + ct[1] + "</th>");
                t.Append("<th>" + ct[2].ToUpper() + "</th>");
                t.Append("<th>" + ct[3] +"</th>");
                t.Append("</tr>");               
            }
            foreach (List<String> ct in faltas)
            {
                t.Append("<tr>");
                t.Append("<th style='color: red'>" + ct[0] + "</th>");
                t.Append("<th style='color: red'>" + ct[1] + "</th>");
                t.Append("<th style='color: red'>" + ct[2].ToUpper() + "</th>");
                t.Append("<th style='color: red'>" + ct[3] + "</th>");
                t.Append("</tr>");
            }
            t.Append("</tbody>");
            t.Append("</table>");
            t.Append("</div>");
            t.Append("</div>");
            Literal s = new Literal { Text = t.ToString() };
            tabla3.Controls.Add(s);
        }

        public void printTablas(List<List<String>> users, String su)
        {
            //   tabla2.Controls.Clear();
            //  tabla3.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("</br>");
            t.Append("<div class='row row-cols-2 row-cols-md-6 g-6'>");
            t.Append("<div class='col'>");
            t.Append("<a name='" + su + ".'><h1></h1></a>");
            t.Append("<table class='table1' style='width: 500px;'>");
            t.Append("<caption class='hola' >" + su + "</caption>");
            t.Append("<thead style='background-color: lightblue'>");
            t.Append("<tr>");
            t.Append("<th style='text-align:center'>ID</th>");
            t.Append("<th style='text-align:center'>NAME</th>");
            t.Append("<th>ACTIVITY</th>");
            t.Append("<th>NUM_MACHINE</th>");
            t.Append("<th>CT_CARGA</th>");
            t.Append("</tr></thead>");
            t.Append("<tbody>");

            foreach (List<String> ct in users)
            {
                //  System.Diagnostics.Debug.WriteLine("error: "+ ct.Count);
                if (ct.Count == 0)
                {

                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Comparacio: "+ct[2]+ "dos: "+ ct[6]);
                    if (ct[4] == "A")
                    {

                        t.Append("<tr>");
                        t.Append("<th>" + ct[0] + "</th>");
                        t.Append("<th>" + ct[1] + "</th>");
                        t.Append("<th  style='text-align:center'>" + ct[2].ToUpper() + "</th>");
                        t.Append("<th>" + ct[3] + "</th>");
                        if (ct[6] != "" && ct[6] != "cr" && ct[6] != "crp")
                        {
                            t.Append("<th style='background-color: green'>" + ct[5] + "</th>");
                        }
                    
                        else if (ct[5].ToString() == "")
                        {
                            t.Append("<th>" + ct[5] + "</th>");
                        }
                        else
                        {
                            t.Append("<th style='background-color: yellow'>" + ct[5] + "</th>");
                        }

                        t.Append("</tr>");
                    }
                    if (ct[4] == "F")
                    {
                        t.Append("<tr>");
                        t.Append("<th style='color: red'>" + ct[0] + "</th>");
                        t.Append("<th style='color: red'>" + ct[1] + "</th>");
                        t.Append("<th style='color: red; text-align:center;'>" + ct[2].ToUpper() + "</th>");
                        t.Append("<th style='color: red'>" + ct[3] + "</th>");
                        if (ct[6] != "" && ct[6] != "cr" && ct[6] != "crp")
                        {
                            t.Append("<th style='background-color: green'>" + ct[5] + "</th>");
                        }
                        else if (ct[5].ToString() == "")
                        {
                            t.Append("<th>" + ct[5] + "</th>");
                        }
                        else
                        {
                            t.Append("<th style='background-color: yellow'>" + ct[5] + "</th>");
                        }

                        t.Append("</tr>");
                    }
                    else if (ct[4] == "P")
                    {
                       
                        t.Append("<tr>");
                        t.Append("<th style='color: blue'>" + ct[0] + "</th>");
                        t.Append("<th style='color: blue'>" + ct[1] + "</th>");
                        t.Append("<th style='color: blue; text-align:center;'>" + ct[2].ToUpper() + "</th>");
                        t.Append("<th style='color: blue'>" + ct[3] + "</th>");
                        if (ct[6] != "" && ct[6] != "cr" && ct[6] != "crp")
                        {
                            t.Append("<th style='background-color: green'>" + ct[5] + "</th>");
                        }
                        else if (ct[5].ToString() == "")
                        {
                            t.Append("<th>" + ct[5] + "</th>");
                        }
                        else
                        {
                            t.Append("<th style='background-color: yellow'>" + ct[5] + "</th>");
                        }

                        t.Append("</tr>");
                    }
                    else if(ct[4]== "-----")
                    {
                        t.Append("<tr>");
                        t.Append("<th>" + ct[0] + "</th>");
                        t.Append("<th>" + ct[1] + "</th>");
                        t.Append("<th>" + ct[2].ToUpper() + "</th>");
                        t.Append("<th>" + ct[3] + "</th>");
                        if (ct[6] != "" && ct[6] != "cr" && ct[6] != "crp")
                        {
                            t.Append("<th style='background-color: green'>" + ct[5] + "</th>");
                        }
                        else if (ct[5].ToString() == "")
                        {
                            t.Append("<th>" + ct[5] + "</th>");
                        }
                        else
                        {
                            t.Append("<th style='background-color: yellow'>" + ct[5] + "</th>");
                        }

                        t.Append("</tr>");
                    }
                }             
            }  
            t.Append("</tbody>");
            t.Append("</table>");
            t.Append("</div>");
            t.Append("</div>");
            Literal s = new Literal { Text = t.ToString() };
            tabla3.Controls.Add(s);
        }

        public List<List<String>> getCTActives(String id)
        {
            List<List<String>> vs = new List<List<string>>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT num_machine, max(status) FROM processes JOIN workcenters on workcenters.id = processes.workcenter_id " +
                " join user_workcenter on user_workcenter.workcenter_id = workcenters.id" +
                " join users on users.id = user_workcenter.user_id join \"Supervisor\" on \"Supervisor\".idemployee::varchar = users.idemployee " +
                " where((status <> 'cr'  ) and processes.deleted_at is null or status is null and processes.\"order\" is not null) and processes.deleted_at is null " +
                "  and \"Supervisor\".id = "+id+ "  GROUP BY num_machine ORDER BY num_machine; ";
            System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                List<String> super = new List<string>();
                //System.Diagnostics.Debug.WriteLine("Asistencias: " + dr[0].ToString());
                super.Add(dr[0].ToString());
                super.Add(dr[1].ToString());
                
                vs.Add(super);
            }
            conn.Close();

            return vs;
        }

        public List<List<String>> getPrestados(String id)
        {
            List<List<String>> vs = new List<List<string>>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            String query = "SELECT DISTINCT num_machine, status,  employee.idemployee, b.name, b.firstname  FROM  processes " +
                " JOIN workcenters on workcenters.id = processes.workcenter_id" +
                " join user_workcenter on user_workcenter.workcenter_id = workcenters.id" +
                " join users on users.id = user_workcenter.user_id" +
                " join \"Supervisor\" on \"Supervisor\".idemployee::varchar = users.idemployee" +
                " join user_processes on processes.id = user_processes.process_id" +
                " join users as b on b.id = user_processes.user_id" +
                " join employee on b.idemployee = employee.idemployee::VARCHAR" +
                " where(status <> 'cr' and processes.deleted_at is null or status is null and processes.\"order\" is not null) and processes.deleted_at is null  and \"Supervisor\".id = "+id+"  and employee.supervisor != "+id+" and user_processes.active = true ORDER BY idemployee;";
            //System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            while (dr.Read())
            {
                List<String> super = new List<string>();
                //System.Diagnostics.Debug.WriteLine("Asistencias: " + dr[0].ToString());
                super.Add(dr[2].ToString());
                super.Add(dr[3].ToString()+" "+dr[4]);
                super.Add(dr[1].ToString());
                super.Add(dr[0].ToString());
                vs.Add(super);
            }
            conn.Close();
            return vs;
        }

        public List<List<String>> combine(List<List<String>> usuarios, List<List<String>> faltas, List<List<String>> prestados,List<List<String>> cts)
        {
            List<List<String>> result = new List<List<string>>();
            //System.Diagnostics.Debug.WriteLine("Total de usuarios: " + (usuarios.Count + faltas.Count)+"    cts con carga: "+cts.Count);
            // System.Diagnostics.Debug.WriteLine("prestados:::"+prestados.Count);

            foreach(List<String> s in prestados)
            {
              //  System.Diagnostics.Debug.WriteLine("prestados: "+s[0] + s[1] + s[2] +s[3]);
            }
            if ((usuarios.Count + faltas.Count+ prestados.Count) <= cts.Count)
            {
              //  System.Diagnostics.Debug.WriteLine("opcion 1" + "op" + (usuarios.Count + faltas.Count) + "  cts" + cts.Count);

                 
                int c = 0;
                int p = 0;
                for(int i=0; i<cts.Count; i++)
                {
                    List<String> user = new List<string>();
                    if (i < usuarios.Count)
                    {
                        user.Add(usuarios[i][0]); user.Add(usuarios[i][1]); user.Add(usuarios[i][2]); user.Add(usuarios[i][3]); user.Add("A");
                        user.Add(cts[i][0]);
                        user.Add(cts[i][1]);
                    }
                
                  //  System.Diagnostics.Debug.WriteLine(faltas.Count+"-----" + c+"contador"+i);
                    if(i>=usuarios.Count &&  (c<faltas.Count) && faltas.Count>0 )
                    {
                        //System.Diagnostics.Debug.WriteLine("Faltas "+faltas[c].Count+ "faltas "+c);
                        if (c < faltas.Count)
                        {
                            user.Add(faltas[c][0]);
                            user.Add(faltas[c][1]);
                            user.Add(faltas[c][2]);
                            user.Add(faltas[c][3]);
                            user.Add("F");
                            user.Add(cts[i][0]);
                            user.Add(cts[i][1]);
                            c++;
                        }
                        
                    }
                    //System.Diagnostics.Debug.WriteLine("prestados count"+ prestados.Count+" p"+p+" contador"+i );
                    if (i>=(usuarios.Count + faltas.Count) && prestados.Count > 0 &&p<prestados.Count)
                    {
                        user.Add(prestados[p][0]);
                        user.Add(prestados[p][1]);
                        user.Add(prestados[p][2]);
                        user.Add(prestados[p][3]);
                        user.Add("P");
                        user.Add(cts[i][0]);
                        user.Add(cts[i][1]);
                        p++;
                    }
                    else
                    {                      
                        user.Add("-----"); user.Add("-----"); user.Add("-----"); user.Add("-----"); user.Add("-----");
                        user.Add(cts[i][0]);
                        user.Add(cts[i][1]);
                    }                 
                    result.Add(user);
                }
            }
            else if((usuarios.Count + faltas.Count+ prestados.Count) > cts.Count)
            {
               //System.Diagnostics.Debug.WriteLine("opcion 2" + "op"+ (usuarios.Count+faltas.Count)+"  cts"+ cts.Count);
                int c = 0;
                int p = 0;
                for (int i = 0; i < (usuarios.Count+faltas.Count+prestados.Count); i++)
                {
                    List<String> user = new List<string>();
                    if (i < usuarios.Count)
                    {
                        user.Add(usuarios[i][0]); user.Add(usuarios[i][1]); user.Add(usuarios[i][2]); user.Add(usuarios[i][3]); user.Add("A");
                        if (i < cts.Count)
                        {
                            user.Add(cts[i][0]);
                            user.Add(cts[i][1]);
                        }
                        else
                        {
                            user.Add("");
                            user.Add("");
                        }                      
                    }
                    if (i >= usuarios.Count && faltas.Count > 0 && (i<faltas.Count+usuarios.Count))
                    {
                     //  System.Diagnostics.Debug.WriteLine("conteo: "+ (usuarios.Count)+" faltas: "+(faltas.Count)+" contador"+i);
                        user.Add(faltas[c][0]); user.Add(faltas[c][1]); user.Add(faltas[c][2]); user.Add(faltas[c][3]); user.Add("F");
                        if (i < cts.Count){
                            user.Add(cts[i][0]);
                            user.Add(cts[i][1]);
                        }else
                        {
                            user.Add("");
                            user.Add("");
                        }
                        c++;
                    }
                   // System.Diagnostics.Debug.WriteLine("conteo: " + (usuarios.Count) + " faltas: " + (faltas.Count) + " prestados: " + (prestados.Count) + " contador" + i);
                    if (i >= (usuarios.Count+faltas.Count) && prestados.Count > 0 )
                    {
                        // System.Diagnostics.Debug.WriteLine("conteo: "+ (usuarios.Count)+" faltas: "+(faltas.Count) + " prestados: " + (prestados.Count) + " contador"+i+"p"+p);
                        user.Add(prestados[p][0]); user.Add(prestados[p][1]); user.Add(prestados[p][2]); user.Add(prestados[p][3]); user.Add("P");
                        if (i < cts.Count)
                        {
                            user.Add(cts[i][0]);
                            user.Add(cts[i][1]);
                        }
                        else
                        {
                            user.Add("");
                            user.Add("");
                        }
                        p++;
                    }
                    result.Add(user);
                }
            }
            /*
            foreach (List<String> v in result)
            {
                foreach (String s in v)
                {
                    System.Diagnostics.Debug.Write(s + " ");
                }
               System.Diagnostics.Debug.WriteLine("");
            }     */
           return result;
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

    }

}