﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TiemposMuertos.aspx.cs" Inherits="Automaq_Reporteador_V2.TiemposMuertos" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">

    <!-- Estilos css y estilos DataTables -->
    <link rel="stylesheet" href="css/nav-bar.css">   
    <link rel="stylesheet" href="css/sidebar.css">
     

    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/tm.css">
    <link rel="stylesheet" href="css/descarga-logs.css">
    <link rel="stylesheet" href="css/modal-usuario.css">
    <link rel="stylesheet" href="css/DataTables.css">
    <link rel="stylesheet" href="css/DataTables.Select.min.css">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap4.min.css">

    <title>Bienvenido a Automaq Reporteador</title>
   
</head>

<body class="background-dark">

    <form id="f_menuPage" runat="server">
    <!-- Barra de navegación -->
    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>
        <img class="img-logo" src="Img/logotecmaq.png" alt="">
        <!--<label class="logo">Reporteador</label> -->
        
        <ul>
            <li><asp:LinkButton runat="server" CssClass="hyperlink active" NavigateUrl="#" ID="home"  Text="Inicio"/></li>
            <!-- Codigo para ejecutar funciones Javascript desde la propiedad href de los elementos 'hyperlink' -->
            <li><asp:HyperLink runat="server" CssClass="hyperlink" NavigateUrl="javascript:show_propiedadesUser();" ID="user_prop_hyper" Text="Mi perfil"/></li>
            <!-- //Fin de código -->
            <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" onclick="Asistencias1" ID="Asistencias" Text="ASISTENCIAS"/></li> <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" onclick="Platform" ID="admin_grupos_hyper" Text="Automaq Plataforma"/></li>
            <li><asp:LinkButton ID="link_button" CssClass="hyperlink" runat="server" onclick="Logout"  Text="Cerrar Sesión" /></li>
        </ul>
    </nav>
      

    <!-- Barra lateral de grupos -->

           
       <div class="sidebar2" id="slide"  runat="server" >
        <h1 id="minimize" class="w3-button w3-teal w3-xlarge w3-right w3-hide-large" style="color:black" font-size="16px" onclick="w3_close();">&#8854;</h1>
    </div>

       <div class="sidebar" id="contenedor"  runat="server" >
        
       <br />
         <br /> <br /> <br />
        <h1>&nbsp;Reportes</h1>
        
        <asp:PlaceHolder id="gru" runat="server"></asp:PlaceHolder>    
        <li><asp:LinkButton runat="server" ID="t1" OnClick="Jobs1"      text="Jobs" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t3" OnClick="Conteos1"   text="Conteos" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t4" OnClick="Eficiencia1" text="Eficiencia" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t5" OnClick="Supervisor1" text="Supervisor" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t6" OnClick="TiemposMuertos1" text="Tiempos Muertos" CssClass="hyperlink  active"/> </li>
        <li><asp:LinkButton runat="server" ID="t7" OnClick="CTS1" text="CT'S" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t8" OnClick="CTSDetallados1" text="CT'S Detallados" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t9" OnClick="Mantenimientos1" text="Mantenimientos" CssClass="hyperlink"/> </li>
        <li><asp:LinkButton runat="server" ID="t10" OnClick="Calidad1" text="Calidad" CssClass="hyperlink"/> </li>
            

    </div>

        <div class="titulo">
               <h2 style="color:black">REPORTE TIEMPOS MUERTOS</h2>
    </div>
        <div class="table-container2" id="con" > 
            <h1 class="w3-button w3-teal w3-xlarge w3-right w3-hide-large" style="color:black" font-size="16px" onclick="w3_close();">&#9776;</h1>
        </div>
             <div class="eticon" id="eticont">
            <p class="parrafo">CT: </p>
            <p class="parrafo">Job: </p>
            <p class="parrafo">Customer: </p>
            <p class="parrafo">Part number: </p>
        </div> 

            <div class="eticon2" id="eticont2">
            <p class="parrafo">Empleado: </p>
            <p class="parrafo">Desde: </p>
            <p class="parrafo">Hasta: </p>
            
        </div> 
         <div class="controles1" id="controles">
           
                  <!-- Aqui van los controles de busqueda -->
           <asp:DropDownList id="cts"  CssClass="entrada_text2"  runat="server">
             <asp:ListItem Selected="True" Value="0"> Todos los CT'S </asp:ListItem>            
             </asp:DropDownList>
           <input id="JobNumber" runat="server" class="entrada_text2" size="15" maxlength="30" type="hidden" placeholder="JOB"  >
        <input id="Job" runat="server" class="entrada_text2" size="15" maxlength="30" type="text" placeholder="Job" >

        <input id="Customer" runat="server" class="entrada_text2" size="15" maxlength="30" type="text" placeholder="Customer" >
        
        <input id="Partn" runat="server" class="entrada_text2" size="15" maxlength="30" type="text" placeholder="Part Number" >
         </div>



         <div class="controles2" id="controles2">
        <asp:DropDownList id="UserList"
                     CssClass="entrada_text2"                    
                    runat="server">
                  <asp:ListItem Selected="True" Value="0"> Todos los empleados</asp:ListItem>  
        </asp:DropDownList>
         
        

    
         <input placeholder="Desde"  type="date" runat="server"  name="inicio_fecha" id="Fecha_inicio" class="entrada_text2" >
       
        
         <input placeholder="Hasta"  type="date" runat="server"  name="inicio_fecha" id="Fecha_final" class="entrada_text2" >
        </div>
        <!-- Finaliza -->   
      
     
           <div class="table-container4" id="controles2">
          <asp:Button ID="Buscar" CssClass="bbtn btn-dark btn-sm"  runat="server" onclick="Find" Text="Buscar Registros" />
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          <asp:Button ID="Reset" CssClass="btn btn-dark btn-sm"     runat="server"  onclick="resetFilter" Text="Resetear Filtros" />
               </div>
      
        <div class="table-container5" id="controles3">
           <asp:Button ID="Excel" CssClass="btn btn-dark btn-sm"  runat="server" onclick="ExportExcel" Text="Exportar a Excel" />
            <p>&nbsp;&nbsp;&nbsp;</p>
          <asp:Button ID="Pdf" CssClass="btn btn-dark btn-sm"     runat="server"  Text="Exportar a PDF" />
        </div>
        <div class="table-container" id="tabla">
       
                   <asp:PlaceHolder id="tabla2" runat="server"></asp:PlaceHolder>
                   
        </div>
       
    <!-- MODAL PARA VER LAS PROPIEDADES DE LA SESION -->

    <div class="modal-user" id="modal_propiedades_user" runat="server">
        <div class="modal-user-content">
            <div class="modal-user-body">
                <h2>Propiedades del Usuario</h2>
                    <br />
                    <p><b>Numero de Nomina:</b> <asp:Label runat="server" ID="lb_userID">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Nombre:</b> <asp:Label runat="server" ID="lb_username">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Apellido:</b> <asp:Label runat="server" ID="lb_userlastname">sdgsd</asp:Label></p>
                    <br />  
                    <p><b>Rol:</b> <asp:Label runat="server" ID="lb_role">sdgsd</asp:Label></p>
                    <br />
                    <button class="button-inverse" id="x" onclick="hide_propiedadesUser();">Aceptar</button>
            </div>
        </div>
         
    </div>

   
    <!-- //FIN DE MODAL PARA VER LAS PROPIEDADES DE LA SESION -->


   <script>


  function w3_close() {
      if (document.getElementById("contenedor").style.display == "block") {
          document.getElementById("contenedor").style.display = "none";
          document.getElementById("controles").style.paddingLeft = '0px';
          document.getElementById("controles2").style.paddingLeft = '0px';
          
          document.getElementById("slide").style.paddingLeft = '0px';
         
          document.getElementById("minimize").style.paddingRight = '00px';
          document.getElementById("tabla").style.paddingLeft = '0px';
          document.getElementById("slide").style.display = "none";
          
      } else {
          document.getElementById("contenedor").style.display = "block";
           document.getElementById("controles").style.paddingLeft = '140px';
          document.getElementById("controles2").style.paddingLeft = '140px';
          document.getElementById("tabla").style.paddingLeft = '140px';
        
          document.getElementById("slide").style.paddingRight = '185px';
          document.getElementById("minimize").style.paddingRight = '160px';
          document.getElementById("slide").style.display = "flex";

          //document.getElementById("slide").style.paddingLeft = '160px';
      }
  
  }

</script>

    <!-- JS, Popper.js, and jQuery -->
    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/popper.min.js"></script>
    <script type="text/javascript" src="Scripts/a076d05399.js"></script>

    <!-- JS de DataTables -->
    <script type="text/javascript" src="js/table-players.js"></script>
    <script type="text/javascript" src="js/descarga-logs.js"></script>
    <script type="text/javascript" src="js/modal-usuario.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dataTables.select.min.js"></script>


        </form>
</body>


</html>