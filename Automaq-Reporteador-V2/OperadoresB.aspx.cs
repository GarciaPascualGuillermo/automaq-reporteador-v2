﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Automaq_Reporteador_V2
{
    public partial class OperadoresB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Access();
            //
            //printUser(getUser());


            printCT(getCT("A"));    
            System.Diagnostics.Debug.WriteLine("holas");
            /*
            if (CheckSession())
            {
                SessionDetails();
                if (Session["param"].ToString() != ""){
                    loadData(Createquery2(Session["param"].ToString()));
                }else{
                    loadTable();
                }
            }
            else{
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
            // Session["param"] = "";
        */
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
            Session["param"] = "";
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
            Session["param"] = "";
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Conteos.aspx");

        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }
     
        public List<List<String>> Access(String idemployee)
        {
            List<List<String>> hoy = new List<List<string>>();

            String query = "select * from employee where inside = 1 and idemployee="+idemployee+"";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {
                List<String> empleado = new List<string>();
                //System.Diagnostics.Debug.WriteLine(reader.GetValue(0) + " " + reader.GetValue(1) + "  " + reader.GetValue(2));
                if (GetUsersOp(dr.GetValue(0).ToString()))
                {
                    empleado.Add(dr.GetValue(0).ToString());
                    empleado.Add(dr.GetValue(1).ToString());
                    empleado.Add(dr.GetValue(2).ToString());
                    hoy.Add(empleado);
                }
            }
            conn.Close();
            return hoy;
        }

        public List<List<String>> Access()
        {
            List<List<String>> hoy = new List<List<string>>();
        

            String query = "select * from employee where inside = 1 ";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
           
            while (dr.Read())
            {
                List<String> empleado = new List<string>();
                //System.Diagnostics.Debug.WriteLine(reader.GetValue(0) + " " + reader.GetValue(1) + "  " + reader.GetValue(2));
                if (GetUsersOp(dr.GetValue(0).ToString())){
                    empleado.Add(dr.GetValue(0).ToString());
                    empleado.Add(dr.GetValue(1).ToString());
                    empleado.Add(dr.GetValue(2).ToString());
                    hoy.Add(empleado);
                }                
            }
            conn.Close();
            return hoy;
        }



        public Boolean GetUsersOp(String idemployee)
        {
            bool flag = false;
            //System.Diagnostics.Debug.WriteLine(idemployee+"prueba ");
            List<String> data = new List<string>();
            String query = "SELECT * FROM users join role_user on users.id=role_user.user_id where idemployee='"+idemployee+ "' and (role_id=5 or role_id=6 or role_id=7 or role_id=8 or role_id=9 ) ; ";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                flag = true;
            }
          
            conn.Close();
            // Console.ReadLine();
            return flag;
        }

        public List<String> GetOperationsDays(String idmachine, String type)
        {
            String param = "";
            if (type == "A")
            {
                param = "";
            }

            else
            {
                param = "and Section='" + type + "'";
            }
            List<String> data = new List<string>();
            String query = "SELECT workcenters.id, num_machine, idemployee,operations.job, operations.operation_service ,concat(users.name, ' ', users.firstname) , processes.status , part_number" +
                " FROM user_processes " +
                " JOIN users on users.id = user_processes.user_id" +
                " join processes on user_processes.process_id = processes.id" +
                " full outer join workcenters on processes.workcenter_id = workcenters.id" +
                " join operations on processes.operation_id = operations.id " +
                " JOIN process_logs on process_logs.process_id=processes.id " +
                " join pieces on operations.piece_id= pieces.id " +
                " where start_at = CURRENT_DATE and (user_processes.active = true or user_processes.log_out) and (status <> 'crp' and status <> 'cr') and workcenters.id= " + idmachine+" "+param+ " ORDER BY process_logs.created_at desc limit 1;";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
           //System.Diagnostics.Debug.WriteLine(query);
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                //List<String> data = new List<string>();
                data.Add(dr[0].ToString());
                data.Add(dr[1].ToString());
                data.Add(dr[2].ToString());
                data.Add(dr[3].ToString());
                data.Add(dr[4].ToString());
                data.Add(dr[5].ToString());
                data.Add(dr[6].ToString());
                data.Add(dr[7].ToString());
                // Console.WriteLine(dr[0]+" --- "+dr[1]);
                //actives.Add(data);
            }
            else
            {
                //List<String> data = new List<string>();
                data.Add("----");
                data.Add("----");
                data.Add("----");
                data.Add("----");
                data.Add("----");
                data.Add("SIN OPERADOR");
                data.Add("----");
                data.Add("----");
                // Console.WriteLine(dr[0]+" --- "+dr[1]);
                //actives.Add(data);
            }
            conn.Close();
            // Console.ReadLine();
            return data;
        }


        public List<List<String>> getCT(String type)
        {
            String param = "";
            if (type == "A")
            {
                param= "";
            }
           
            else
            {
                param = "and Section='"+type+"'";
            }
            List<List<String>> actives = new List<List<string>>();
            String query = "SELECT id, num_machine from workcenters where num_serie_tunerd != 'N/A' "+param+" order by workcenters.order;";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            //Console.WriteLine(query);
            while (dr.Read())
            {
                List<String> data = new List<string>();
                data.Add(dr[0].ToString());
                data.Add(dr[1].ToString());
                List<String> active = GetOperationsDays(dr[0].ToString(), type);
                data.Add(active[0]);
                data.Add(active[1]);
                data.Add(active[2]);
                data.Add(active[3]);
                data.Add(active[4]);
                data.Add(active[5]);
                data.Add(active[6]);
                data.Add(active[7]);
                // System.Diagnostics.Debug.WriteLine(dr[0]+" --- "+dr[1]+"------"+active[0]+"-------"+active[1] +"--------"+ active[2] + "-------" + active[3] + "-------" + active[4]);
                actives.Add(data);
            }
            conn.Close();
            // Console.ReadLine();
            return actives;
        }



        public String getOp(String idemployee)
        {
            String data = "";
            String query = "SELECT concat(idemployee, ' ', name, ' ',firstname) from users where idemployee='"+idemployee+"';";
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand(query, conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                data = dr[0].ToString();
            }
            conn.Close();
            // Console.ReadLine();
            return data;
        }



        public LinkButton Createbtn(String name, String iden)
        {
            LinkButton btn = new LinkButton()
            {
                ID = name+"1",
                CommandName = name,
                Text = iden,
            };
            btn.Click += new EventHandler(verCT);
         //   System.Diagnostics.Debug.WriteLine(btn.CommandName);
            return btn;
        }

    


        public void printCT(List<List<String>> machines)
        {
            tabla2.Controls.Clear();
            tabla3.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("");
            t.Append("<div class='container-fluid'>");
            t.Append("<div class='row'>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s1 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s1);
            tabla2.Controls.Add(Createbtn("A", "Todos"));
            // t.Append("<asp:LinkButton runat = 'server' ID='A' CommandName='A' OnClick='verCT' text='Todos'/>");
            t.Clear();
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s2 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s2);
            tabla2.Controls.Add(Createbtn("S", "Seguetas"));
           // t.Append("<asp:LinkButton runat = 'server' ID='S' CommandName='S' OnClick='verCT' text='Seguetas'/>");
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s3 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s3);
            tabla2.Controls.Add(Createbtn("TC", "Tornos Chicos"));
            //t.Append("<asp:LinkButton runat = 'server' ID='TC' CommandName='TC' OnClick='verCT' text='Tornos chicos'/>");
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s4 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s4);
            tabla2.Controls.Add(Createbtn("TV", "Tornos verticales"));
            //t.Append("<asp:LinkButton runat = 'server' ID='TV' CommandName='TV' OnClick='verCT' text='Tornos verticales'/>");
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s5 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s5);
            tabla2.Controls.Add(Createbtn("TP", "Tornos petroleros"));
            //t.Append("<asp:LinkButton runat = 'server' ID='TP' CommandName='TP' OnClick='verCT' text='Tornos petroleros'/>");
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s6 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s6);
            tabla2.Controls.Add(Createbtn("5E", "5 Ejes"));
            //t.Append("<asp:LinkButton runat = 'server' ID='ES' CommandName='5E' OnClick='verCT' text='5 Ejes'/> ");
            t.Append("</div>");
            t.Append("<div class='col btn btn-link font-light text-primary' style='color: rgb(110, 110, 110); '>");
            Literal s7 = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s7);
            tabla2.Controls.Add(Createbtn("CM", "Centros de Maquinado"));
            //t.Append("<asp:LinkButton runat = 'server' ID='CM' CommandName='CM' OnClick='verCT' text='Centros de Maquinado'/>");
            t.Append("</div>");
            t.Append("</div>");
            t.Append("</div> ");
            t.Append("</br>");
            t.Append("<div class='row row-cols-2 row-cols-md-6 g-6'> ");
            String id= "card";
            int contador = 0;
            foreach (List<String> ct in machines)
            {
                contador++;
                String card=id+contador;
              
                t.Append("<div class='col'>");
                t.Append("<section class='container1'>");
             
                t.Append("<div class='card1' id='"+card+"' onclick='flip("+card+")'>");
                if (ct[7] == "SIN OPERADOR")
                {
                    t.Append("<div class='front'  style='background-color:SlateGray;'>");
                }
                else
                {
                    if (ct[8].ToString() == "ih" || ct[8].ToString() == "fh")
                    {
                        t.Append("<div class='front'  style='background-color:DarkOrange;'>");
                    }
                    else
                    if (ct[8].ToString() == "ia" || ct[8].ToString() == "fa" || ct[8].ToString() == "il" || ct[8].ToString() == "ial" || ct[8].ToString() == "ls" || ct[8].ToString() == "ln")
                    {
                        t.Append("<div class='front'  style='background-color:Gold;'>");
                    }
                    else
                    if (ct[8].ToString() == "pr")
                    {
                        t.Append("<div class='front'  style='background-color:green;'>");
                    }
                    else
                    if (ct[8].ToString() == "pfm" || ct[8].ToString() == "pfh" || ct[8].ToString() == "pm")
                    {
                        t.Append("<div class='front'  style='background-color:red;'>");
                    }
                    else
                    if (ct[8].ToString() == "crp" || ct[8].ToString() == "cr")
                    {
                        t.Append("<div class='front'  style='background-color:Navy;'>");
                    }
                    else
                    if (ct[8].ToString() == "")
                    {
                        t.Append("<div class='front'  style='background-color:blue;'>");
                    }
                    else
                    {
                        t.Append("<div class='front'  style='background-color:SlateGray;'>");
                    }
                }
                t.Append("<h2>" + ct[1] +"<br>" +"</h2>");
                t.Append("<p>" + ct[8].ToString().ToUpper() + "</p>");
                t.Append("</div>");
              //  t.Append("<div class='front'>1</div>");
                t.Append("<div class='back'>");
                t.Append("<h2 >" + ct[5] + "</h2>");
                t.Append("<p style='font-size:15px'>" + "OP: " + ct[6] + "</p>");
                t.Append("<p style='font-size:15px'>" + "P.N.: " + ct[9] + "</p>");
                t.Append("<p style='font-size:15px'>" + ct[7] + "</p>");
              
                t.Append("</div>");
                t.Append("</div>");
                t.Append("</section>");
                t.Append("</div>");
                t.Append("</br>");              
            }            
            t.Append("</div>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }

        public void printUser(List<List<String>> users)
        {
            tabla2.Controls.Clear();
            tabla3.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<div class='row row-cols-2 row-cols-md-6 g-6'>");
            foreach (List<String> ct in users)
            {
                t.Append("<div class='col'>");
                t.Append("<div class='flip-card'>");
                t.Append("<div class='flip-card-inner'>");
                if (ct[3] != "----" || ct[1] =="1")
                {
                    t.Append("<div class='flip-card-front' style='background-color:green;'>");
                }
                else
                {                    
                    t.Append("<div class='flip-card-front' style='background-color:SlateGray;'>");
                }
                t.Append("<p class='target'>" + ct[7] + "</p>");
             
                t.Append("</div>");
                t.Append("<div class='flip-card-back'>");
                t.Append("<h2 >" + ct[3] + "</h2>");
                t.Append("<p>JOB: " + ct[5] + "</p>");
                t.Append("<p>" + "OP: " + ct[6] + "</p>");
                t.Append("</div>");
                t.Append("</div>");
                t.Append("</div>");

                t.Append("</div>");
                t.Append("</br>");
            }
            t.Append("</div>");
            Literal s = new Literal { Text = t.ToString() };
            tabla3.Controls.Add(s);
        }
        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void verCT(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            System.Diagnostics.Debug.WriteLine(btn.CommandName + "holis");

            printCT(getCT(btn.CommandName));
        }


        public void verCT2(object sender, EventArgs e)
        {

            printCT(getCT("A"));
        }

      
        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

    }

}