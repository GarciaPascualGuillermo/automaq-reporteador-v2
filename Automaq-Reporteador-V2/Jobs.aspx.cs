﻿using Npgsql;
using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Data.SqlClient;



namespace Automaq_Reporteador_V2
{
    public partial class Jobs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckSession())
            {
                SessionDetails();
                if(Session["param"].ToString()!="")
                {
                    loadData(Createquery2(Session["param"].ToString()));
                    
                }
                else
                {
                    loadTable();
                }
                
            }
            else
            {
                Response.Write("<script>alert('Primero tienes que inciar sesión');</script>");
                Response.Redirect("Default.aspx");
            }
           // Session["param"] = "";
        }

        public void SessionDetails()
        {
            lb_userID.Text = Session["IdEmployee"].ToString();
            lb_username.Text = Session["User"].ToString();
            lb_userlastname.Text = Session["LastName"].ToString();
            lb_role.Text = Session["Role"].ToString();
        }
        public bool CheckSession()
        {
            bool flag = false;
            if (Session["User"] != null)
            {
                flag = true;
            }
            return flag;
        }
        //metedos de redireccion a otros reportes
        public void Jobs1(object sender, EventArgs e)
        {
            Response.Redirect("Jobs.aspx");
            Session["param"] = "";
        }
        public void Ajustadores1(object sender, EventArgs e)
        {
            Response.Redirect("Ajustadores.aspx");
            Session["param"] = "";
        }
        public void Conteos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Conteos.aspx");
            
        }
        public void Eficiencia1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Eficiencia.aspx");
        }
        public void Supervisor1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Supervisor.aspx");
        }
        public void TiemposMuertos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("TiemposMuertos.aspx");
        }
        public void CTS1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTS.aspx");
        }
        public void CTSDetallados1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("CTSDetallados.aspx");
        }
        public void Mantenimientos1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Mantenimientos.aspx");
        }
        public void Calidad1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Calidad.aspx");
        }
        public void Asistencias1(object sender, EventArgs e)
        {
            Session["param"] = "";
            Response.Redirect("Operadores.aspx");
        }


        //aqui terminan
        public void Platform(object sender, EventArgs e)
        {
            Response.Redirect("https://automaq.tecmaq.local");
        }

        public void Logout(object sender, EventArgs e)
        {
            Session["IdEmployee"] = null;
            Session["User"] = null;
            Session["LastName"] = null;
            Session["Role"] = null;
            Response.Redirect("Default.aspx");
        }

        //reportes
        public void resetFilter(object sender, EventArgs e)
        {
         
            Job.Value = "";
            Customer.Value = "";
            Partn.Value = "";
            Fecha_final.Value = "";
            Fecha_inicio.Value = "";
        }

        public void Find(object sender, EventArgs e)
        {
            Createquery();
            Response.Redirect("Jobs.aspx");
        }

      
        protected void loadTable()
        {
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display nowrap' cellspacing='0' runat='server'id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>REV");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ENTREGA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD HECHA");
            t.Append("</td>");
            t.Append("<td class='t_header'>STATUS");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");

            t.Append("</thead>");
            t.Append("<tbody>");
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>REV");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ENTREGA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD HECHA");
            t.Append("</td>");
            t.Append("<td class='t_header'>STATUS");
            t.Append("</td>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);
        }


        protected void loadData(String param)
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=172.31.3.253; User Id=tecmaq; Password=tecmaq;Database=tecmaq;");
            conn.Open();
           // System.Diagnostics.Debug.WriteLine("" + Createquery());
            NpgsqlCommand command = new NpgsqlCommand(param, conn);
            // Execute the query and obtain a result set
            NpgsqlDataReader dr = command.ExecuteReader();
            tabla2.Controls.Clear();
            StringBuilder t = new StringBuilder();
            t.Append("<table class='table-players display  nowrap' cellspacing='0' id='table_players' style='width: 1275px;'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>REV");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ENTREGA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD HECHA");
            t.Append("</td>");
            t.Append("<td class='t_header'>STATUS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SELECT");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");
            while (dr.Read())
            {
                String status = "Closed";
                if ((dr[8].ToString())=="1")
                {
                    status = "Active";
                }
                t.Append("<tr>");
                t.Append("<td> ");
                Literal s1 = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s1);
                tabla2.Controls.Add(createLabel(dr[0].ToString()));
                //tabla2.Controls.Add(createLabel(dr[0].ToString()));
                t.Clear();
                // tabla2.Controls.Add(s1);

                t.Append("</td>");
                t.Append("<td>" + dr[1] + "");
                t.Append("</td>");
                DateTime d = DateTime.Parse(dr[2].ToString());
                t.Append("<td>" + d.ToString("dd/MM/yyyy") + "");
                t.Append("</td>");
                t.Append("<td>" + dr[3] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[4] + "");
                t.Append("</td>");
                t.Append("<td>" + dr[5] + "");
                t.Append("</td>");
                DateTime d2 = DateTime.Parse(dr[6].ToString());
                t.Append("<td>" + d2.ToString("dd/MM/yyyy") + "");
                t.Append("</td>");
                t.Append("<td>" + dr[7] + "");
                t.Append("</td>");
                t.Append("<td>"+status+"");
                t.Append("</td>");
                t.Append("<td>");
                Literal s2 = new Literal { Text = t.ToString() };
                tabla2.Controls.Add(s2);
                tabla2.Controls.Add(createLabel(dr[0].ToString(), "Select"));
                //tabla2.Controls.Add(createLabel(dr[0].ToString()));
                t.Clear();
                // tabla2.Controls.Add(s1);
                t.Append("</td>");
                t.Append("</tr>");

            }
            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>JOB");
            t.Append("</td>");
            t.Append("<td class='t_header'>CUSTOMER");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>PART NUMBER");
            t.Append("</td>");
            t.Append("<td class='t_header'>REV");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD ORDEN");
            t.Append("</td>");
            t.Append("<td class='t_header'>FECHA ENTREGA");
            t.Append("</td>");
            t.Append("<td class='t_header'>CANTIDAD HECHA");
            t.Append("</td>");
            t.Append("<td class='t_header'>STATUS");
            t.Append("</td>");
            t.Append("<td class='t_header'>SELECT");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");
            Literal s = new Literal { Text = t.ToString() };
           // tabla2.Controls.Add(s);
            tabla2.Controls.Add(s);
            conn.Close();
            //tabla2.Controls.Add(createLabel("sdfsd"));
          
        }

       

        public LinkButton createLabel(String s)
        {
            LinkButton button = new LinkButton
            {
                ID = s,
              //  OnClientClick = "Platform()",
                Text = s,
               // CssClass = "button-inverse",
                
                
            };
            button.Click += new EventHandler(job);
            //button.Command += plat;
            return button;
        }
        public LinkButton createLabel(String s, String name)
        {
            LinkButton button = new LinkButton
            {
                ID = "l"+s,
                ToolTip=s,
                Text = name,              
            };
            button.Click += new EventHandler(table);
            //button.Command += plat;
            return button;
        }

        private void job(object sender, EventArgs e)
        {
            LinkButton bttn = sender as LinkButton;
         //   System.Diagnostics.Debug.WriteLine("Job Llamada: "+bttn.ID);
            Response.Redirect("JobReport.aspx?JN="+bttn.ID);
           
        }
        private void table(object sender, EventArgs e)
        {
            LinkButton bttn = sender as LinkButton;
            //   System.Diagnostics.Debug.WriteLine("Job Llamada: "+bttn.ID);
            //Response.Redirect("JobReport.aspx?JN=" + bttn.ToolTip);
            mat(bttn.ToolTip);
            insu(bttn.ToolTip);

        }


        protected void mat(String job)
        {
            String conexion = "server=servidor\\exactsqlexpress;database=production;user id=sa;password=job1!boss;";
            SqlConnection cnn = new SqlConnection(conexion);
            cnn.Open();
            String consulta = "SELECT Material_Req.Job, Material_Req.Material, Material_Req.Description, Material_Req.Status, Material_Req.Est_Qty, Material_Trans.Material_Trans_Date, " +
                " Material_Trans.Quantity, Material_Trans.Stock_UofM, Material_Trans.Location_ID, Material_Trans.Lot" +
                " FROM Material_Trans RIGHT OUTER JOIN Material_Req ON Material_Trans.Material_Req = Material_Req.Material_Req" +
                " WHERE(Material_Req.Job = '"+job+"')" +
                " ORDER BY dbo.Material_Req.Material, dbo.Material_Trans.Material_Trans_Date;";
            SqlCommand cmd = new SqlCommand(consulta, cnn);

            try
            {
                material.Controls.Clear();
                StringBuilder t = new StringBuilder();
                t.Append("<table class='table-players display nowrap' cellspacing='0' runat='server'  style='width: 1275px;'>");
                t.Append("<thead>");
                t.Append("<tr>");
                t.Append("<td class='t_header'>JOB");
                t.Append("</td>");
                t.Append("<td class='t_header'>MATERIAL");
                t.Append("</td>");
                t.Append("<td class='t_header'>DESCRIPTION");
                t.Append("</td>");
                t.Append("<td class='t_header'>STATUS");
                t.Append("</td>");
                t.Append("<td class='t_header'>EST_QTY");
                t.Append("</td>");
                t.Append("<td class='t_header'>MATERIAL_TRANS_DATE");
                t.Append("</td>");
                t.Append("<td class='t_header'>QUANTITY");
                t.Append("</td>");
                t.Append("<td class='t_header'>STOCK_UOFN");
                t.Append("</td>");
                t.Append("<td class='t_header'>LOCATION_ID");
                t.Append("</td>");
                t.Append("<td class='t_header'>LOT");
                t.Append("</td>");
                t.Append("</thead>");
                t.Append("<tbody>");
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    t.Append("<tr>");
                    t.Append("<td>" + reader[0] + "</td>");
                    t.Append("<td>" + reader[1] + "</td>");
                    t.Append("<td>" + reader[2] + "</td>");
                    t.Append("<td>" + reader[3] + "</td>");
                    t.Append("<td>" + reader[4] + "</td>");
                    t.Append("<td>" + reader[5] + "</td>");
                    t.Append("<td>" + reader[6] + "</td>");
                    t.Append("<td>" + reader[7] + "</td>");
                    t.Append("<td>" + reader[8] + "</td>");
                    t.Append("<td>" + reader[9] + "</td>");
                    t.Append("</tr>");
                }
                t.Append("</tbody>");
                t.Append("<tfoot>");
                t.Append("</tfoot>");
                t.Append("</table>");
                Literal s = new Literal { Text = t.ToString() };
                material.Controls.Add(s);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e +"");
            }
            cnn.Close();
        }


        protected void insu(String job)
        {
            String conexion = "server=servidor\\exactsqlexpress;database=production;user id=sa;password=job1!boss;";
            SqlConnection cnn = new SqlConnection(conexion);
            cnn.Open();
            String consulta = " select PO,Line,Vendor,Order_Date,Order_Quantity,Purchase_Unit," +
                " Tran_Type,Document,Quantity,Stock_UofM,Material_Trans_Date,Status AS StatusLine from" +
                " (SELECT Source.Source, PO_Header.PO, PO_Detail.Line, PO_Header.Vendor, PO_Header.Order_Date, PO_Detail.Order_Quantity, PO_Detail.Purchase_Unit, PO_Detail.Status " +
                " FROM Job_Operation INNER JOIN" +
                " Source ON Job_Operation.Job_Operation = Source.Job_Operation INNER JOIN" +
                " PO_Detail ON Source.PO_Detail = PO_Detail.PO_Detail INNER JOIN" +
                " PO_Header ON PO_Detail.PO = PO_Header.PO" +
                " WHERE     (Job_Operation.Job = '"+job+"')) a left join" +
                " (SELECT     Material_Trans.Source, Material_Trans.Material_TransKey, Tran_Type, [Document], Quantity, Stock_UofM, Material_Trans_Date " +
                " FROM         Material_Trans " +
                " WHERE(Material_Trans.Vendor_Trans is not null) ) b on " +
                " a.Source = b.Source order by Material_TransKey; ";
            SqlCommand cmd = new SqlCommand(consulta, cnn);

            try
            {
                insumos.Controls.Clear();
                StringBuilder t = new StringBuilder();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    t.Append("<table class='table-players display nowrap' cellspacing='0' runat='server'  style='width: 1275px;'>");
                    t.Append("<thead>");
                    t.Append("<tr>");
                    t.Append("<td class='t_header'>PO");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>VENDOR");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>ORDER_DATE");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>LINE");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>ORDER_QUANTITY");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>PURCHASE_UNIT");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>TRAN_TYPE");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>DOCUMENT");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>QUANTITY");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>STOCK_UOFM");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>MATERIAL_TRANS_DATE");
                    t.Append("</td>");
                    t.Append("<td class='t_header'>STATUSLINE");
                    t.Append("</td>");
                    t.Append("</thead>");
                    t.Append("<tbody>");


                    while (reader.Read())
                    {
                        t.Append("<tr>");
                        t.Append("<td>" + reader[0] + "</td>");
                        t.Append("<td>" + reader[1] + "</td>");
                        t.Append("<td>" + reader[2] + "</td>");
                        t.Append("<td>" + reader[3] + "</td>");
                        t.Append("<td>" + reader[4] + "</td>");
                        t.Append("<td>" + reader[5] + "</td>");
                        t.Append("<td>" + reader[6] + "</td>");
                        t.Append("<td>" + reader[7] + "</td>");
                        t.Append("<td>" + reader[8] + "</td>");
                        t.Append("<td>" + reader[9] + "</td>");
                        t.Append("<td>" + reader[10] + "</td>");
                        t.Append("<td>" + reader[11] + "</td>");
                        t.Append("</tr>");
                    }
                    t.Append("</tbody>");
                    t.Append("<tfoot>");
                    t.Append("</tfoot>");
                    t.Append("</table>");
                    Literal s = new Literal { Text = t.ToString() };
                    insumos.Controls.Add(s);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e + "");
            }
            cnn.Close();
        }

        public void Impresora(object sender, EventArgs e)
        {          
           Response.Redirect("Conteos.aspx");
        }
        public void Impresora()
        {
            Response.Redirect("Conteos.aspx");
        }
        public String Createquery()
        {
            String param = " ";
            if (Request.Form["Fecha_inicio"] == "")
            {
                param += "where jobs.created_at >= '04/04/2020'";
            }
            if (Request.Form["Fecha_inicio"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_inicio"]);
                fs = fs.AddHours(5);
                param += "where (jobs.created_at>= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Fecha_final"] != "")
            {
                DateTime fs = DateTime.Parse(Request.Form["Fecha_final"]);
                    fs = fs.AddDays(1);  fs = fs.AddHours(1).AddMinutes(0).AddSeconds(0);
                param += "and (jobs.created_at <= '" + fs.ToString("yyyy/MM/dd hh:mm:ss") + "')";
            }
            if (Request.Form["Customer"] != "")
            {
                param += " and customer= '" + Request.Form["Customer"].Trim() + "' ";
            }
            if (Request.Form["Partn"] != "")
            {
                param += " and  pieces.part_number= '" + Request.Form["Partn"].Trim() + "' ";
            }
            if (Request.Form["Job"] != "")
            {
                param += " and  operations.job= '" + Request.Form["Job"].Trim() + "' ";
            }
            Session["param"] = param;
            return param;   
        }

        public String Createquery2(String param)
        {
            String query = "select num_job, customer,  max(jobs.created_at), pieces.part_number, rev, max(jobs.quantity), max(delivered_at), sum(user_processes.end-start),max(cast(processes.active as int)) from jobs " +
               " join operations on operations.job = jobs.num_job" +
               " Join processes on processes.operation_id = operations.id" +
               " join user_processes on user_processes.process_id = processes.id" +
               " join pieces on operations.piece_id = pieces.id" +
               " join clients on pieces.client_id = clients.id " +
               param +
               " GROUP BY  num_job, customer,  pieces.part_number, rev" +
               " order by num_job";
          //  System.Diagnostics.Debug.WriteLine(query);
            //loadData(query);
            return query;
            
        }

        public void ExportExcel(String query)
        {

        }
        public String OutTime(TimeSpan s)
        {
            String s1 = "";
            double days = s.Days;
            // System.Diagnostics.Debug.WriteLine("days: "+days);
            double hours = s.Hours;
            // System.Diagnostics.Debug.WriteLine("hours: "+hours);
            double minutes = s.Minutes;
            // System.Diagnostics.Debug.WriteLine("minutes: "+ minutes);
            double seconds = s.Seconds;
            // System.Diagnostics.Debug.WriteLine("seconds: " +seconds);
            int totalhours = (Convert.ToInt32(days * 24) + Convert.ToInt32(hours));
            String h, m, se;
            if (totalhours < 10)
            {
                h = "0" + totalhours;
            }
            else
            {
                h = "" + totalhours;
            }
            if (minutes < 10)
            {
                m = "0" + minutes;
            }
            else
            {
                m = "" + minutes;
            }
            if (seconds < 10)
            {
                se = "0" + seconds;
            }
            else
            {
                se = "" + seconds;
            }
            s1 = h + ":" + m + ":" + se;
            // System.Diagnostics.Debug.WriteLine(s1);
            return s1;
        }
    }

}